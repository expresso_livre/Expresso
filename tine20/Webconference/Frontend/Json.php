<?php

/**
 * Tine 2.0
 * @package     Webconference
 * @subpackage  Frontend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>, Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */

/**
 *
 * This class handles all Json requests for the Webconference application
 *
 * @package     Webconference
 * @subpackage  Frontend
 */
class Webconference_Frontend_Json extends Tinebase_Frontend_Json_Abstract {

    /**
     * the controller
     * 
     * @var Webconference_Controller_ExampleRecord
     */
    protected $_controller = NULL;

    /**
     * the constructor
     */
    public function __construct() {
        $this->_applicationName = 'Webconference';
        $this->_controller = Webconference_Controller_Config::getInstance();
    }

    /**
     * Search for records matching given arguments
     *
     * @param  array $filter
     * @param  array $paging
     * @return array
     */
    public function searchConfigs($filter, $paging) {
        return $this->_search($filter, $paging, $this->_controller, 'Webconference_Model_ConfigFilter');
    }
   
    /**
     * Return a single record
     *
     * @param   string $id
     * @return  array record data
     */
    public function getConfig($id) {
        return $this->_get($id, $this->_controller);
    }

    /**
     * creates/updates a record
     * 
     * @param  array $recordData
     * @return array created/updated record
     */
    public function saveConfig($recordData) {
        return $this->_save($recordData, Webconference_Controller_Config::getInstance(), 'Config');
    }


    /**
     * deletes existing records Webconference configuration
     *  
     * @param  array  $ids 
     * @return string
     */
    public function deleteConfigs($ids) {
        return $this->_delete($ids, $this->_controller);
    }

    /**
     * Get the Settings
     * 
     * @return array --settings 
     */
    public function getSettings() {
        $result = Webconference_Controller::getInstance()->getConfigSettings()->toArray();
        return $result;
    }

    /**
     * Search meetings active on the server
     * 
     * @return
     * 	- Null if the server is unreachable
     * 	- If FAILED then returns an array containing a returncode, messageKey, message.
     * 	- If SUCCESS then returns an array of all the meetings. Each element in the array is an array containing a meetingID,
     *           moderatorPW, attendeePW, hasBeenForciblyEnded, running.
     */
    public function getMeetings() 
    {
        return Webconference_Controller_BigBlueButton::getInstance()->getMeetings();
    }

    public function inviteUsersToJoin($users, $moderator, $roomId) 
    {
	return Webconference_Controller_BigBlueButton::getInstance()->inviteUsersToJoin($users, $moderator, $roomId);
    }

    public function isMeetingActive($roomName, $configID)
    {
        $active = Webconference_Controller_BigBlueButton::getInstance()->isMeetingActive($roomName, $configID);
        if($active){
            return array(
                'active' => true,
                'message'=> ''
            );
        } else {
	    return array(
		'active' => false,
		'message'=> 'The Webconference you are trying to join no longer exists'
	    );
	}
    }

    /**
     * @param string | array $filter
     * @param string | array $paging
     * @throws Tinebase_Exception_NotFound
     * @return array
     */
    public function searchRooms($filter, $paging)
    {
        if (!($limitRoomMonth = Webconference_Config::getInstance()->get(Webconference_Config::LIMIT_ROOM_MONTH,FALSE))){
            return $this->_search($filter, $paging, Webconference_Controller_Room::getInstance(), 'Webconference_Model_RoomFilter', TRUE);
        } else {
            // search amount of rooms created within current month by logged user
            $data = array( array(
                    "field" => "creation_time",
                    "operator" => "within",
                    "value" => 'monthThis',
            ), array(
                    "field" => "created_by",
                    "operator" => "equals",
                    "value" => Tinebase_Core::getUser()->getId()
            ));
            $filter = new Webconference_Model_RoomFilter($data);
            $rooms = $this->_search($filter, $paging, Webconference_Controller_Room::getInstance(), 'Webconference_Model_RoomFilter', TRUE);
            // checks if month rooms limit was reached by logged user
            if ( (int)$rooms['totalcount'] >= $limitRoomMonth ) {
                $translation = Tinebase_Translation::getTranslation('Webconference');
                throw new Tinebase_Exception_NotFound( $translation->_('Error (Reached month rooms limit!)'));
            }
            return $rooms;
        }
    }

    public function getRoom($uid)
    {
	return $this->_get($uid, Webconference_Controller_Room::getInstance());
    }
    
    /**
     * This method calls end meeting on the specified meeting in the bigbluebutton server.
     *
     * @param roomName -- the unique meeting identifier used to store the meeting in the bigbluebutton server
     * @return
     * 	- Null if the server is unreachable
     * 	- An array containing a returncode, messageKey, message.
     */
    public function endMeeting($roomId)
    {
        return Webconference_Controller_BigBlueButton::getInstance()->endMeeting($roomId);
    }
   
    public function saveRoom($recordData)
    {
	return $this->_save($recordData, Webconference_Controller_Room::getInstance(), 'Room');
    }
     
    
    public function logAccessLogon($roomId) 
    {
	return Webconference_Controller_AccessLog::getInstance()->logAccessLogin($roomId);
    }   


    public function regLogoff($idAccess) 
    {
	return Webconference_Controller_AccessLog::getInstance()->regLogoff($idAccess);
    }
    
     /**
     * Returns registry data
     * 
     * @return array
     */
    public function getRegistryData()
    {
        $defaultContainer = Webconference_Controller::getInstance()->getDefaultContainer()->toArray();
        $defaultContainer['account_grants'] = Tinebase_Container::getInstance()->getGrantsOfAccount(Tinebase_Core::getUser(), $defaultContainer['id'])->toArray();
        $registryData = array(
            'defaultContainer' => $defaultContainer
        );
        return $registryData;
    }

    /**
     * Search room from name and return ids
     * @param String Room Name
     * @return array of ids rooms
     */
    public function searchRoomName($roomName)
    {
        return Webconference_Controller_Room::getInstance()->getRoomName($roomName);
    }

}

