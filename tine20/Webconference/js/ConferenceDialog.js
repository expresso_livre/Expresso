/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2013 Metaways Infosystems GmbH (http://www.metaways.de)
 */
Ext.namespace('Tine.Webconference');

 Tine.Webconference.ConferenceDialog  = Ext.extend(Ext.FormPanel, {
    windowNamePrefix: 'ConferenceEditWindow_',
    appName: 'Webconference',
	/**
     * @cfg {Tine.Tinebase.Application} app
     * instance of the app object (required)
     */
    app: null,
    record: null,
    bodyStyle:'padding:5px',
    layout: 'fit',
    border: false,
    cls: 'tw-editdialog',
    anchor:'100% 100%',
    deferredRender: false,
    buttonAlign: null,
    bufferResize: 500,
    initComponent: function() {
        this.relationPanelRegistry = this.relationPanelRegistry ? this.relationPanelRegistry : [];
      
        if (! this.app) {
            this.app = Tine.Tinebase.appMgr.get(this.appName);
        }
        
	if (! this.windowNamePrefix) {
            this.windowNamePrefix = this.modelName + 'EditWindow_';
        }
        
        this.initButtons();
        // get items for this dialog
        this.items = this.getFormItems();
        Tine.Webconference.ConferenceDialog.superclass.initComponent.call(this);
    },

   /**
     * init buttons
     */
    initButtons: function() {
        this.fbar = [];
	/*
	this.action_add_user = new Ext.Action({
	    text: this.app.i18n._('Add User'),
	    handler: this.onAddUser,
	    disabled: ! this.moderator,
	    tooltip : this.app.i18n._('Invite an User to the Webconference'),
	    iconCls: 'action_addContact',
	    scope: this
	});
	*/
	this.action_exit = new Ext.Action({
	    text: this.app.i18n._('Exit'),
	    handler: this.onExit,
	    disabled: false,
	    tooltip : this.app.i18n._('Left Webconference'),
	    iconCls: 'action_logOut',
	    scope: this
	});
	this.action_terminate = new Ext.Action({
	    text: this.app.i18n._('Finish'),
	    handler: this.onTerminate,
	    disabled: ! this.moderator,
	    tooltip : this.app.i18n._('Terminate Webconference kicking all users out'),
	    scope: this,
	    iconCls: 'action_terminate'
	});

	this.tbar = new Ext.Toolbar({
            items: [{
                xtype: 'buttongroup',
                columns: 7,
                items: [
		    //this.action_add_user,
		    this.action_exit,
		    this.action_terminate
                ]
            }]
        });
    },   

    onAddUser: function() {
	var roomId = Tine.Tinebase.appMgr.get('Webconference').roomId;
	Tine.Webconference.getRoom(roomId, function(jsonText) {
	    this.record =Tine.Webconference.roomRecordBackend.recordReader({responseText:  Ext.util.JSON.encode(jsonText)});
	    Tine.Webconference.RoomEditDialog.openWindow({
		record: this.record,
		//Tine.Webconference.ContactPickerDialog.openWindow({
		listeners: {
		    scope: this,
		    'update': function(record) {
			console.log('update');
		    },
		    'cancel':function(win){

		    },
		    'destroy':function(win){

		    }
		}
	    });
	});
    },

    onExit: function(btn){	
        this.window.close();
	this.fireEvent('refresh');
	this.app.roomActive = false;
	this.app.origin = WebconferenceOrigin.MENU;
	Ext.get('webconference-iframe').dom.src = '';
	this.app.logAccessLogoff();
    },
    
    onTerminate: function(btn){
	this.hideConference();
	Ext.MessageBox.confirm(
	    '', 
	    Tine.Tinebase.appMgr.get('Webconference').i18n._('This will kick all participants out of the meeting. Terminate webconference') + ' ?', 
	    function(btn) {
		if(btn === 'yes') { 
		    var roomId = Tine.Tinebase.appMgr.get('Webconference').roomId;
		    Ext.Ajax.request({
			params: {
			    method: 'Webconference.endMeeting',
			    roomId: this.roomId
			},
			scope: this,
			success: function(_result, _request){
			    Tine.Tinebase.appMgr.get('Webconference').roomActive = false;
			    this.onExit();
			}
		    });
		} else {
		    this.showConference();
		}
	    }, this);
    },

    /**
     * returns dialog
     * NOTE: when this method gets called, all initalisation is done.
     * @private
     */
    getFormItems: function() {
        return {
	    id: 'webconference-panel',
	    header: false,
	    closable:false,
	    hideMode:'visibility', // to work with flash
	    layout:'fit', 
	    labelAlign: 'top',
            border: false,
            items: [
		[new Ext.ux.IFrameComponent({
		 id: 'webconference-iframe-cmp',
		 url: this.url})]
	    ]
	};
    },
    	
    /**
     * Hides the conference panel and sets flash element size to zero. 
     * That is necessary in Chrome browsers where flash overlap the add users and confirm dialog popup windows.
     */
    hideConference: function(){
        if(Ext.fly('webconference-iframe') ){
	    var bbb = Ext.fly('webconference-iframe');
	    bbb.setWidth(0);
	    bbb.setHeight(0);
	}
    },
    
    /**
     * Shows the conference panel and sets flash element size to 100%. 
     * That is necessary in Chrome browsers where flash overlap the add users and confirm dialog popup windows.
     */
    showConference: function(){
        //Tine.Tinebase.appMgr.get('Webconference').getMainScreen().getCenterPanel().show();
        if(Ext.fly('webconference-iframe')){
            var bbb = Ext.fly('webconference-iframe'); 
            bbb.setWidth('100%');
            bbb.setHeight('100%');
        }
    }    
});

/**
 * Conference Popup
 * 
 * @param   {Object} config
 * @return  {Ext.ux.Window}
 */
Tine.Webconference.ConferenceDialog.openWindow = function (config) {
    var id = (config.record && config.record.id) ? config.record.id : 0;
    this.record = config.record;
    var window = Tine.WindowFactory.getWindow({
        width: 1024,
        height: 768,
        name: Tine.Webconference.ConferenceDialog.prototype.windowNamePrefix + id,
        contentPanelConstructor: 'Tine.Webconference.ConferenceDialog',
        contentPanelConstructorConfig: config
    });
    //Ext.get('webconference-iframe').dom.src = this.url;
    return window;
}
/**
 * Component for the iframe component of the webconference application.
 *
 */
Ext.ux.IFrameComponent = Ext.extend(Ext.BoxComponent, {
    onRender : function(ct, position){
	this.el = ct.createChild({
	    tag: 'iframe', 
	    id: 'webconference-iframe', 
	    name: 'webconference-iframe',
	    frameBorder: 0, 
	    src: this.url
	});
    }
});