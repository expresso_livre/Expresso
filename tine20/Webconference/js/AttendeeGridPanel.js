/*
 * Tine 2.0
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */
 
Ext.ns('Tine.Webconference');

/**
 * @namespace   Tine.Webconference
 * @class       Tine.Webconference.AttendeeGridPanel
 * @extends     Ext.grid.EditorGridPanel
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */
Tine.Webconference.AttendeeGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
    autoExpandColumn: 'user_id',
    clicksToEdit: 1,
    enableHdMenu: false,
    
    /**
     * @cfg defaut text for new attendee combo
     * _('Click here to invite another attender...')
     */
    addNewAttendeeText: 'Click here to invite another attender...',
    
    /**
     * @cfg {Boolean} showGroupMemberType
     * show user_type groupmember in type selection
     */
    showMemberOfType: false,
    
    /**
     * @cfg {Boolean} showNamesOnly
     * true to only show types and names in the list
     */
    showNamesOnly: false,
    
    /**
     * The record currently being edited
     * 
     * @type Tine.Webconference.Model.Room
     * @property record
     */
    record: null,
    
    /**
     * id of current account
     * 
     * @type Number
     * @property currentAccountId
     */
    currentAccountId: null,
    
    /**
     * ctx menu
     * 
     * @type Ext.menu.Menu
     * @property ctxMenu
     */
    ctxMenu: null,
    
    /**
     * store to hold all attendee
     * 
     * @type Ext.data.Store
     * @property attendeeStore
     */
    attendeeStore: null,
    
    stateful: true,
    stateId: 'conf-attendeegridpanel',
    
    initComponent: function() {
        this.app = this.app ? this.app : Tine.Tinebase.appMgr.get('Webconference');
        this.currentAccountId = Tine.Tinebase.registry.get('currentAccount').accountId;
        this.title = this.hasOwnProperty('title') ? this.title : this.app.i18n._('Attendee');
        this.plugins = this.plugins || [];
        if (! this.showNamesOnly) {
            this.plugins.push(new Ext.ux.grid.GridViewMenuPlugin({}));
        }
        
        this.store = new Ext.data.SimpleStore({
            fields: Tine.Webconference.Model.Attender.getFieldDefinitions().concat('sort'),
            sortInfo: {field: 'user_id', direction: 'ASC'},
            sortData : function(f, direction){
                direction = direction || 'ASC';
                var st = this.fields.get(f).sortType;
                var fn = function(r1, r2){
                    // make sure new-attendee line is on the bottom
                    if (!r1.data.user_id) return direction == 'ASC';
                    if (!r2.data.user_id) return direction != 'ASC';
                    
                    var v1 = st(r1.data[f]), v2 = st(r2.data[f]);
                    return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
                };
                this.data.sort(direction, fn);
                if(this.snapshot && this.snapshot != this.data){
                    this.snapshot.sort(direction, fn);
                }
            }
        });
        this.on('beforeedit', this.onBeforeAttenderEdit, this);
        this.on('afteredit', this.onAfterAttenderEdit, this);
        this.initColumns();
        
        this.mon(Ext.getBody(), 'click', this.stopEditingIf, this);
        
        this.viewConfig = {
            getRowClass: this.getRowClass
        };
        Tine.Webconference.AttendeeGridPanel.superclass.initComponent.call(this);
    },
    
    initColumns: function() {
        this.columns = [
        {
            id: 'displaycontainer_id',
            dataIndex: 'displaycontainer_id',
            width: 200,
            sortable: false,
            hidden: this.showNamesOnly || true,
            header: Tine.Tinebase.translation._hidden('Saved in'),
            tooltip: this.app.i18n._('This is the webconference where the attender has saved this event in'),
            renderer: this.renderAttenderDispContainer.createDelegate(this),
            editor2: new Tine.widgets.container.selectionComboBox({
                blurOnSelect: true,
                selectOnFocus: true,
                appName: 'Webconference',
                //startNode: 'personalOf', -> rework to startPath!
                getValue: function() {
                    if (this.selectedContainer) {
                        // NOTE: the store checks if data changed. If we don't overwrite to string, 
                        //  the check only sees [Object Object] wich of course never changes...
                        var container_id = this.selectedContainer.id;
                        this.selectedContainer.toString = function() {return container_id;};
                    }
                    return this.selectedContainer;
                },
                listeners: {
                    scope: this,
                    select: function(field, newValue) {
                        // the field is already blured, due to the extra chooser window. We need to change the value per hand
                        var selection = this.getSelectionModel().getSelectedCell();
                        if (selection) {
                            var row = selection[0];
                            this.store.getAt(row).set('displaycontainer_id', newValue);
                        }
                    }
                }
            })
        }, {
            id: 'user_type',
            dataIndex: 'user_type',
            width: 40,
            sortable: true,
            resizable: false,
            header: this.app.i18n._('Type'),
            renderer: this.renderAttenderType.createDelegate(this),
            editor: new Ext.form.ComboBox({
                blurOnSelect  : true,
                expandOnFocus : true,
                mode          : 'local',
                store         : [
                    ['user',     this.app.i18n._('User')   ]
                ]
            })
        }, {
            id: 'role',
            dataIndex: 'role',
            width: 70,
            sortable: true,
            header: this.app.i18n._('Role'),
            renderer: this.renderAttenderRole.createDelegate(this),
            editor: {
                xtype: 'widget-keyfieldcombo',
                app:   'Webconference',
                keyFieldName: 'wconfRoles'
            }
        }, {
            id: 'user_id',
            dataIndex: 'user_id',
            width: 300,
            sortable: true,
            header: this.app.i18n._('Name'),
            renderer: this.renderAttenderName.createDelegate(this),
            editor: true
        },  

        ];
    },

    validateInsertStore: function(o){
        var isDuplicate = false;
        this.store.each(function(attender) {
            if (o.record.getUserId() == attender.getUserId()
                    && o.record.get('user_type') == attender.get('user_type')
                    && o.record != attender) {
                attender.set('checked', true);
                var row = this.getView().getRow(this.store.indexOf(attender));
                Ext.fly(row).highlight();
                isDuplicate = true;
                return false;
            }
        }, this);
        if (isDuplicate) {
            o.record.reject();
            this.startEditing(o.row, o.column);
        } else if (o.value) {
            o.record.explicitlyAdded = true;
            o.record.set('checked', true);
            var newAttender = new Tine.Webconference.Model.Attender(Tine.Webconference.Model.Attender.getDefaultData(), 'new-' + Ext.id() );	    this.store.add([newAttender]);
            this.startEditing(o.row +1, o.column);
            if (o.record.get('user_id').account_id === null) {
                Ext.Msg.show({
                    title: this.app.i18n._('External Attendee'),
                    msg: this.app.i18n._('You have selected an external attendee!'),
                    icon: Ext.MessageBox.INFO,
                    buttons: Ext.Msg.OK,
                    scope: this
                });
            }
        }
    },
    
    onAfterAttenderEdit: function(o) {
        switch (o.field) {
            case 'user_id' :
                if (o.value && o.record.get('user_id') && o.record.get('user_id').account_id === null) {
                    Ext.MessageBox.wait(_('Please wait'), this.app.i18n._('Verifying account'));
                    var filter = [{field: 'email', operator: 'equals', value: o.record.get('user_id').email}];
                    filter.push({field: 'type', operator: 'equals', value: 'user'});
                    Tine.Addressbook.searchContacts(filter, null, function(response) {
                        Ext.MessageBox.hide();
                        if (response.results.length > 0) {
                            var contact = new Tine.Webconference.Model.Attender(response.results[0]);
                            o.record.reject();
                            o.record.set('user_id',contact.data);
                        }
                        this.validateInsertStore(o);
                    }, this);
                } else {
                    this.validateInsertStore(o);
                }
                break;
                
            case 'user_type' :
                this.startEditing(o.row, o.column +1);
                break;
            
            case 'container_id':
                // check if displaycontainer of owner got changed
                if (o.record == this.roomOriginator) {
                    this.record.set('container_id', '');
                    this.record.set('container_id', o.record.get('displaycontainer_id'));
                }
                break;
        }
    },
    
    onBeforeAttenderEdit: function(o) {        
        if (o.field == 'displaycontainer_id') {
            if (! o.value || ! o.value.account_grants || ! o.value.account_grants.deleteGrant) {
                o.cancel = true;
            }
            return;
        }
        
        // for all other fields user need editGrant
        if (! this.record.get('editGrant')) {
            o.cancel = true;
            return;
        }
        
        // don't allow to set anything besides quantity and role for already set attendee
        if (o.record.get('user_id')) {
            o.cancel = true;
            if (o.field == 'quantity' && o.record.get('user_type') == 'resource') {
                o.cancel = false;
            }
            if (o.field == 'role') {
                o.cancel = false;
            }
            return;
        }
        
        if (o.field == 'user_id') {
            // switch editor
            var colModel = o.grid.getColumnModel();
            switch(o.record.get('user_type')) {
                case 'user' :
                colModel.config[o.column].setEditor(new Tine.Addressbook.SearchCombo({
                    allowBlank: true,
                    blurOnSelect: true,
                    selectOnFocus: true,
                    forceSelection: false,
                    verifyUserAccount: true,
                    renderAttenderName: this.renderAttenderName,
                    getValue: function() {
                        var value = this.selectedRecord ? this.selectedRecord.data : ((this.getRawValue() && Ext.form.VTypes.email(this.getRawValue())) ? this.getRawValue() : null);
                        return value;
                    }
                }));
                break;
            }
            colModel.config[o.column].editor.selectedRecord = null;
        } 
    },
    
    /**
     * give new attendee an extra cls
     */
    getRowClass: function(record, index) {
        if (! record.get('user_id')) {
           return 'x-cal-add-attendee-row';
        }
    },
    
    /**
     * stop editing if user clicks else where
     * 
     * FIXME this breaks the paging in search combos, maybe we should check if search combo paging buttons are clicked, too
     */
    stopEditingIf: function(e) {
        if (! e.within(this.getGridEl())) {
           this.stopEditing();
        }
    },
    
    // NOTE: Ext docu seems to be wrong on arguments here

  onContextMenu: function(e, target) {
        e.preventDefault();
        var row = this.getView().findRowIndex(target);
        var attender = this.store.getAt(row);
        var disable_delegation = true;
        if (attender && ! this.disabled) {
            // don't delete 'add' row
            if (! attender.get('user_id')) {
                return;
            }
            if (attender.get('user_id').account_id 
             && Tine.Tinebase.registry.get('currentAccount').accountId==attender.get('user_id').account_id 
             && (this.record.get('organizer') == undefined ||
                this.record.get('organizer').account_id!=Tine.Tinebase.registry.get('currentAccount').accountId)) {
                disable_delegation = false;
            }
                        
            this.ctxMenu = new Ext.menu.Menu({
                items: [{
                    text: this.app.i18n._('Remove Attender'),
                    iconCls: 'action_delete',
                    scope: this,
                    disabled: !this.record.get('editGrant'),
                    handler: function() {
                        this.store.removeAt(row);
                    }
                    
                },/*{
                    text: this.app.i18n._('Delegate Attendance'),
                    iconCls: 'action_delegate',
                    scope: this,
                    disabled: disable_delegation,
                    handler: function() {
                        this.delegateAttendance();
                    }
                }*/]
            });
            this.ctxMenu.showAt(e.getXY());
        }
    },
   
    /**
     * loads this panel with data from given record
     * called by edit dialog when record is loaded
     * 
     * @param {Tine.Webconference.Model.Room} record
     * @param Array addAttendee
     */
    onRecordLoad: function(record) {
        this.record = record;
        this.store.removeAll();
        var attendee = record.get('attendee');
        Ext.each(attendee, function(attender) {
            var record = new Tine.Webconference.Model.Attender(attender, attender.id);
            this.store.addSorted(record);
            
            if (attender.displaycontainer_id  && this.record.get('container_id') && attender.displaycontainer_id.id == this.record.get('container_id').id) {
                this.roomOriginator = record;
            }
        }, this);
        if (record.get('editGrant')) {
            this.store.add([new Tine.Webconference.Model.Attender(Tine.Webconference.Model.Attender.getDefaultData(), 'new-' + Ext.id() )]);
       }
    },
    
    /**
     * Updates given record with data from this panel
     * called by edit dialog to get data
     * 
     * @param {Tine.Webconference.Model.Room} record
     */
    onRecordUpdate: function(record) {
        this.stopEditing(false);
        
        var attendee = [];
        this.store.each(function(attender) {
            var user_id = attender.get('user_id');
          if (user_id/* && user_id.id*/) {
              
              if (typeof user_id.get == 'function') {
                    attender.data.user_id = user_id.data;
                }
                
               attendee.push(attender.data);
            }
        }, this);
        
        record.set('attendee', attendee);
    },
    
    onKeyDown: function(e) {
        switch(e.getKey()) {            
            case e.DELETE: 
                if (this.record.get('editGrant')) {
                    var selection = this.getSelectionModel().getSelectedCell();
                    
                    if (selection) {
                        var row = selection[0];
                        
                        // don't delete 'add' row
                        var attender = this.store.getAt(row);
                        if (! attender.get('user_id')) {
                            return;
                        }

                        this.store.removeAt(row);
                    }
                }
                break;
        }
    },
   
    delegateAttendance: function(record) {
        // delegate attendance to a new user:
        Tine.Webconference.AttendeeEditDialog.openWindow({
            listeners: {
                scope: this,
                update: function (eventJson) {
                    var o = Tine.Webconference.roomRecordBackend.recordReader({responseText: eventJson}).get('attendee')[0];
                    o.dirty = true;
                    o.modified = {};
                    this.store.each(function(attender) {
                        if (Tine.Tinebase.registry.get('currentAccount').accountId==attender.get('user_id').account_id) {
                            var row = this.getView().getRow(this.store.indexOf(attender));
                            var idx = this.store.indexOf(attender);
                            // detect duplicate entry
                            // TODO detect duplicate emails, too 
                            var isDuplicate = false;
                            this.store.each(function(att) {
                                if (o.user_id.id == att.getUserId()
                                        && o.user_type == att.get('user_type')
                                        && o != att) {
                                    row = this.getView().getRow(this.store.indexOf(att));
                                    isDuplicate = true;
                                    return false;
                                }
                            }, this);
                            if (! isDuplicate) {
                                this.store.remove(attender);
                                var record = new Tine.Webconference.Model.Attender(o, attender.id);
                                this.store.insert(idx, record);
                                row = this.getView().getRow(this.store.indexOf(record));
                            }
                            Ext.fly(row).highlight();
                        }
                    }, this);
                }
            }
        });
    },
    
    renderAttenderName: function(name, metaData, record) {
        if (name) {
            var type = record ? record.get('user_type') : 'user';
            return this['renderAttender' + Ext.util.Format.capitalize(type) + 'Name'].apply(this, arguments);
        }
        
        // add new user:
        if (arguments[1]) {
            arguments[1].css = 'x-form-empty-field';
            return this.app.i18n._(this.addNewAttendeeText);
        }
    },
    
    renderAttenderUserName: function(name) {
        name = name || "";
        if (typeof name.get == 'function' && name.get('n_fileas')) {
            return Ext.util.Format.htmlEncode(name.get('n_fileas'));
        }
        if (name.n_fileas) {
            return Ext.util.Format.htmlEncode(name.n_fileas);
        }
        if (name.accountDisplayName) {
            return Ext.util.Format.htmlEncode(name.accountDisplayName);
        }
        // how to detect hash/string ids
        if (Ext.isString(name) && ! name.match('^[0-9a-f-]{40,}$') && ! parseInt(name, 10)) {
            return Ext.util.Format.htmlEncode(name);
        }
        // NOTE: this fn gets also called from other scopes
        return Tine.Tinebase.appMgr.get('Webconference').i18n._('No Information');
    },
    
    renderAttenderGroupmemberName: function(name) {
        var name = Tine.Webconference.AttendeeGridPanel.prototype.renderAttenderUserName.apply(this, arguments);
        return name + ' ' + Tine.Tinebase.appMgr.get('Webconference').i18n._('(as a group member)');
    },
    
    renderAttenderGroupName: function(name) {
        if (typeof name.getTitle == 'function') {
            return Ext.util.Format.htmlEncode(name.getTitle());
        }
        if (name.name) {
            return Ext.util.Format.htmlEncode(name.name);
        }
        if (Ext.isString(name)) {
            return Ext.util.Format.htmlEncode(name);
        }
        return Tine.Tinebase.appMgr.get('Webconference').i18n._('No Information');
    },
    
    renderAttenderMemberofName: function(name) {
        return Tine.Webconference.AttendeeGridPanel.prototype.renderAttenderGroupName.apply(this, arguments);
    },
    
    renderAttenderDispContainer: function(displaycontainer_id, metadata, attender) {
        metadata.attr = 'style = "overflow: none;"';
        
        if (displaycontainer_id) {
            if (displaycontainer_id.name) {
                return Ext.util.Format.htmlEncode(displaycontainer_id.name).replace(/ /g,"&nbsp;");
            } else {
                metadata.css = 'x-form-empty-field';
                return this.app.i18n._('No Information');
            }
        }
    },
    
    renderAttenderQuantity: function(quantity, metadata, attender) {
        return quantity > 1 ? quantity : '';
    },
    
    renderAttenderRole: function(role, metadata, attender) {
        var i18n = Tine.Tinebase.appMgr.get('Webconference').i18n,
            renderer = Tine.widgets.grid.RendererManager.get('Webconference', 'Attender', 'role', Tine.widgets.grid.RendererManager.CATEGORY_GRIDPANEL);

        if (this.record && this.record.get('editGrant')) {
            metadata.attr = 'style = "cursor:pointer;"';
        } else {
            metadata.css = 'x-form-empty-field';
        }
        
        return renderer(role);
    },
    
    renderAttenderType: function(type, metadata, attender) {
        metadata.css = 'tine-grid-cell-no-dirty';
        var cssClass = '',
            qtipText =  '',
            userId = attender.get('user_id'),
            hasAccount = userId && ((userId.get && userId.get('account_id')) || userId.account_id);
            
        switch (type) {
            case 'user':
                cssClass = hasAccount || ! userId ? 'renderer_typeAccountIcon' : 'renderer_typeContactIcon';
                qtipText = hasAccount || ! userId ? '' : Tine.Tinebase.appMgr.get('Webconference').i18n._('External Attendee');
                break;
            case 'group':
                cssClass = 'renderer_accountGroupIcon';
                break;
            default:
                cssClass = 'cal-attendee-type-' + type;
                break;
        }
        
        var qtip = qtipText ? 'ext:qtip="' + Tine.Tinebase.common.doubleEncode(qtipText) + '" ': '';
        
        var result = '<div ' + qtip + 'style="background-position:0px;" class="' + cssClass + '">&#160</div>';
        
        if (! attender.get('user_id')) {
            result = Tine.Tinebase.common.cellEditorHintRenderer(result);
        }
        
        return result;
    },
    
    /**
     * disable contents not panel
     */
    setDisabled: function(v) {
        if (v) {
            this.store.filterBy(function(r) {return ! (r.id && typeof r.id === 'string' && r.id.match(/^new-/))});
       } else {
            this.store.clearFilter();
        }
    }
});
