<?php

/**
 * Controller for webconference
 * @package     Webconference
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>, Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */
define('MODERATOR_PW', 'moderatorpw');
define('ATTENDEE_PW', 'attendeepw');

class Webconference_Controller_BigBlueButton extends Tinebase_Controller_Abstract {

    protected $_backend;
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() {
        $this->_backend = Webconference_Backend::factory(Webconference_Backend::BIGBLUEBUTTONAPI);
    }

    /**
     * holds the instance of the singleton
     * 
     * @var Webconference_Controller_BigBlueButton
     */
    private static $_instance = NULL;

    /**
     * the singleton pattern
     * 
     * @return Webconference_Controller_BigBlueButton
     */
    public static function getInstance() {
        if (self::$_instance === NULL) {
            self::$_instance = new Webconference_Controller_BigBlueButton();
        }
        return self::$_instance;
    }

    private function _getAccountId($_email){
	try{
	    return Tinebase_User::getInstance()->getUserByProperty('accountEmailAddress', $_email)->accountId;
	} catch (Exception $e){
	    return null;
	}
    }     

    /**
     * generates a randomstrings of given length
     * 
     * @param int $_length
     */
    public static function getRandomString($_length) {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < (int) $_length; $i++) {
            $randomString .= $chars[mt_rand(1, strlen($chars)) - 1];
        }
        return $randomString;
    }

    /**
     * Checks if the url is online or not.
     * Used to check the webconference link before show the page to the user.
     *
     * @param string $url
     * @return array 
     * @todo Improve the way of checking the url
     */
    public function checkUrl($url) {
        $online = false;
        ini_set("default_socket_timeout", "05");
        set_time_limit(5);
        $f = fopen($url, "r");
        $r = fread($f, 1000);
        fclose($f);
        if (strlen($r) > 1){
            $online = true;
	}
        return $online;
    }
    
    public function getLogoutUrl($protocol) {
        $host = $_SERVER['HTTP_HOST'];
        $base = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
        return $protocol.'//'.$host.$base.'/Webconference/views/logoutPage.html';
    } 
    
    private function _getLogoutUrl($protocol) {
        $host = $_SERVER['HTTP_HOST'];
        $base = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/'));
        return $protocol.'//'.$host.$base.'/Webconference/views/logoutPage.html';
    }
    
    private function _getBigBlueButtonConfigBalance() {
	$data = Webconference_Controller_Config::getInstance()->getAll();
	$quant = 1;
	$bbb = null;
	foreach ($data as $conf){
	    $meetings = $this->_backend->getMeetings($conf->url, $conf->salt);
	    if ($meetings->returncode == 'SUCCESS' && $meetings->messageKey == 'noMeetings'){
		return $conf;
	    }   
	    $meetingsId = array();
	    if ($meetings) {
		foreach ($meetings as $meeting){
		    $meetingId = (String)$meeting["meetingID"];
		    $hasBeenForciblyEnded = (String)$meeting["hasBeenForciblyEnded"];
		    if ($hasBeenForciblyEnded == "true"){
			continue;
		    }
		    array_push($meetingsId, $meetingId);
		}
	    }
	    $perc = ( count($meetingsId) / $conf->limit_room);
	    if ($perc < 1 && $perc < $quant){
		$quant = $perc;
		$bbb = $conf;
	    }
	}
        return $bbb;
    } 
    
    /**
     * This method creates a new meeting
     * 
     * @return String -- URL of the meeting
     */
     public function createMeeting($userName, $meetingID, $title, $protocol){
	 $translation = Tinebase_Translation::getTranslation('Webconference');
	 $logoutURL = $this->_getLogoutUrl($protocol); 
	 $config = $this->_getBigBlueButtonConfigBalance();
	 if ($config == null){
	    throw new Tinebase_Exception_NotFound($translation->_('ERROR (no webconference server available or the room limit has been reached)'));
	}

	if ((!isset($title)) || (trim($title) == ""))
	{
	    $title = $userName . date(" H:i:s d/m/Y");
	}
	
	$welcomeString = sprintf($translation->_("Welcome to the Webconference %s by %s"), $title, $userName);

	$result = $this->_backend->createMeeting($title, $meetingID, $welcomeString, MODERATOR_PW, ATTENDEE_PW, $config->salt, $config->url, $logoutURL); 
	if (!$result) {
	    throw new Tinebase_Exception_NotFound($translation->_('ERROR (the webconference server is unreachable)'));
	}
	if ($result->returncode == 'FAILED') {
	    throw new Tinebase_Exception_NotFound(sprintf($translation->_("ERROR (%s): %s"), $result->messageKey, $result->message));
	}
        //retornar o config_id e o meetingID
	return $config->id;
     }
    
    public function joinURL($meetingID, $userName, $userEmail, $role, $configId){
	$config = Webconference_Controller_Config::getInstance()->get($configId);
	switch ($role){
	    case "MODERATOR":
		$PW = MODERATOR_PW;
		break;
	    case "ATTENDEE":
		$PW = ATTENDEE_PW;
		break;
	}

	$configTokenXML = NULL;
	if (Tinebase_Core::getUser()->hasRight('Webconference', Webconference_Acl_Rights::SHARED_DESKTOP)){
	    $result = $this->setShareDesktop($config->salt, $config->url, $meetingID);
	    if ($result != NULL  & $result->returncode == 'SUCCESS'){
		$configTokenXML = $result->configToken;
	    }
	}
	$roomURL = $this->_backend->joinURL($meetingID, $userName, $PW, $config->salt, $config->url, $configTokenXML);
	return $roomURL;
    }

    private function setShareDesktop($salt, $url, $meetingID) {
	$result = NULL;
	$xml = $this->_backend->getDefaultConfigXML($salt, $url);
	$isShareDesktop = false;
	if ($xml != NULL){
	    foreach ($xml->modules->module as $module){
		if ($module->attributes()->name == 'DeskShareModule'){
		    $isShareDesktop = $module['showButton'] == 'true';
		    $module['showButton'] = 'true';
		}
	    }
	    if (!$isShareDesktop){
		$result = $this->_backend->setConfigXML($meetingID, $salt, $url, $xml);
	    }
	}
	return $result;
    }
    
   /*This method check the BigBlueButton server to see if the meeting is running (i.e. there is someone in the meeting)
    *
    *@param roomName -- the unique meeting identifier used to store the meeting in the bigbluebutton server
    *
    *@return A boolean of true if the meeting is running and false if it is not running
    */
    public function isMeetingActive($_meetingID, $_configId)
    {
	$config = Webconference_Controller_Config::getInstance()->get($_configId);
	return $this->_backend->getMeetingIsActive($_meetingID, MODERATOR_PW, $config->url, $config->salt);
    }
    
        /**
     * This method calls end meeting on the specified meeting in the bigbluebutton server.
     *
     * @param roomName -- the unique meeting identifier used to store the meeting in the bigbluebutton server
     * @param moderatorPassword -- the moderator password of the meeting
     * @return
     * 	- Null if the server is unreachable
     * 	- An array containing a returncode, messageKey, message.
     */
    public function endMeeting($roomID) {
	$room = Webconference_Controller_Room::getInstance()->get($roomID);
	
	$config = Webconference_Controller_Config::getInstance()->get($room->wconf_config_id);
        return $this->_backend->endMeeting($room->room_name, MODERATOR_PW, $config->url, $config->salt);
    }
    
    /**
     * This method calls getMeetings on the bigbluebutton server, then calls getMeetingInfo for each meeting and concatenates the result.
     *
     * @return
     * 	- Null if the server is unreachable
     * 	- If FAILED then returns an array containing a returncode, messageKey, message.
     * 	- If SUCCESS then returns an array of all the meetings. Each element in the array is an array containing a meetingID,
      moderatorPW, attendeePW, hasBeenForciblyEnded, running.
     */
    public function getMeetings() {
        $servers =  Webconference_Controller_Config::getInstance()->getAll();	
	$data = array();
	foreach ($servers as $server){
	    try {
		$dataTemp = $this->_backend->getMeetingsArray($server->url, $server->salt);
	    } catch (Exception $exc) {
		$dataTemp = array("ERROR"=>$exc->getMessage());
	    }
	    array_push($data, array($server->url=>$dataTemp));
	}
        return $data;
    }
    
    /**
     * This method returns an array of the attendees in the specified meeting.
     *
     * @param roomName -- the unique meeting identifier used to store the meeting in the bigbluebutton server
     * @param moderatorPassword -- the moderator password of the meeting
     * @return
     * 	- Null if the server is unreachable.
     * 	- If FAILED, returns an array containing a returncode, messageKey, message.
     * 	- If SUCCESS, returns an array of array containing the userID, fullName, role of each attendee
     */
    public function getRoomUsers($_meetingID, $_configId) {
	$config = Webconference_Controller_Config::getInstance()->get($_configId);
        return $this->_backend->getUsers($_meetingID, MODERATOR_PW, $config->url, $config->salt);
    }
}

