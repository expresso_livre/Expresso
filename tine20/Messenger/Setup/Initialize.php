<?php
/**
 * @package     Messenger
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * class for Messenger Initialization
 *
 * @package     Messenger
 */
class Messenger_Setup_Initialize extends Setup_Initialize
{

    /**
     * Initialize settings
     */
    protected function _initializeSettings()
    {
        $config = Messenger_Config::getInstance();
        $config->set(Messenger_Config::DOMAIN, $config->get(Messenger_Config::DOMAIN));
        $config->set(Messenger_Config::RESOURCE, $config->get(Messenger_Config::RESOURCE));
        $config->set(Messenger_Config::FORMAT, $config->get(Messenger_Config::FORMAT));
        $config->set(Messenger_Config::SERVER_URL, $config->get(Messenger_Config::SERVER_URL));
        $config->set(Messenger_Config::TEMP_FILES, $config->get(Messenger_Config::TEMP_FILES));
        $config->set(Messenger_Config::DISABLE_VIDEO_CHAT, $config->get(Messenger_Config::DISABLE_VIDEO_CHAT));
    }
}