Ext.ns('Tine.Messenger');

// Show Messenger's messages (info, errors, etc)
// in the browsers debugging console
// ex.: Chrome's Developer Tools, Firebug, etc
Tine.Messenger.Log = {
    prefix: 'EXPRESSO MESSENGER: ',
    
    info: function (txt) {
        Tine.log.info(Tine.Messenger.Log.prefix + txt);
    },
    
    error: function (txt) {
        Tine.log.error(Tine.Messenger.Log.prefix + txt);
    },
    
    debug: function (txt) {
        Tine.log.debug(Tine.Messenger.Log.prefix + txt);
    },
    
    warn: function (txt) {
        Tine.log.warn(Tine.Messenger.Log.prefix + txt);
    }
};

Tine.Messenger.LogHandler = {
    
    status: function(title, message, type, resource){
        var showNotfications = showNotfications = Tine.Messenger.registry.get('preferences').get('showNotifications');
        
        resource = resource ? " (" + resource + ")" : "";
        title   = "<span class='title'>" + title + "</span>";
        message = "<span class='body'>" + message + resource + "</span>";
        
        if(type == 'STATUS'){
            showNotfications = Tine.Messenger.registry.get('preferences').get('showNotifications');
        }
        if(type == 'ERROR'){
            //TODO: implement
        }
        if(type == 'INFO'){
            //TODO: implement
        }
        if(type == 'LOG'){
            title = '';
        }
        
        if(showNotfications == 1){
            var handler = $("<div class='msg'>" + title + message + "</div>");
            $("#messenger-loghandler-status").append(handler);
            handler.delay(8000).fadeOut("slow");
        }
        
    },
    
    /**
     *  @method _getPresence
     *  @param presence
     */
    _getPresence: function(presence) {
        var app = Tine.Tinebase.appMgr.get('Messenger');
        var type = $(presence).attr("type"),
            from = $(presence).attr("from"),
            to = $(presence).attr("to"),
            jid = Strophe.getBareJidFromJid(from),
            show = $(presence).find('show'),
            resource;

        if (type !== 'error'){
            if(to !== from){
                if (type != null && type.match(/subscribe/i)) {
                    Tine.Messenger.LogHandler._subscriptionResponse(presence);
                } else {
                    var contact = Tine.Messenger.RosterHandler.getContactElement(jid);
                    if(contact){
                        var title = contact.text || jid;
                        var status = "";
                        var available = false;
                        
                        if(type == 'unavailable'){
                            status = app.i18n._('is unavailable');
                            if (contact.attributes.resources.length == 1)
                                Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_UNAVAILABLE);
			    else if (contact.attributes.resources.length > 1)
				Tine.Messenger.LogHandler.sendProbePresence(jid);
                            resource = Tine.Messenger.RosterTree().removeResource(from);
                            Tine.Messenger.IM.verifyOfflineContactsDisplay(true);
                        } else {
                            resource = Tine.Messenger.RosterTree().setResource(from);
                            var show_text = show.text(),
                                status_text = $(presence).find('status').text() ? 
                                              app.i18n._('Status text')+': '+ $(presence).find('status').text() : '';
                            if(show_text == 'away') {
                                status = app.i18n._('is away');
                                Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_AWAY, '', status_text + " (" + resource + ")");
                            }else if(show_text == 'dnd'){
                                status = app.i18n._('is busy');
                                Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_DONOTDISTURB, '', status_text + " (" + resource + ")");
                            } else if(show_text == 'xa'){
                                status = app.i18n._('auto status (idle)');
                                Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_XA, '', status_text + " (" + resource + ")");
                            } else {
                                console.log('GET PRESENCE 10');
                                $('div.unavailable').show();
                                available = true;
                                status = app.i18n._('is on-line');
                                Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_AVAILABLE, '', status_text + " (" + resource + ")");
                                Tine.Messenger.IM.verifyOfflineContactsDisplay(true);
                            }
                        }
                        if(status && (show.length > 0 || type == 'unavailable' || available)){
                            Tine.Messenger.LogHandler.status(title, status, 'STATUS', resource);
                            Tine.Messenger.LogHandler.onChatStatusChange(from, title + " " + status);
                        }
			
                        var chat = Tine.Messenger.VideoChat.getChatWindow(jid);
			Tine.Messenger.VideoChat.setIconDisabled(chat, jid, type == 'unavailable');
                        if (chat != 'undefined' && chat != null)
                            chat.setFileTransferDisabled(jid, type == 'unavailable');
                    }
                }
            }
        } else {
            var err_msg = $(presence).find('error').children().get(0).tagName,
                message = '';
                
            switch(err_msg){
                case 'recipient-unavailable':
                    message = app.i18n._('The intended recipient is temporarily unavailable.');
                    break;
                case 'remote-server-not-found':
                    message = app.i18n._('The remote server does not exist or could not be reached.');
                    break;
                case 'remote-server-timeout':
                    message = app.i18n._('Communication with the remote server has been interrupted.');
                    break;
                default:
                    message = err_msg;
            }
            Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_UNAVAILABLE, IMConst.SB_WAITING, '', message);
            Tine.Messenger.LogHandler.status(app.i18n._('SERVER ERROR'), message, 'ERROR');
        }

        return true;
    },
    
    /**
     *  @method _subscriptionResponse
     *  @private
     *  @param presence
     */
    _subscriptionResponse: function (presence) {
        var app = Tine.Tinebase.appMgr.get('Messenger');
        var type = $(presence).attr("type"),
            from = $(presence).attr("from"),
            jid = Strophe.getBareJidFromJid(from),
            name = $(presence).attr('name') || $(presence).find('nick').text() || from;
        
        if (type == IMConst.SB_SUBSCRIBED) {
            Tine.Messenger.LogHandler.status(name, app.i18n._('Accept your subscription'));
            Tine.Messenger.RosterTree().updateBuddy(jid, IMConst.ST_AVAILABLE, IMConst.SB_BOTH);
        } else if(type == IMConst.SB_SUBSCRIBE) {
            var buddy = Tine.Messenger.RosterHandler.getContactElement(jid);
            if(buddy == null){
                if (name.contains('@') && Tine.Messenger.registry.get('hideJID')) {
                    name = Tine.Messenger.Util.getFullName(
                            jid,
                            Tine.Messenger.Util.callbackWrapper(Tine.Messenger.LogHandler.subscribeBuddy)
                    );
                } else {
                    Tine.Messenger.LogHandler.subscribeBuddy(jid, name);
                }
            } else {
                Tine.Messenger.LogHandler.sendSubscribeMessage(jid, IMConst.SB_SUBSCRIBED);
            }
        } else {
            var buddy = Tine.Messenger.RosterHandler.getContactElement(from);
            var subsBefore = buddy.ui.textNode.getAttribute('subscription');
            Tine.Messenger.LogHandler.status(name, app.i18n._('Denied/Removed your subscription'), 'INFO');
            Tine.Messenger.RosterTree().updateBuddy(from, IMConst.ST_UNAVAILABLE, IMConst.SB_NONE, '', app.i18n._('Not authorized!'));
            var subsAfter = buddy.ui.textNode.getAttribute('subscription');
            if (subsAfter != subsBefore) {
                Tine.Messenger.ChatHandler.setChatMessage(from,
                    app.i18n._('Your subscription status changed to: ') +
                        app.i18n._(buddy.ui.textNode.getAttribute('subscription')),
                    app.i18n._('Info'),
                    'messenger-notify');
            }
        }
    },
    
    /**
     *  @method subscribeBuddy
     *  @public
     *  @param jid (required)
     *  @param name (required)
     */
    subscribeBuddy: function(jid, name) {
        var app = Tine.Tinebase.appMgr.get('Messenger');
        Ext.Msg.buttonText.yes = app.i18n._('Allow');
        Ext.Msg.buttonText.no = app.i18n._('Deny');
        Ext.Msg.minWidth = 300;
        Ext.Msg.confirm(app.i18n._('Subscription Approval'),
            name + ' ' + app.i18n._('wants to subscribe you.'),
            function (id) {
                var response;
                if (id == 'yes') {
                    Tine.Messenger.Window.AddBuddyWindow(jid, name);
                    Tine.Messenger.RosterHandler.renameContact(jid, name);
                    response = IMConst.SB_SUBSCRIBED;
                } else if (id == 'no') {
                    response = IMConst.SB_UNSUBSCRIBED;
                }
                Tine.Messenger.LogHandler.sendSubscribeMessage(jid, response);
            }
         );
    },

    /**
     *  @method sendSubscribeMessage
     *  @public
     *  @param jid (required)
     *  @param type (required) <b>subscribe</b> or <b>subscribed</b> 
     *                      or <b>unsubscribe</b> or <b>unsubscribed</b>
     */
    sendSubscribeMessage: function(jid, type){
        if(type == 'subscribe' || type == 'subscribed' || 
           type == 'unsubscribe' || type == 'unsubscribed')
        {
            var conn = Tine.Tinebase.appMgr.get('Messenger').getConnection();
            conn.send($pres({to: jid, name: Tine.Tinebase.registry.get('currentAccount').accountDisplayName, type: type}).c('priority').t('10'));
        }
    },
    
    sendProbePresence: function(jid){
	var conn = Tine.Tinebase.appMgr.get('Messenger').getConnection();
            conn.send($pres({to: jid, type: 'probe'}));
    },
    
    _onErrorMessage: function(message){
        var app = Tine.Tinebase.appMgr.get('Messenger');
        var raw_jid = $(message).attr("from");
        var jid = Strophe.getBareJidFromJid(raw_jid);
        
        var body = $(message).find("html > body");
        if (body.length === 0) {
            body = $(message).find("body");
        }
        if(body.length > 0){
            Tine.Messenger.ChatHandler.setChatMessage(jid, app.i18n._('Error sending: ') + body.text(), app.i18n._('Error'), 'messenger-notify');
        }
        Tine.Messenger.Log.error(app.i18n._('Error number ') + $(message).children("error").attr("code"));
        
        return true;
    },
    
    _onError: function(_iq){
        var app = Tine.Tinebase.appMgr.get('Messenger');
        var err_msg = $(_iq).find('error').children().get(0).tagName,
            message = '';

        switch(err_msg){
            case 'item-not-found':
                message = app.i18n._('Cancel 404: The item was not found.');
                break;
            case 'feature-not-implemented':
                message = app.i18n._('Cancel 501: The feature was not implemented.');
                break;
            case 'internal-server-error':
                message = app.i18n._('Wait 500: Internal server error.');
                break;
            case 'service-unavailable':
                message = app.i18n._('Cancel 503: Service unavailable.');
                break;
            default:
                message = err_msg;
        }
        Tine.Messenger.LogHandler.status(app.i18n._('SERVER ERROR'), message, 'ERROR');
        Tine.Messenger.Log.error(app.i18n._('Error number ') + $(_iq).children("error").attr("code"));
        
        return true;
    },
    
    onChatStatusChange: function(raw_jid, status){
        var app = Tine.Tinebase.appMgr.get('Messenger');
        var jid = Strophe.getBareJidFromJid(raw_jid);
        var resource = Strophe.getResourceFromJid(raw_jid);
        var chat_id = Tine.Messenger.ChatHandler.formatChatId(jid);
        
        if(Ext.getCmp(chat_id)){
            Tine.Messenger.ChatHandler.setChatMessage(jid, status + ' (' + resource + ')', app.i18n._('Info'), 'messenger-notify');
        }
        
        return true;
    }
    
};
