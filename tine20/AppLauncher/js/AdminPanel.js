/*
 * Tine 2.0
 *
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 *
 */

Ext.namespace('Tine.AppLauncher');


/**
 * admin settings panel
 *
 * @namespace   Tine.AppLauncher
 * @class       Tine.AppLauncher.AdminPanel
 * @extends     Tine.widgets.dialog.EditDialog
 *
 * <p>AppLauncher Admin Panel</p>
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>

 *
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Messenger.AdminPanel
 */
Tine.AppLauncher.AdminPanel = Ext.extend(Tine.widgets.dialog.EditDialog, {

    appName: 'AppLauncher',
    recordClass: Tine.AppLauncher.Model.Settings,
    recordProxy: Tine.AppLauncher.settingsBackend,
    evalGrants: false,

    domainCombo: null,

    onRecordLoad: function () {
        Tine.AppLauncher.AdminPanel.superclass.onRecordLoad.call(this);
        this.window.setTitle(String.format(_('Change settings for application {0}'), this.appName));
        this.onDomainChange();
    },

    updateToolbars: function () {
    },
    /**
     * is called from onApplyChanges
     * @param {Boolean} closeWindow
     */
    doApplyChanges: function (closeWindow) {
        // we need to sync record before validating to let (sub) panels have
        // current data of other panels
        this.onRecordUpdate();

        // quit copy mode
        this.copyRecord = false;

        if (this.isValid()) {
            Ext.Ajax.request({
                params: {
                    method: 'AppLauncher.saveSettings',
                    _data: this.record.data
                },
                scope: this,
                success: function (_result, _request) {

                    Tine.AppLauncher.registry.replace('domain', this.record.data.domain);
                    Tine.AppLauncher.registry.replace('adminlist', this.record.data.adminlist);

                    if (!Ext.isFunction(this.window.cascade)) {

                        // update form with this new data
                        // NOTE: We update the form also when window should be closed,
                        //       cause sometimes security restrictions might prevent
                        //       closing of native windows
                        this.onRecordLoad();
                    }
                    var ticketFn = this.onAfterApplyChanges.deferByTickets(this, [closeWindow]),
                            wrapTicket = ticketFn();

                    this.fireEvent('update', Ext.util.JSON.encode(this.record.data), this.mode, this, ticketFn);
                    wrapTicket();
                },
                failure: this.onRequestFailed,
                timeout: 300000 // 5 minutes

            });


        } else {
            this.saving = false;
            this.loadMask.hide();
            Ext.MessageBox.alert(_('Errors'), this.getValidationErrorMessage());
        }
    },

    /**
     * Returns form items
     * @returns {AdminPanelAnonym$0.getFormItems.AdminPanelAnonym$6}
     */
    getFormItems: function () {

        var commonComboConfig = {
            xtype: 'combo',
            listWidth: 300,
            mode: 'local',
            forceSelection: true,
            allowEmpty: false,
            triggerAction: 'all',
            editable: false,
            tabIndex: this.getTabIndex
        };

        this.domainCombo = new Ext.form.ComboBox(Ext.applyIf({
            id: 'domainComboBox',
            name: 'domain',
            fieldLabel: this.app.i18n._('Domain'),
            store: [['outros', this.app.i18n._('Outros')], ['planejamento', this.app.i18n._('Planejamento')]],
            value: 'outros',
            listeners: {
                scope: this,
                change: this.onDomainChange,
                select: this.onDomainChange
            }
        }, commonComboConfig));

        return {
            layout: 'form',
            border: false,
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'fieldset',
                    autoHeight: 'auto',
                    defaults: {width: 300},
                    title: this.app.i18n._('Application Launcher Settings'),
                    id: 'setup-applauncher-service-group',
                    items: [
                        this.domainCombo,
                        {
                            allowBlank: true,
                            id: 'adminlistTextField',
                            name: 'adminlist',
                            fieldLabel: this.app.i18n._('Admin List'),
                            xtype: 'textfield'
                        }
                    ]
                }
            ]
        };
    },

    /**
     * Disables Admin List TextBox when domain is not "planejamento"
     * @returns void
     */
    onDomainChange: function () {
        var domain = this.domainCombo.getValue();

        var adminlistTextField = Ext.getCmp('adminlistTextField');
        if (domain == 'planejamento') {
            adminlistTextField.setDisabled(false);
        } else {
            adminlistTextField.setValue("");
            adminlistTextField.setDisabled(true);
        }
    }

});

/**
 * AppLauncher admin settings popup
 *
 * @param   {Object} config
 * @return  {Ext.ux.Window}
 */
Tine.AppLauncher.AdminPanel.openWindow = function (config) {
    var window = Tine.WindowFactory.getWindow({
        width: 600,
        height: 400,
        id: 'applauncher-admin-panel',
        name: 'applauncher-admin-panel',
        contentPanelConstructor: 'Tine.AppLauncher.AdminPanel',
        contentPanelConstructorConfig: config
    });
    return window;
};