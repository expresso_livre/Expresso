<?php
/**
 * Tine 2.0
 * @package     AppLauncher
 * @subpackage  Frontend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Rommel Cysne <rommel.cysne@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */

/**
 *
 * This class handles all Json requests for the AppLauncher application
 *
 * @package     AppLauncher
 * @subpackage  Frontend
 */
class AppLauncher_Frontend_Json extends Tinebase_Frontend_Json_Abstract
{
    /**
     * the controller
     *
     * @var AppLauncher_Controller
     */
    protected $_controller = NULL;
    
    /**
     * the models handled by this frontend
     * @var array
     */
    protected $_models = array('AppLauncherRecord');

    /**
     * the constructor
     *
     */
    public function __construct()
    {
        $this->_applicationName = 'AppLauncher';
        $this->_controller = AppLauncher_Controller::getInstance();
    }

    /**
     * Return app settings
     *
     * @return array
     */
    public function getSettings()
    {
        $settings = $this->_controller->getSettings();
        return array(
            'domain' => $settings[AppLauncher_Config::DOMAIN],
            'adminlist' => $settings[AppLauncher_Config::EXTERNAL_APPLICATIONS]['adminlist']
        );
    }

    /**
     * Save settings
     *
     * @param array $settings
     * @return array
     */
    public function saveSettings(array $_data)
    {
        $settings = array(
            AppLauncher_Config::DOMAIN => $_data[AppLauncher_Config::DOMAIN],
            AppLauncher_Config::EXTERNAL_APPLICATIONS => array(
                'adminlist' => $_data['adminlist'] ? $_data['adminlist'] : ''
            )
        );

        $result = $this->_controller->saveSettings($settings);
        return array(
                'status' => $result ? 'success' : 'failure'
            );
    }

    /**
     * Returns registry data
     * 
     * @return array
     */
    public function getRegistryData()
    {
        return $this->getSettings();
    }

}
