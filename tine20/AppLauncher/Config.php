<?php
/**
 * @package     AppLauncher
 * @subpackage  Config
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * AppLauncher config class
 * 
 * @package     AppLauncher
 * @subpackage  Config
 */
class AppLauncher_Config extends Tinebase_Config_Abstract
{
    /**
     * DOMAIN
     * 
     * @var string
     */
    const DOMAIN = 'domain';

    /**
     * APPLICATIONS
     *
     * @var string
     */
    const EXTERNAL_APPLICATIONS = 'externalApplications';

    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Definition::$_properties
     */
    protected static $_properties = array(
        self::DOMAIN => array(
                                    //_('Expecify the domain for AppLauncher')
            'label'                 => 'Expecify the domain for AppLauncher',
                                    //_('Expecify the domain for AppLauncher')
            'description'           => 'Expecify the domain for AppLauncher',
            'type'                  => Tinebase_Config_Abstract::TYPE_STRING,
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => 'outros',
        ),
        self::EXTERNAL_APPLICATIONS => array(
                                   //_('External applications')
            'label'                 => 'External applications',
                                   //_('External applications')
            'description'           => 'External applications',
            'type'                  => 'object',
            'class'                 => 'Tinebase_Config_Struct',
            'clientRegistryInclude' => FALSE,
            'setByAdminModule'      => TRUE,
            'setBySetupModule'      => FALSE,
            'default'               => array(
                'adminlist' => ''
            )
        ),
    );
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::$_appName
     */
    protected $_appName = 'AppLauncher';
    
    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Config
     */
    private static $_instance = NULL;
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */    
    private function __construct() {}
    
    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */    
    private function __clone() {}
    
    /**
     * Returns instance of Tinebase_Config
     *
     * @return Tinebase_Config
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    /**
     * (non-PHPdoc)
     * @see tine20/Tinebase/Config/Abstract::getProperties()
     */
    public static function getProperties()
    {
        return self::$_properties;
    }
}