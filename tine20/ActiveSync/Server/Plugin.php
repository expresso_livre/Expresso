<?php
/**
 * Tine 2.0
 *
 * @package     ActiveSync
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * plugin server
 *
 * @package     ActiveSync
 * @subpackage  Server
 */
class ActiveSync_Server_Plugin extends Tinebase_Server_Plugin_Abstract
{
    /**
     * Default Http Server class name
     */
    const ACTIVESYNC_SERVER_HTTP = 'ActiveSync_Server_Http';

    /**
     * (non-PHPdoc)
     * @see Tinebase_Server_Plugin_Interface::getServer()
     */
    public static function getServer(\Zend\Http\Request $request)
    {
        if ((isset($_SERVER['REDIRECT_ACTIVESYNC']) && $_SERVER['REDIRECT_ACTIVESYNC'] == 'true') || // legacy
            ($request->getQuery('frontend') === 'activesync')) {
            $server = static::_getServer(self::ACTIVESYNC_SERVER_HTTP);
            Tinebase_Core::set(Tinebase_Core::SERVER_CLASS_NAME, get_class($server));
            return $server;
        }
    }
}
