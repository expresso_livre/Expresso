<?php
/**
 * @package     Tinebase
 * @subpackage  Config
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014-2016 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * config manager
 *
 * @package     Tinebase
 * @subpackage  Config
 */
final class Tinebase_Config_Manager
{
    /**
     *
     * @var Tinebase_Config_Manager
     */
    private static $_instance = NULL;

    /**
     *
     * @var boolean
     */
    private static $_multidomain = FALSE;

    /**
     * Singleton design pattern
     *
     * @return Tinebase_Config_Manager
     */
    public static function getInstance()
    {
        if (NULL == self::$_instance){
            self::$_instance = new self();
        }
        return static::$_instance;
    }

    /**
     * Remove configuration for an application
     *
     * @param Tinebase_Model_Application $application
     * @return number (records deleted)
     */
    public function deleteConfigByApplication(Tinebase_Model_Application $application)
    {
        if (self::$_multidomain){
            return Tinebase_Config::getInstance()->deleteConfigByApplicationName($application->name);
        }
        return Tinebase_Config::getInstance()->deleteConfigByApplicationId($application->getId());
    }

    /**
     * try to get domain name
     *
     * @param string $fromAccountLoginName
     */
    public function detectDomain($fromAccountLoginName = FALSE)
    {
        if (self::$_multidomain && !$fromAccountLoginName){
            $config = new Zend_Config(include Tinebase_Config::getGlobalConfigFileNamePath());
            if(isset($config->domaindata->domain)) {
                Tinebase_Config::setDomain($config->domaindata->domain);
            }
            Setup_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Detected domain to install: ' . $config->domain);
        }
    }

    /**
     * (non-PHPdoc)
     * @see Tinebase_Config_Manager_Interface::getAccountsStorageData()
     */
    public function getAccountsStorageData()
    {
        if (self::$_multidomain){
            return $this->_getAccountsStorageDataForMultidomain();
        }
        return $this->_getAccountsStorageDataForDefault();
    }

    /**
     * (non-PHPdoc)
     * @see Tinebase_Config_Manager_Interface::getAuthProviderData()
     */
    public function getAuthProviderData()
    {
        if (self::$_multidomain){
            return $this->_getAuthProviderDataForMultidomain();
        }
        return $this->_getAuthProviderDataForDefault();
    }

    /**
     * List of enabled application as text
     *
     * @return array
     */
    public function getEnabledApplications()
    {
        if (!self::$_multidomain || (Tinebase_Session::isStarted() && Zend_Auth::getInstance()->hasIdentity())) {
            return Tinebase_Application::getInstance()->getApplicationsByState(Tinebase_Application::ENABLED)->name;
        }
        return array();
    }

    /**
     *
     * @param string $username
     * @return Tinebase_Model_User | NULL
     */
    public function getUserFromSyncBackend($username)
    {
        if(Tinebase_User::getConfiguredBackend() == Tinebase_User::LDAP) {
            $accountUser = Tinebase_User::getInstance();
            if (is_object($accountUser)){
                $emailValidator = new Zend_Validate_EmailAddress();
                if(self::$_multidomain && $emailValidator->isValid($username)) {
                    $user = $accountUser->getUserByPropertyFromSyncBackend('accountEmailAddress', $username, 'Tinebase_Model_FullUser');
                } else {
                    $user = $accountUser->getUserByPropertyFromSyncBackend('accountLoginName', $username, 'Tinebase_Model_FullUser');
                }
                return $user;
            }
        }
        return null;
    }

    /**
     * Get username
     *
     * @param string $username
     * @return string
     */
    public function getUsername($username)
    {
        if ($username instanceof Tinebase_Model_FullUser) {
            $username = (self::$_multidomain ? $username->accountEmailAddress : $username->accountLoginName);
        }
        return $username;
    }

    /**
     *
     * @param Tinebase_Model_FullUser | NULL $user
     * @return boolean
     */
    public function isMultidomainUser($user)
    {
        if(static::$_multidomain && $user != NULL) {
            Tinebase_Config::setDomainFromAccountLoginName(Tinebase_Core::getUser()->accountEmailAddress);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * (non-PHPdoc)
     * @see Tinebase_Config_Manager_Interface::setDomainFromAccountLoginName()
     */
    public function setDomainFromAccountLoginName($accountLoginName)
    {
        if (self::$_multidomain){
            Tinebase_Config::setDomainFromAccountLoginName($accountLoginName);
        }
    }

    /**
     * (non-PHPdoc)
     * @see Tinebase_Config_Manager_Interface::setupConfigFileExists()
     */
    public function setupConfigFileExists()
    {
        return $this->_adapter->setupConfigFileExists();
    }

    /**
     * Remove unused free time from database if is not multidomain
     */
    public function removeUnusedFreeTime()
    {
        if (self::$_multidomain) return;
        $cb = new Tinebase_Backend_Sql(array(
            'modelName' => 'Tinebase_Model_Config',
            'tableName' => 'config',
        ));

        $filter = new Tinebase_Model_ConfigFilter(array(
                array('field' => 'name', 'operator' => 'equals', 'value' => HumanResources_Config::FREETIME_TYPE)
        ));
        $record = $cb->search($filter)->getFirstRecord();
        $result = json_decode($record->value);
        $newResult = array('name' => HumanResources_Config::FREETIME_TYPE);
        foreach($result->records as $field) {
            if ($field->id == 'VACATION_EXTRA' || $field->id == 'VACATION_REMAINING') {
                continue;
            }
            $newResult['records'][] = $field;
        }
        $record->value = json_encode($newResult);
        $cb->update($record);
    }

    /**
     * @param Zend_Console_GetOpt $opts
     * @throws Tinebase_Exception
     */
    public function thrownExceptionForDomain($opts)
    {
        if (!self::$_multidomain) return;
        if (!isset($opts->domain)) {
            throw new Tinebase_Exception('Domain is not set. See help for details.');
        } elseif (!in_array($opts->domain, self::getDomainNames())) {
            throw new Tinebase_Exception('Domain "'. $opts->domain .'" does not exist.');
        }        
    }

    /**
     * Checks if provided application is installed
     *
     * @throws Tinebase_Exception
     */
    public function checkApplicationInstalled($applicationName)
    {
        if(self::$_multidomain && !Tinebase_Config::hasDomain()){
            return TRUE;
        }

        if(!Tinebase_Application::getInstance()->isInstalled($applicationName)) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: $applicationName is not installed.");
            throw new Tinebase_Exception_NotInstalled("$applicationName is not installed");
        }
    }

    /**
     * Checks if database configuration is right
     *
     * @throws Tinebase_Exception_Backend
     */
    public function checkDatabaseConfiguration()
    {
        if(self::$_multidomain && !Tinebase_Config::hasDomain()) {
            return;
        }

        try {
            Tinebase_Core::getDb();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Cannot create database connection. Check database configuration.");
            throw new Tinebase_Exception_Backend("Cannot create database connection. Please check database configuration");
        }
        
    }

    /**
     * Verify if environment is set
     *
     * @param $username string
     * @return array(boolean,string,integer) | TRUE
     */
    public function isEnvironmentSet($username)
    {
        if (self::$_multidomain){
            try {
                Tinebase_Config::setDomainFromAccountLoginName($username);
            } catch (Tinebase_Exception $ex) {
                Tinebase_Core::getLogger()->warn(__METHOD__ . "::" . __LINE__ . ":: The username $username informed contain an invalid domain.");
                return array(
                    'success' => FALSE,
                    'errorMessage' => 'The email address informed has an invalid domain.',
                    'errorCode' => -2
                );
            }
        }

        try {
            $this->checkDatabaseConfiguration();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: ".$ex->getMessage());
            return array(
                'success' => FALSE,
                'errorMessage' => 'The server is currently unable to handle the request due to a temporary overloading, maintenance or misconfiguration of the server. Please try again or contact your administrator.',
                'errorCode' => -2
            );
        }

        try {
            $this->checkTinebaseInstalled();
        } catch(Exception $ex) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: ".$ex->getMessage());
            return array(
                'success'      => FALSE,
                'errorMessage' => 'Tine 2.0 needs to be updated or is not installed yet.',
                'errorCode' => -2
            );
        }

        // check if setup/update required
        try {
            $setupController = Setup_Controller::getInstance();
            $applications = Tinebase_Application::getInstance()->getApplications();
            foreach ($applications as $application) {
                if ($application->status == 'enabled' && $setupController->updateNeeded($application)) {
                    Tinebase_Core::getLogger()->DEBUG(__CLASS__ . '::' . __METHOD__ . ' (' . __LINE__ .") Update/Setup required for {$application->name}!");
                    throw new Tinebase_Exception_Update('Tine 2.0 needs to be updated or is not installed yet. Please wait or contact your administrator.');
                }
            }
        } catch (Exception $teu) {
            return array(
                'sucess'        => FALSE,
                'errorMessage'  => $teu->getMessage(),
                'errorCode' => 0
            );
        }

        return TRUE;
    }


    /**
     * Checks if Tinebase is installed
     *
     * @throws Tinebase_Exception_Backend
     */
    public function checkTinebaseInstalled()
    {
        if(self::$_multidomain && !Tinebase_Config::hasDomain()){
            return;
        }
        if(!Tinebase_Application::getInstance()->isInstalled('Tinebase')) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Tinebase is not installed.");
            throw new Tinebase_Exception_NotInstalled("Tinebase is not installed");
        }
    }

    /**
     *
     * @param boolean $enabled
     */
    public static function setMultidomain($enabled)
    {
        static::$_multidomain = $enabled;
    }

    /**
     *
     * @return boolean
     */
    public static function isMultidomain()
    {
        return static::$_multidomain;
    }

    /**
     * Deal with different formats for login name
     *
     * @param string $loginName
     * @param string $requestType
     * @return string
     */
    public function getRealLoginName($loginName, $requestType)
    {
        if ($requestType == ActiveSync_Server_Http::REQUEST_TYPE){
            $domain = substr($loginName, 0, strrpos($loginName, '\\'));
            if (Tinebase_Config_Manager::isMultidomain() && strpos($loginName, "@") === false) {
                $loginName = $loginName . '@' . $domain;
            }
            Tinebase_Config::setDomainFromAccountLoginName($loginName);
        }
        return $loginName;
    }

    /**
     * Applications whose Javascript files must be loaded
     *
     * @var array
     */
    protected static $_enabledApplications = array();

    /**
     * return accounts storage data
     *
     * @return array
     */
    private function _getAccountsStorageDataForDefault()
    {
        $result = array();
        try {
            $result['backend'] = Tinebase_User::getConfiguredBackend();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->info(__METHOD__ . "::" . __LINE__ . ":: Cannot get user backend. Setting default.");
            $result['backend'] = Tinebase_User::SQL;
        }

        try {
            $backendConfiguration = Tinebase_User::getBackendConfigurationWithDefaults();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->info(__METHOD__ . "::" . __LINE__ . ":: Cannot get user backend configuration. Setting void");
            $backendConfiguration = array();
        }

        return array_merge($result, $backendConfiguration);
    }

    /**
     * return accounts storage data
     *
     * @return array
     */
    private function _getAccountsStorageDataForMultidomain()
    {
        $result = array();
        if (isset(Tinebase_Config::getInstance()->Tinebase_User_BackendType)) {
            $backend = Tinebase_Config::getInstance()->Tinebase_User_BackendType;
        } else {
            $backend = (Setup_Core::get(Setup_Core::CHECKDB)) ? Tinebase_User::getConfiguredBackend() : Tinebase_User::SQL;
        }
        $result['backend'] = $backend;

        if (isset(Tinebase_Config::getInstance()->Tinebase_User_BackendConfiguration)) {
            $result[$backend] = Tinebase_Config::getInstance()->Tinebase_User_BackendConfiguration->toArray();
        } else {
            $result = array_merge($result, Tinebase_User::getBackendConfigurationWithDefaults(Setup_Core::get(Setup_Core::CHECKDB)));
        }
        return $result;
    }

    /**
     * return authentication provider data
     *
     * @return array
     */
    private function _getAuthProviderDataForDefault()
    {
        $result = array();
        try {
            $result['backend'] = Tinebase_Auth::getConfiguredBackend();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->info(__METHOD__ . "::" . __LINE__ . ":: Cannot get auth backend. Setting default.");
            $result['backend'] = Tinebase_Auth::SQL;
        }

        try {
            $backendConfiguration = Tinebase_Auth::getBackendConfigurationWithDefaults();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->info(__METHOD__ . "::" . __LINE__ . ":: Cannot get auth configuration. Setting void");
            $backendConfiguration = array();
        }

        return array_merge($result, $backendConfiguration);
    }

    /**
     * return authentication provider data
     *
     * @return array
     */
    private function _getAuthProviderDataForMultidomain()
    {
        $result = array();
        if (isset(Tinebase_Config::getInstance()->Tinebase_Authentication_BackendType)) {
            $backend = Tinebase_Config::getInstance()->Tinebase_Authentication_BackendType;
        } else {
            $backend = (Setup_Core::get(Setup_Core::CHECKDB)) ? Tinebase_Auth::getConfiguredBackend() : Tinebase_Auth::SQL;
        }
        $result['backend'] = $backend;
        if (isset(Tinebase_Config::getInstance()->Tinebase_Authentication_BackendConfiguration)){
            $result[$backend] = Tinebase_Config::getInstance()->Tinebase_Authentication_BackendConfiguration->toArray();
        } else {
            $result[$backend] = array();
        }
        return $result;
    }

    /**
     * Returns an array with all domain names
     *
     * @return String
     */
    public static function getDomainNames()
    {
        $domainsPath = realpath(__DIR__ . '/../..') . DIRECTORY_SEPARATOR . 'domains' . DIRECTORY_SEPARATOR;
        $domainNames = scandir($domainsPath);
        $domainNames = array_values(array_diff($domainNames, array('.', '..')));
        $validNames = array();
        foreach($domainNames as $domainName){ // validate domain pattern
            $match = preg_match('/^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$/', $domainName);
            if (!empty($match)){
                $validNames[] = $domainName;
            }
        }
        $domainNames = $validNames;
        return $domainNames;
    }

    /**
     * Return email config data
     * @return array
     */
    public function getEmailConfig()
    {
        try {
            $this->checkTinebaseInstalled();
        } catch (Exception $ex) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . "::" . __LINE__ . ":: Tinebase is not installed");
            return array();
        }
        return Setup_Controller::getInstance()->getEmailConfig();
    }

    /**
     * Error message
     * @return string
     */
    public function getLoginErrorMessage()
    {
        return ($this->isMultidomain() ? 'Wrong email or password!' : 'Wrong username or password!');
    }
}
