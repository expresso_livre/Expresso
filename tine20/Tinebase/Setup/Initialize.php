<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Jonas Fischer <j.fischer@metaways.de>
 * @copyright   Copyright (c) 2008-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * class for Tinebase initialization
 * 
 * @package     Tinebase
 * @subpackage  Setup
 */
class Tinebase_Setup_Initialize extends Setup_Initialize
{
    /**
     * Override method: Tinebase needs additional initialisation
     * 
     * @see tine20/Setup/Setup_Initialize#_initialize($_application)
     */
    public function _initialize(Tinebase_Model_Application $_application, $_options = null)
    {
        $this->_initProcedures();

        $this->_setupConfigOptions($_options);
        $this->_setupGroups();
        
        Tinebase_Acl_Roles::getInstance()->createInitialRoles();
        
        parent::_initialize($_application, $_options);
    }

    /**
     * Initializes database procedures if they exist
     */
    protected function _initProcedures()
    {
        $backend = Setup_Backend_Factory::factory();
        $dbCommand = Tinebase_Backend_Sql_Command::factory(Tinebase_Core::getDb());
        $dbCommand->initProcedures($backend);
    }

    /**
     * set config options (accounts/authentication/email/...)
     * 
     * @param array $_options
     */
    protected function _setupConfigOptions($_options)
    {
        // ignore empty options
        foreach($_options as $key => $value) {
            if (empty($_options[$key])) {
                unset($_options[$key]);
            }
        }

        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Saving config options (accounts/authentication/email/...)');
        
        // this is a dangerous TRACE as there might be passwords in here!
        //if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($_options, TRUE));

        $defaults = empty($_options['authenticationData']) ? Setup_Controller::getInstance()->loadAuthenticationData() : $_options['authenticationData'];

        $defaultGroupNames = $this->_parseDefaultGroupNameOptions($defaults['accounts'][Tinebase_User::getConfiguredBackend()]);
        $defaults['accounts'][Tinebase_User::getConfiguredBackend()] = array_merge($defaults['accounts'][Tinebase_User::getConfiguredBackend()], $defaultGroupNames);
        
        $emailConfigKeys = Setup_Controller::getInstance()->getEmailConfigKeys();
        $configsToSet = array_merge($emailConfigKeys, array('authentication', 'accounts', 'redirectSettings', 'acceptedTermsVersion'));
        
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($configsToSet, TRUE));
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($defaults, TRUE));
        
        $optionsToSave = array();
        foreach ($configsToSet as $group) {
            if (isset($_options[$group])) {
                $parsedOptions = (is_string($_options[$group])) ? Setup_Frontend_Cli::parseConfigValue($_options[$group]) : $_options[$group];

                switch ($group) {
                    case 'authentication':
                    case 'accounts':
                        $backend = (isset($parsedOptions['backend'])) ? ucfirst($parsedOptions['backend']) : $defaults[$group]['backend'];
                        $optionsToSave[$group][$backend] = array_merge($defaults[$group][$backend], (isset($parsedOptions[$backend])) ? $parsedOptions[$backend] : $parsedOptions);
                        $optionsToSave[$group]['backend'] = $backend;
                        break;
                    default:
                        $optionsToSave[$group] = $parsedOptions;
                }
            } else if (isset($defaults[$group])) {
                $optionsToSave[$group] = $defaults[$group];
            }
        }
        
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . ' ' . print_r($optionsToSave, TRUE));
        
        Setup_Controller::getInstance()->saveEmailConfig($optionsToSave);
        Setup_Controller::getInstance()->saveAuthentication($optionsToSave);
    }
    
    /**
    * Extract default group name settings from {@param $_options}
    *
    * @param array $_options
    * @return array
    */
    protected function _parseDefaultGroupNameOptions($_options)
    {
        $adminGroupName = Tinebase_Group::DEFAULT_ADMIN_GROUP;
        if((isset($_options[Tinebase_User::DEFAULT_ADMIN_GROUP_NAME_KEY]) &&
                $_options[Tinebase_User::DEFAULT_ADMIN_GROUP_NAME_KEY])) {
            $adminGroupName = $_options[Tinebase_User::DEFAULT_ADMIN_GROUP_NAME_KEY];
        }

        $userGroupName = Tinebase_Group::DEFAULT_USER_GROUP;
        if((isset($_options[Tinebase_User::DEFAULT_USER_GROUP_NAME_KEY]) &&
                $_options[Tinebase_User::DEFAULT_USER_GROUP_NAME_KEY])) {
            $userGroupName = $_options[Tinebase_User::DEFAULT_USER_GROUP_NAME_KEY];
        }

        $result = array(
            'defaultAdminGroupName' => $adminGroupName,
            'defaultUserGroupName'  => $userGroupName,
        );
        
        return $result;
    }
    
    /**
     * import groups(ldap)/create initial groups(sql)
     *
     * @todo allow to configure if groups should be synced?
     */
    protected function _setupGroups()
    {
        if (Tinebase_Group::getInstance() instanceof Tinebase_Group_Interface_SyncAble) {
            Tinebase_Group::syncGroups();
        } else {
            Tinebase_Group::createInitialGroups();
        }
    }
    
    /**
     * Override method because this app requires special rights
     * @see tine20/Setup/Setup_Initialize#_createInitialRights($_application)
     * 
     * @todo make hard coded role name ('user role') configurable
     */
    protected function _createInitialRights(Tinebase_Model_Application $_application)
    {
        parent::_createInitialRights($_application);

        $roles = Tinebase_Acl_Roles::getInstance();
        $userRole = $roles->getRoleByName('user role');
        $roles->addSingleRight(
            $userRole->getId(), 
            $_application->getId(), 
            Tinebase_Acl_Rights::CHECK_VERSION
        );
        $roles->addSingleRight(
            $userRole->getId(), 
            $_application->getId(), 
            Tinebase_Acl_Rights::REPORT_BUGS
        );
        $roles->addSingleRight(
            $userRole->getId(), 
            $_application->getId(), 
            Tinebase_Acl_Rights::MANAGE_OWN_STATE
        );
    }
    
    /**
     * init scheduler tasks
     */
    protected function _initializeSchedulerTasks()
    {
        $scheduler = Tinebase_Core::getScheduler();
        
        Tinebase_Scheduler_Task::addAlarmTask($scheduler);
        Tinebase_Scheduler_Task::addCacheCleanupTask($scheduler);
        Tinebase_Scheduler_Task::addCredentialCacheCleanupTask($scheduler);
        Tinebase_Scheduler_Task::addTempFileCleanupTask($scheduler);
        Tinebase_Scheduler_Task::addDeletedFileCleanupTask($scheduler);
        Tinebase_Scheduler_Task::addSessionsCleanupTask($scheduler);
        Tinebase_Scheduler_Task::addAccessLogCleanupTask($scheduler);
    }
}
