<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2015 Serpro (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * plugin server
 *
 * @package     Tinebase
 * @subpackage  Server
 */
class Tinebase_Server_Plugin_WebDAV extends Tinebase_Server_Plugin_Abstract
{
    /**
     * Default Http Server class name
     */
    const TINEBASE_SERVER_WEBDAV = 'Tinebase_Server_WebDAV';

    /**
     * (non-PHPdoc)
     * @see Tinebase_Server_Plugin_Interface::getServer()
     */
    public static function getServer(\Zend\Http\Request $request)
    {
        /**************************** WebDAV / CardDAV / CalDAV API **********************************
         * RewriteCond %{REQUEST_METHOD} !^(GET|POST)$
        * RewriteRule ^/$            /index.php?frontend=webdav [E=REMOTE_USER:%{HTTP:Authorization},L,QSA]
        *
        * RewriteRule ^/addressbooks /index.php?frontend=webdav [E=REMOTE_USER:%{HTTP:Authorization},L,QSA]
        * RewriteRule ^/calendars    /index.php?frontend=webdav [E=REMOTE_USER:%{HTTP:Authorization},L,QSA]
        * RewriteRule ^/principals   /index.php?frontend=webdav [E=REMOTE_USER:%{HTTP:Authorization},L,QSA]
        * RewriteRule ^/webdav       /index.php?frontend=webdav [E=REMOTE_USER:%{HTTP:Authorization},L,QSA]
        */
        if ($request->getQuery('frontend') === 'webdav') {
            $server = static::_getServer(self::TINEBASE_SERVER_WEBDAV);
            Tinebase_Core::set(Tinebase_Core::SERVER_CLASS_NAME, get_class($server));
            return $server;
        }
    }
}
