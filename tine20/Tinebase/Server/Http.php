<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 *
 */

/**
 * HTTP Server class with handle() function
 *
 * @package     Tinebase
 * @subpackage  Server
 */
class Tinebase_Server_Http extends Tinebase_Server_Abstract implements Tinebase_Server_Interface
{
    /**
     * Tinebase Frontend namespace
     */
    CONST TINEBASE_FRONTEND_NAMESPACE = 'Tinebase';

    /**
     * Tinebase Frontend Class
     */
    CONST TINEBASE_FRONTEND_CLASS = 'Tinebase_Frontend_Http';

    /**
     * the request method
     *
     * @var string
     */
    protected $_method = NULL;

    /**
     * the constructor
     */
    public function __construct()
    {
        $this->_supportsSessions = true;
        parent::__construct();
    }

    /**
     * Frontend class to be used
     * @var string
     */
    protected static $_configuredFrontendClass = self::TINEBASE_FRONTEND_CLASS;

    /**
     * Configured frontend namespace
     * @var string
     */
    protected static $_configuredFrontendNamespace = self::TINEBASE_FRONTEND_NAMESPACE;

    /**
     * Add actions to be executed for login
     */
    protected static $_plugins = array();

    /**
     * handler for HTTP api requests
     * @todo session expire handling
     * @see Tinebase_Server_Interface::handle()
     *
     * @return HTTP
     */
    public function handle(\Zend\Http\Request $request = null, $body = null)
    {
        $this->_request = $request instanceof \Zend\Http\Request ? $request : Tinebase_Core::get(Tinebase_Core::REQUEST);
        $this->_body    = $body !== null ? $body : fopen('php://input', 'r');

        $server = new Tinebase_Http_Server();
        $server->setClass(self::$_configuredFrontendClass, self::$_configuredFrontendNamespace);
        
        try {

            if (Tinebase_Session::sessionExists()) {
                try {
                    Tinebase_Core::startCoreSession();
                } catch (Zend_Session_Exception $zse) {
                    // expire session cookie for client
                    Tinebase_Session::expireSessionCookie();
                }
            }

            Tinebase_Core::initFramework();
            
            Tinebase_Core::getLogger()->INFO(__METHOD__ . '::' . __LINE__ .' Is HTTP request. method: ' . $this->getRequestMethod());
            
            // register addidional HTTP apis only available for authorised users
            if (Tinebase_Session::isStarted() && Tinebase_Auth::hasIdentity()) {

                Tinebase_Config_Manager::getInstance()->detectDomain(TRUE);

                if (empty($_REQUEST['method'])) {
                    $_REQUEST['method'] = 'Tinebase.mainScreen';
                }
                
                $applicationParts = explode('.', $this->getRequestMethod());
                $applicationName = ucfirst($applicationParts[0]);
                
                if(Tinebase_Core::getUser() && Tinebase_Core::getUser()->hasRight($applicationName, Tinebase_Acl_Rights_Abstract::RUN)) {
                    try {
                        $server->setClass($applicationName.'_Frontend_Http', $applicationName);
                    } catch (Exception $e) {
                        Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ ." Failed to add HTTP API for application '$applicationName' Exception: \n". $e);
                    }
                }
                
            } else {
                if (empty($_REQUEST['method'])) {
                    $_REQUEST['method'] = 'Tinebase.login';
                }

                foreach(self::$_plugins as $plugin) {
                    $plugin->executeAction();
                }

                // sessionId got send by client, but we don't use sessions for non authenticated users
                if (Tinebase_Session::sessionExists()) {
                    // expire session cookie on client
                    Tinebase_Session::expireSessionCookie();
                }
            }
            
            $this->_method = $this->getRequestMethod();
            
            $server->handle($_REQUEST);
        } catch (Tinebase_Exception_NotInstalled $nie) {
            $this->_dieAndDestroySession($nie, $nie->getMessage());
        } catch (Zend_Json_Server_Exception $zjse) {
            // invalid method requested or not authenticated
            Tinebase_Core::getLogger()->INFO(__METHOD__ . '::' . __LINE__ .' Attempt to request a privileged Http-API method without valid session from "' . Tinebase_AccessLog::getRemoteIpAddress());
            
            header('HTTP/1.0 403 Forbidden');

            exit;
            
        } catch (Exception $exception) {
            if (! is_object(Tinebase_Core::getLogger())) {
                // no logger -> exception happened very early
                $this->_dieAndDestroySession($e, 'Service Unavailable');
            }
            
            Tinebase_Exception::log($exception);
            
            try {
                // check if setup is required
                $setupController = Setup_Controller::getInstance();
                if ($setupController->setupRequired()) {
                    $this->_dieAndDestroySession($e, 'Application needs to be installed or updated');
                } else {
                    $this->_method = 'Tinebase.exception';
                }
                
                $server->handle(array('method' => $this->_method));
                
            } catch (Exception $e) {
                $this->_dieAndDestroySession($e, 'Service Unavailable');
            }
        }
    }

    /**
     * Adds a frontend class and defines it as configured frontend
     * @param string $classname
     * @param string $namespace
     */
    public static function setFrontendClass($classname)
    {
        if($classname === self::TINEBASE_FRONTEND_CLASS) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Classname cannot be \"Tinebase_Frontend_Http\". Renames it");
            throw new Tinebase_Exception_InvalidArgument("Namespace cannot be \"Tinebase_Frontend_Http\". Renames it");
        } else {
            if(!class_exists($classname)) {
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Classname ".$classname." not found");
                throw new Tinebase_Exception_InvalidArgument("Classname ".$classname." not found");
            }

            if(!is_subclass_of($classname, 'Tinebase_Frontend_Http_Abstract')) {
                Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Class provided: $classname is not subclass of Tinebase_Frontend_Http_Abstract");
                throw new Tinebase_Exception_InvalidArgument("Class provided: $classname is not subclass of Tinebase_Frontend_Http_Abstract");
            }
        }

        self::$_configuredFrontendClass = $classname;
    }

    /**
     * Add a server plugin action class
     * @param type $classname
     * @throws Tinebase_Exception_InvalidArgument
     * @throws Tinebase_Exceptions_InvalidArgument
     */
    public static function addPlugin($classname)
    {
        if(!class_exists($classname)) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Classname $classname not found");
            throw new Tinebase_Exception_InvalidArgument("Classname $classname not found");
        }

        if(!is_subclass_of($classname, 'Tinebase_Server_Plugin_Actions_Interface')) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Classname is not implementation of Tinebase_Server_Plugin_Actions_Interface");
            throw new Tinebase_Exceptions_InvalidArgument("Classname is not implementation of Tinebase_Server_Plugin_Actions_Interface");
        }

        if(in_array($classname, self::$_plugins)) {
            Tinebase_Core::getLogger()->err(__METHOD__ . "::" . __LINE__ . ":: Plugin $classname already added");
            throw new Tinebase_Exceptions_InvalidArgument("Plugin $classname already added");
        }

        call_user_func(array($classname, 'init'));
        self::$_plugins[] = new $classname();
    }

    
    /**
    * returns request method
    *
    * @return string|NULL
    */
    public function getRequestMethod()
    {
        if (isset($_REQUEST['method'])) {
            $this->_method = $_REQUEST['method'];
        }
        
        return $this->_method;
    }

    /**
     * @param Exception $exception
     * @param string $message
     */
    protected function _dieAndDestroySession($exception, $message)
    {
        error_log($exception);
        Tinebase_Session::destroyAndRemoveCookie();
        header('HTTP/1.0 503 Service Unavailable');
        die($message);
    }
}
