Ext.ns('Ext.ux');

Ext.ux.WindowPanel = Ext.extend(Ext.Panel, {

    closable    : true,
    maximizable : true,
    minSize: 100,
    maxSize: 300,
    ctid        : '',
    tpl         : new Ext.XTemplate(
        '<li class="{cls}" id="{id}" style="overflow:hidden">',
            '<a class="x-tab-strip-close"></a>',
            '<a class="x-tab-strip-maximize"></a>',
            '<a class="x-tab-strip-restore"></a>',
            '<a class="x-tab-right" href="#" style="padding-left:6px">',
                '<em class="x-tab-left">',
                    '<span class="x-tab-strip-inner">',
                        '<span style="margin-left:0px" class="x-tab-strip-text {iconCls}">{text} {extra}</span>',
                    '</span>',
                '</em>',
            '</a>',
        '</li>'
    ),

    afterRender: function() {

        Ext.ux.WindowPanel.superclass.afterRender.apply(this, arguments);

        var tabEl = Ext.get(this.tabEl);

        // Route *keyboard triggered* click events to the tab strip mouse handler.
        tabEl.select('a.x-tab-strip-restore').on('click', function(e){
            var bar = Ext.getCmp('minimized-windows-bar'),
                win = Ext.getCmp(this.ctid);
            if (win) {
                win.show();
                win.restore();
                bar.remove(this);
            }
        }, this, {preventDefault: true});

        // Route *keyboard triggered* click events to the tab strip mouse handler.
        tabEl.select('a.x-tab-strip-maximize').on('click', function(e){
            var bar = Ext.getCmp('minimized-windows-bar'),
                win = Ext.getCmp(this.ctid);
            if (win) {
                win.show();
                win.maximize();
                bar.remove(this);
            }
        }, this, {preventDefault: true});

    },

    initComponent: function() {

        if(!this.itemTpl){
            this.tpl.disableFormats = true;
            this.tpl.compile();
            Ext.TabPanel.prototype.itemTpl = this.tpl;
        }

        Ext.ux.WindowPanel.superclass.initComponent.apply(this, arguments);

        this.addListener({
            beforeclose: {
                fn: this.onClose,
                scope: this
            }
        });
    },

    onClose: function(p) {
        var win = Ext.getCmp(this.ctid);
        if (win) {
            win.close();
        }
        return true;
    }

});

Ext.reg('windowpanel', Ext.ux.WindowPanel);