<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Leonardo Kuamoto <leonardo.kuamoto@serpro.gov.br>
 * @copyright Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 */

/**
 * Tinebase_Model_UserFilter
 *
 * @package     Tinebase
 * @subpackage  Filter
 *
 * @todo add bday filter
 */
class Tinebase_Model_UserFilter extends Tinebase_Model_Filter_FilterGroup
{
    /**
     * @var string class name of this filter group
     *      this is needed to overcome the static late binding
     *      limitation in php < 5.3
     */
    protected $_className = 'Tinebase_Model_UserFilter';

    /**
     * @var string application of this filter group
     */
    protected $_applicationName = 'Tinebase';

    /**
     * @var string name of model this filter group is designed for
     */
    protected $_modelName = 'Tinebase_Model_User';

    /**
     * @var array filter model fieldName => definition
     */
    protected $_filterModel = array(
        'query'                => array(
            'filter' => 'Tinebase_Model_Filter_Query',
            'options' => array('fields' => array('first_name', 'last_name', 'full_name', 'display_name',
                                                 'login_name',)),
        ),
        'email'  => array('filter' => 'Tinebase_Model_Filter_Text'),
        'login_name'  => array('filter' => 'Tinebase_Model_Filter_Text')
    );
}
