<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Ldap
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @copyright   Copyright (c) 2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */

/**
 * LDAP base class for tine 2.0
 * @package     Tinebase
 * @subpackage  Ldap
 */
class Tinebase_Ldap extends Zend_Ldap
{
    /**
     * default security limit for response size
     */
    const SIZE_LIMIT = 350;

    /**
     * default security limit for search time (seconds)
     */
    const TIME_LIMIT = 5;

    /**
     * size limit for searches
     * @var integer
     */
    private $_sizeLimit = null;

    /**
     * time limit for searches
     * @var integer
     */
    private $_timeLimit = null;

    /**
     * Extend constructor
     *
     * @param array $_options
     * @return @see Zend_Ldap
     */
    public function __construct(array $_options)
    {
        // strip non Zend_Ldap options
        $options = array_intersect_key($_options, array(
            'host'                      => null,
            'port'                      => null,
            'useSsl'                    => null,
            'username'                  => null,
            'password'                  => null,
            'bindRequiresDn'            => null,
            'baseDn'                    => null,
            'accountCanonicalForm'      => null,
            'accountDomainName'         => null,
            'accountDomainNameShort'    => null,
            'accountFilterFormat'       => null,
            'allowEmptyPassword'        => null,
            'useStartTls'               => null,
            'optReferrals'              => null,
            'tryUsernameSplit'          => null
        ));
        
        $returnValue = parent::__construct($options);
        
        return $returnValue;
    }
    
    /**
     * Delete an LDAP entry
     *
     * @param  string|Zend_Ldap_Dn $dn
     * @param  array $data
     * @return Zend_Ldap *Provides a fluid interface*
     * @throws Zend_Ldap_Exception
     */
    public function deleteProperty($dn, array $data)
    {
        if ($dn instanceof Zend_Ldap_Dn) {
            $dn = $dn->toString();
        }

        $isDeleted = @ldap_mod_del($this->getResource(), $dn, $data);
        if($isDeleted === false) {
            /**
             * @see Zend_Ldap_Exception
             */
            require_once 'Zend/Ldap/Exception.php';
            throw new Zend_Ldap_Exception($this, 'deleting: ' . $dn);
        }
        return $this;
    }
    
    /**
     * read binary attribute from one entry from the ldap directory
     *
     * @todo still needed???
     * 
     * @param string $_dn the dn to read
     * @param string $_filter search filter
     * @param array $_attribute which field to return
     * @return blob binary data of given field
     * @throws  Exception with ldap error
     */
    public function fetchBinaryAttribute($_dn, $_filter, $_attribute)
    {
        $searchResult = @ldap_search($this->getResource(), $_dn, $_filter, $_attribute, $this->_attrsOnly, $this->_sizeLimit, $this->_timeLimit);
        
        if($searchResult === FALSE) {
            throw new Exception(ldap_error($this->getResource()));
        }
        
        $searchCount = ldap_count_entries($this->getResource(), $searchResult);
        if($searchCount === 0) {
            throw new Exception('Nothing found for filter: ' . $_filter);
        } elseif ($searchCount > 1) {
            throw new Exception('More than one entry found for filter: ' . $_filter);
        }
        
        $entry = ldap_first_entry($this->getResource(), $searchResult);
        
        return ldap_get_values_len($this->getResource(), $entry, $_attribute);
    }
    
    /**
     * Add new information to the LDAP repository
     *
     * @param string|Zend_Ldap_Dn $dn
     * @param array $entry
     * @return Zend_Ldap *Provides a fluid interface*
     * @throws Zend_Ldap_Exception
     */
    public function addProperty($dn, array $entry)
    {
        if (!($dn instanceof Zend_Ldap_Dn)) {
            $dn = Zend_Ldap_Dn::factory($dn, null);
        }
        self::prepareLdapEntryArray($entry);
        foreach ($entry as $key => $value) {
            if (is_array($value) && count($value) === 0) {
                unset($entry[$key]);
            }
        }

        $rdnParts = $dn->getRdn(Zend_Ldap_Dn::ATTR_CASEFOLD_LOWER);
        $adAttributes = array('distinguishedname', 'instancetype', 'name', 'objectcategory',
            'objectguid', 'usnchanged', 'usncreated', 'whenchanged', 'whencreated');
        $stripAttributes = array_merge(array_keys($rdnParts), $adAttributes);
        foreach ($stripAttributes as $attr) {
            if (array_key_exists($attr, $entry)) {
                unset($entry[$attr]);
            }
        }
                
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . '  $dn: ' . $dn->toString());
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . '  $data: ' . print_r($entry, true));
        
        $isAdded = @ldap_mod_add($this->getResource(), $dn->toString(), $entry);
        if($isAdded === false) {
            /**
             * @see Zend_Ldap_Exception
             */
            require_once 'Zend/Ldap/Exception.php';
            throw new Zend_Ldap_Exception($this, 'adding: ' . $dn->toString());
        }
        return $this;
    }
    
    /**
     * Update LDAP registry
     *
     * @param string|Zend_Ldap_Dn $dn
     * @param array $entry
     * @return Zend_Ldap *Provides a fluid interface*
     * @throws Zend_Ldap_Exception
     */
    public function updateProperty($dn, array $entry)
    {
        if (!($dn instanceof Zend_Ldap_Dn)) {
            $dn = Zend_Ldap_Dn::factory($dn, null);
        }
        self::prepareLdapEntryArray($entry);

        $rdnParts = $dn->getRdn(Zend_Ldap_Dn::ATTR_CASEFOLD_LOWER);
        $adAttributes = array('distinguishedname', 'instancetype', 'name', 'objectcategory',
            'objectguid', 'usnchanged', 'usncreated', 'whenchanged', 'whencreated');
        $stripAttributes = array_merge(array_keys($rdnParts), $adAttributes);
        foreach ($stripAttributes as $attr) {
            if (array_key_exists($attr, $entry)) {
                unset($entry[$attr]);
            }
        }

        if (count($entry) > 0) {
            $isModified = @ldap_mod_replace($this->getResource(), $dn->toString(), $entry);
            if($isModified === false) {
                /**
                 * @see Zend_Ldap_Exception
                 */
                require_once 'Zend/Ldap/Exception.php';
                throw new Zend_Ldap_Exception($this, 'updating: ' . $dn->toString());
            }
        }
        return $this;
    }

    /**
     * sets the size limit for searches
     * @param integer $_value
     */
    public function setSizeLimit($_value) {
        if(is_integer($_value) && $_value > 0) {
            $this->_sizeLimit = $_value;
        }
    }

    /**
     * sets the time limit for searches
     * @param integer $_value
     */
    public function setTimeLimit($_value) {
        if(is_integer($_value) && $_value > 0) {
            $this->_timeLimit = $_value;
        }
    }

    /**
     * Returns a boolean indicator if user password is expired
     *
     * @param  string $_acctname
     * @return boolean
     * @throws Zend_Ldap_Exception
     */
    public function passwordExpired($_acctName)
    {
        ////Bind as account user to fetch user data
        $this->bind($this->_options['username'], $this->_options['password']);
        $baseDn = $this->getBaseDn();
        if (!$baseDn) {
            /**
             * @see Zend_Ldap_Exception
             */
            require_once 'Zend/Ldap/Exception.php';
            throw new Zend_Ldap_Exception(null, 'Base DN not set');
        }
        $accountFilter = $this->_getAccountFilter($_acctName);
        if (!$accountFilter) {
            /**
             * @see Zend_Ldap_Exception
             */
            require_once 'Zend/Ldap/Exception.php';
            throw new Zend_Ldap_Exception(null, 'Invalid account filter');
        }
        $pwExpiredAttribute = strtolower(Tinebase_User::getBackendConfiguration(Tinebase_Config::PASSWORD_EXPIRATION_ATTRIBUTE));
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)){
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ .
                    ' LDAP will search for password expiration attribute "' . $pwExpiredAttribute . '"'
                    . ' on user "' . $_acctName . '"');
        }
        $fetchData = $this->search($accountFilter, $baseDn, self::SEARCH_SCOPE_SUB, array($pwExpiredAttribute));
        $countFetchData = $fetchData->count();
        if ($countFetchData === 1){
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)){
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                        . ' LDAP found user entry for password expiration');
            }
            $userData = $fetchData->getFirst();
            if(!(isset($userData[$pwExpiredAttribute][0]))){
                $fetchData->close();
                Tinebase_Core::getLogger()->crit(__METHOD__ . '::' . __LINE__
                        . ' LDAP backend is configured to check password expiration, but it was not possible to find'
                        . ' out the desired attribute "' . $pwExpiredAttribute . '" on user account '
                        . '"' . $_acctName . '"' . print_r($userData, true));

                throw new Zend_Ldap_Exception(null, 'LDAP user account "' . $_acctName . '" does not have a password '
                        . 'expiration attribute called "' . $pwExpiredAttribute . '". Check setup entry for authentication '
                        . 'section and LDAP data from fetched user', Zend_Ldap_Exception::LDAP_NO_SUCH_ATTRIBUTE);
            }
            $userAttTmz = $userData[$pwExpiredAttribute][0];
            $return = $this->testPasswordExpirationDate($userAttTmz);
            if ($return){
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)){
                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                            . ' LDAP user ' . $_acctName . ' password is expired');
                }
            }else{
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)){
                    Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                            . ' LDAP user ' . $_acctName . ' has not expired password');
                }
            }
        } else{
            Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__
                    . ' LDAP fails trying to check password expiration attribute for user authentication with '
                    . 'credencials "' . $_acctName . '"');
        }
        $fetchData->close();
        return $return;
    }

    /**
    * Returns a boolean indicator if user password is expired
    *
    * @param  string $_date
    * @return boolean
    */
    public function testPasswordExpirationDate($_date)
    {
        /**
        *@TODO put the regular expression in the config so the format of the date expected can change
        */
        $expirationInterval = Tinebase_User::getBackendConfiguration(Tinebase_Config::PASSWORD_EXPIRATION_INTERVAL);
        $return = true;
        $userAttTmz = $_date;
        if (preg_match('/\d{14,14}Z$/', $userAttTmz) === 1){
            $dateFormat = substr($userAttTmz, 0, 4)    . "-" . substr($userAttTmz, 4, 2) . "-" .
                           substr($userAttTmz, 6, 2)    . " " . substr($userAttTmz, 8, 2) . ":" .
                           substr($userAttTmz, 10, 2)   . ":" . substr($userAttTmz, 12, 2);
            $dateUserPasswordExpires = new DateTime($dateFormat);
            $dateToday = new DateTime(date('Y-m-d H:i:s'));
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)){
                Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                        . ' Checking dates $dateUserPasswordExpires >= $dateToday '
                        . $dateUserPasswordExpires->getTimestamp() . ' >= ' . $dateToday->getTimestamp());
            }
            if($dateUserPasswordExpires >= $dateToday || $expirationInterval === '0'){
                $return = false;
            }
        }
        return $return;
    }

    /**
     * A global LDAP search routine for finding information.
     *
     * @param string|Zend_Ldap_Filter_Abstract $filter
     * @param string|Zend_Ldap_Dn $basedn
     * @param integer $scope
     * @param array $attributes
     * @param string $sort
     * @param string $collectionClass
     * @return Zend_Ldap_Collection
     */
    public function search($filter, $basedn = null, $scope = self::SEARCH_SCOPE_SUB,
        array $attributes = array(), $sort = null, $collectionClass = null, $sizelimit = 0, $timelimit = 0)
    {

        if ($basedn === null) {
            $basedn = $this->getBaseDn();
        }

        else if ($basedn instanceof Zend_Ldap_Dn) {
            $basedn = $basedn->toString();
        }

        if ($filter instanceof Zend_Ldap_Filter_Abstract) {
            $filter = $filter->toString();
        }

        if(!empty($this->_sizeLimit) && $this->_sizeLimit > 0) {
            $sizelimit = $this->_sizeLimit;
        } else {
            $sizelimit = self::SIZE_LIMIT; // default security value
        }

        if(!empty($this->_timeLimit) && $this->_timeLimit > 0) {
            $timelimit = $this->_timeLimit;
        } else {
            $timelimit = self::TIME_LIMIT; // default security value
        }

        switch ($scope) {
            case self::SEARCH_SCOPE_ONE:
                $search = @ldap_list($this->getResource(), $basedn, $filter, $attributes, false, $sizelimit, $timelimit);
                break;
            case self::SEARCH_SCOPE_BASE:
                $search = @ldap_read($this->getResource(), $basedn, $filter, $attributes, false, $sizelimit, $timelimit);
                break;
            case self::SEARCH_SCOPE_SUB:
            default:
                $search = @ldap_search($this->getResource(), $basedn, $filter, $attributes, false, $sizelimit, $timelimit);
                break;
        }

        $errorCode = $this->getLastErrorCode();
        if($errorCode == Zend_Ldap_Exception::LDAP_PARTIAL_RESULTS ||
                $errorCode == Zend_Ldap_Exception::LDAP_SIZELIMIT_EXCEEDED) {
            $message = "Ldap partial results returned";
        }

        if (!is_null($sort) && is_string($sort)) {
            $isSorted = ldap_sort($this->getResource(), $search, $sort);
            if($isSorted === false) {
                $errorCode = $this->getLastErrorCode();
                $message   = "Sorting error: $errorCode";
                return new Tinebase_Record_LdapCollection(null, $errorCode, $message);
            }
        }

        /**
         * Zend_Ldap_Collection_Iterator_Default
         */
        require_once 'Zend/Ldap/Collection/Iterator/Default.php';
        $iterator = new Zend_Ldap_Collection_Iterator_Default($this, $search);
        require_once 'Zend/Ldap/Collection.php';
        return new Tinebase_Record_LdapCollection($iterator);
    }
}
