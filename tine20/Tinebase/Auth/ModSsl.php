<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  Auth
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009-2013 Serpro (http://www.serpro.gov.br)
 * @copyright   Copyright (c) 2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Mário César Kolling <mario.koling@serpro.gov.br>
 */

/**
 * DigitalCertificate authentication backend
 * 
 * @package     Tinebase
 * @subpackage  Auth
 */
class Tinebase_Auth_ModSsl extends Zend_Auth_Adapter_ModSsl implements Tinebase_Auth_Interface
{
    /**
     * type of backend;
     */
    const TYPE = 'ModSsl';
    
    protected $_identity;
    protected $_credential;
    
    public function __construct() {
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) {
            Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . ' Loading Module Tinebase_Auth_ModSsl');
        }
    }
    
    public function setCredential($credential)
    {
        $this->_credential = $credential;
    }
    
    public function setIdentity($value)
    {
        $this->_identity = $value;
    }

    /**
     * Returns the type of backend
     * @return string
     */
    public static function getType()
    {
        return self::TYPE;
    }
        
    /**
     * Returns default configurations of the backend
     * @return array
     */
    public static function getBackendConfigurationDefaults()
    {
        return array();
    }

    /**
     * Returns a connection to user backend
     * @param array $_options
     * @return Tinebase_Ldap
     * @throws Tinebase_Exception_Backend
     */
    public static function getBackendConnection(array $_options = array())
    {
        return null;
    }

    /**
     * Checks if user backend is valid
     * @param mixed $_authBackend
     * @return boolean
     */
    public static function isValid($_authBackend)
    {
        return true;
    }

    /**
     * Force close connection to backend
     */
    public function closeConnection()
    {
    }

    /*
     * (non-PHPdoc)
     * @see Zend_Auth_Adapter_ModSsl::authenticate()
     */
    public function authenticate()
    {
        if (self::hasModSsl()) {
            // Fix to support reverseProxy without SSLProxyEngine
            $clientCert = !empty($_SERVER['SSL_CLIENT_CERT']) ? $_SERVER['SSL_CLIENT_CERT'] : $_SERVER['HTTP_SSL_CLIENT_CERT'];

            // get Identity
            $certificate = Custom_Auth_ModSsl_Certificate_Factory::buildCertificate($clientCert);
            $config = Tinebase_Config::getInstance()->get('modssl');

            if (class_exists($config->username_callback)) {
                $callback = new $config->username_callback($certificate);
            } else { // fallback to default
                $callback = new Custom_Auth_ModSsl_UsernameCallback_Standard($certificate);
            }

            $username = call_user_func(array($callback, 'getUsername'));

            $this->setIdentity($username);
            $this->setCredential(null);

            if(($checkResult = Tinebase_Config_Manager::getInstance()->isEnvironmentSet($username)) === TRUE) {

                if ($certificate instanceof Custom_Auth_ModSsl_Certificate_X509) {
                    if(!$certificate->isValid()) {
                        $lines = '';
                        foreach($certificate->getStatusErrors() as $line) {
                            $lines .= $line . '#';
                        }

                        Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' ModSsl authentication for '. $this->_identity . ' failed: ' . $lines);

                        return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID, $this->_identity, $certificate->getStatusErrors());
                    }
                    $messages = array();

                    // If certificate is valid store it in database
                    $controller = Addressbook_Controller_Certificate::getInstance();
                    try {
                        $controller->create(new Addressbook_Model_Certificate($certificate));
                    } catch (Tinebase_Exception_Duplicate $ted) {
                        // Fail silently if certificate already exists
                        Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' ' . $ted->getMessage());
                    }
                    return new Zend_Auth_Result(Zend_Auth_Result::SUCCESS, $this->_identity, $messages);
                }
            } else {
                return new Zend_Auth_Result(Zend_Auth_Result::FAILURE_UNCATEGORIZED, $this->_identity, array($checkResult['errorMessage']));
            }
        }

        return new Zend_Auth_Result(Zend_Auth_Result::FAILURE, 'Unknown User', array('Unknown Authentication Error'));
    }

    /**
     * Checks if backend must be forced over the configuration
     * [EXPRESSO EXCLUSIVE FEATURE]
     * @return boolean
     */
    public static function isForcedBackend()
    {
        return Zend_Auth_Adapter_ModSsl::hasModSsl();
    }
}