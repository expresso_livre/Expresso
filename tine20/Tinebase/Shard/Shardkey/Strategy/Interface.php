<?php
/**
 * Tine 2.0
 *
 * @package     Shard
 * @subpackage  Strategy
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Interface for Tinebase_Shard_Shardkey_Strategy
 *
 * @package     Shard
 */

Interface Tinebase_Shard_Shardkey_Strategy_Interface
{
    /**
     * Get ShardKey from selected strategy
     *
     * @param  string $_database
     * @return string || NULL
     */
    public function getShardKey($_database);

    /**
     * Get All shardKeys associated with a virtualshard from selected strategy
     *
     * @param  string $_database
     * @param  string $_virtualShard
     * @return array || NULL || FALSE
     */
    public function getShardKeysByVirtualShard($_database, $_virtualShard);

    /**
     * Get All shardKeys from selected strategy
     *
     * @param  string $_database
     * @return array || NULL
     */
    public function getAllShardKeys($_database);

    /**
     * Get All Filters for Shard Key
     *
     * @param  string $_shardKey
     * @return array
     */
    public function getFilters($_shardKey);
}
