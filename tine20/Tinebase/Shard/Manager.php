<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * Shard Controller
 *
 * @package     Tinebase
 * @subpackage  Shard
 */
class Tinebase_Shard_Manager
{
    /**
     * constant for shard config registry index
     */
    const SHARD_CONFIG_TO_SETUP = 'shardconfigtosetup';

    /**
     * constant for Logged User Backend Connection Config registry index
     */
    const LOGIN_USER_BACKEND_CONNECTION_CONFIG = 'loginuserbackendconnectionconfig';

    /**
     * constant for Shard Logger
     */
    const SHARD_LOGGER = 'shardlogger';

    /**
     * Array of configs
     *
     * @var array
     */
    private static $shardConfig = array();

    /**
     * Logger config
     *
     * @var array
     */
    private static $loggerConfig = NULL;

    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_Shard_Manager
     */
    private static $instance = NULL;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __construct() {}

    /**
     * don't clone. Use the singleton.
     */
    private function __clone() {}

    /**
     * the singleton pattern
     *
     * @return Tinebase_Shard
     */
    public static function getInstance()
    {
        if (self::$instance === NULL) {
            self::$instance = new Tinebase_Shard_Manager();
        }
        return self::$instance;
    }

    /**
     * Returns shard config filename
     *
     * @param string $_database | NULL
     * @return mixed
     */
    public static function getShardConfigId($_database = NULL)
    {
        $_database = ($_database === NULL) ? 'database' : $_database;
        $backendShardConfig = Tinebase_Core::getConfig()->shard;
        if (isset($backendShardConfig[$_database]) || array_key_exists($_database, $backendShardConfig)) {
             return $backendShardConfig[$_database];
        }

        return FALSE;
    }

    /**
     * Returns shard config
     *
     * @param string $_database | NULL
     * @return mixed
     */
    public static function getShardConfig($_database = NULL)
    {
        $_database = ($_database === NULL) ? 'database' : $_database;
        if (! (isset(self::$shardConfig[$_database]) || array_key_exists($_database, self::$shardConfig))) {
             $shardConfigFileName = self::getShardConfigId($_database) . '.inc.php';

             if (isset($shardConfigFileName)) {
                 Tinebase_Shard_Config::setConfigFileName($shardConfigFileName);
             }

             $shardConfig = Tinebase_Shard_Config::getInstance();

             self::$shardConfig[$_database] = $shardConfig;
             return $shardConfig;
        }
        return self::$shardConfig[$_database];
    }

    /**
     * Returns connection configuration that points to shardKey associated shard
     *
     * @param  string $_database
     * @param  NULL || string $_shardKey
     * @return array || NULL
     */
    public function getConnectionConfig($_database, $_shardKey = NULL)
    {
        try {
            $shardConfig = self::getShardConfig($_database);

            if (empty($_shardKey)) {
                if (! isset($shardConfig->shardKeyStrategy)) {
                    throw new Tinebase_Exception_NotFound ('shardKeyStrategy not found in Shard Config File.');
                }

                $shardkeyStrategy = new Tinebase_Shard_Shardkey_Strategy_Context();

                $resultShardKeyStrategy = $shardkeyStrategy->setStrategy($shardConfig->shardKeyStrategy);

                if (! isset($resultShardKeyStrategy)) {
                    throw new Tinebase_Exception_NotFound ('Not implemented Shardkey Strategy ' . $shardConfig->shardKeyStrategy);
                }

                $_shardKey = $shardkeyStrategy->getShardKey($_database);

                if (! isset($_shardKey)) {
                    throw new Tinebase_Exception_NotFound ('Unable to get shardKey.');
                }
            }

            $connectionConfigClass = 'Tinebase_Shard_Connectionconfig_' . $shardConfig->connectionConfigClass['class'];
            $connectionConfigObj = $connectionConfigClass::getInstance($shardConfig->connectionConfigClass['options'], $_database);

            $resharding = $this->_resharding($_database);
            if ($resharding !== 'disabled') {
                if ($resharding == $_shardKey) {
                    Tinebase_Core::getLogger()->notice(__METHOD__ . '::' . __LINE__ . ' Shard Key '
                            . $resharding . ' was blocked by the Resharding process. Wait the process finish and try again.');
                    return NULL;
                } else {
                    $connectionConfigKey = $this->_getReshardingAssociation($_database, $_shardKey);
                    if ($connectionConfigKey !== FALSE) {
                        $connectionConfig = new Tinebase_Config_Struct($connectionConfigObj->getConnectionConfig($connectionConfigKey));

                        if (! isset($connectionConfig)) {
                            throw new Tinebase_Exception_NotFound ('Unable to associate Backend Connection Config Key '
                                    .  $connectionConfigKey . ' of shard key ' . $_shardKey . ' with Backend Connection Config.');
                        }

                        return $connectionConfig;
                    }
                }
            }

            if (! isset($_shardKey) && Tinebase_Core::get(self::LOGIN_USER_BACKEND_CONNECTION_CONFIG)) {
                return Tinebase_Core::get(self::LOGIN_USER_BACKEND_CONNECTION_CONFIG);
            }

            if (! isset($shardConfig->associationVirtualshardShardkeyStrategy)) {
                throw new Tinebase_Exception_NotFound ('associationVirtualshardShardkeyStrategy not found in Shard Config File. ');
            }

            $associationVirtualshardConnectionconfigkeyClass =
                    'Tinebase_Shard_Association_Virtualshard_Connectionconfigkey_'
                    . $shardConfig->associationVirtualshardConnectionconfigkeyClass['class'];
            $associationVirtualshardConnectionconfigkeyObj =
                    $associationVirtualshardConnectionconfigkeyClass::getInstance($shardConfig->associationVirtualshardConnectionconfigkeyClass['options'], $_database);

            $associationVirtualshardShardkey = new Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Context();

            $result = $associationVirtualshardShardkey->setStrategy($shardConfig->associationVirtualshardShardkeyStrategy);

            if (! isset($result)) {
                 throw new Tinebase_Exception_NotFound ('Not implemented association between Virtualshard and Shardkey Strategy '
                         . $shardConfig->associationVirtualshardShardkeyStrategy);
            }

            $virtualShard = $associationVirtualshardShardkey->getVirtualShard($_database, $_shardKey);

            if (! isset($virtualShard)) {
                 throw new Tinebase_Exception_NotFound ('Unable to get Virtual Shard of shardKey' . $_shardKey . '  ');
            }

            $connectionConfigKey = $associationVirtualshardConnectionconfigkeyObj->getAssociation($virtualShard);

            if (! isset($connectionConfigKey) || ($connectionConfigKey === FALSE)) {
                 throw new Tinebase_Exception_NotFound ('Unable to associate Virtual Shard ' .  $virtualShard . ' of Shardkey '
                         . $_shardKey . ' with Backend Connection Config.');
            }

            $connectionConfig = new Tinebase_Config_Struct($connectionConfigObj->getConnectionConfig($connectionConfigKey));

            if (! isset($connectionConfig)) {
                 throw new Tinebase_Exception_NotFound ('Unable to associate Backend Connection Config Key '
                         .  $connectionConfigKey . ' of shardkey ' . $_shardKey . ' with Backend Connection Config.');
            }

            if (! isset($_shardKey)) {
                Tinebase_Core::set(self::LOGIN_USER_BACKEND_CONNECTION_CONFIG, $connectionConfig);
            }

            if (Tinebase_Core::isLogLevel ( Zend_Log::INFO ))
                Tinebase_Core::getLogger()->INFO(__METHOD__ . '::' . __LINE__ . ' Shardkey: ' . $_shardKey . ', Shard: '
                        . $virtualShard . ', Backend Connection Config Key: ' . $connectionConfigKey);


            return $connectionConfig;
        } catch (Tinebase_Exception_NotFound $e) {
            Tinebase_Core::getLogger()->DEBUG(__METHOD__ . '::' . __LINE__ . ' ' . $e->getMessage() . '   '
                    . $e->getTraceAsString());
            return NULL;
        } catch (Tinebase_Exception $e) {
            if (Tinebase_Core::isLogLevel ( Zend_Log::DEBUG ))
                Tinebase_Core::getLogger()->DEBUG(__METHOD__ . '::' . __LINE__ . ' ' . $e->getMessage());
            return NULL;

        }
    }

    /**
     * Add a connection configuration
     *
     * @param  string $_database
     * @param  array  $_connectionConfig
     * @param  string $_outputLogFile
     * @return boolean success
     */
    public function setConnectionConfig($_database, $_connectionConfig, $_outputLogFile)
    {
        self::$loggerConfig =  array (
            'active' => TRUE,
            'priority' => 8,
            'filename' => $_outputLogFile,
            'traceQueryOrigins' => FALSE,
        );

        $shardConfig = self::getShardConfig($_database);
        $connectionConfigClass = 'Tinebase_Shard_Connectionconfig_' . $shardConfig->connectionConfigClass['class'];
        $connectionConfigObj = $connectionConfigClass::getInstance($shardConfig->connectionConfigClass['options'], $_database);

        $key = key($_connectionConfig);
        $result = $connectionConfigObj->setConnectionConfig($key, $_connectionConfig[$key]);
        return $result;
    }

    /**
     * Delete a connection configuration
     *
     * @param  string $_database
     * @param  array  $_connectionConfig
     * @param  string $_outputLogFile
     * @return boolean success
     */
    public function removeConnectionConfig($_database, $_connectionConfig, $_outputLogFile)
    {
        self::$loggerConfig =  array (
            'active' => TRUE,
            'priority' => 8,
            'filename' => $_outputLogFile,
            'traceQueryOrigins' => FALSE,
        );

        $shardConfig = self::getShardConfig($_database);
        $connectionConfigClass = 'Tinebase_Shard_Connectionconfig_' . $shardConfig->connectionConfigClass['class'];
        $connectionConfigObj = $connectionConfigClass::getInstance($shardConfig->connectionConfigClass['options'], $_database);

        $result = $connectionConfigObj->removeConnectionConfig($_connectionConfig);
        return result;
    }

    /**
     * Get association of one or all Shard Keys
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @param  string $_outputLogFile
     * @return boolean success
     */
    public function getAssociationOfShardKey($_database, $_shardKey = NULL, $_outputLogFile)
    {
        self::$loggerConfig =  array (
            'active' => TRUE,
            'priority' => 8,
            'filename' => $_outputLogFile,
            'traceQueryOrigins' => FALSE,
        );

        $shardConfig = self::getShardConfig($_database);
        $associationVirtualshardShardkey = new Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Context();
        $result = $associationVirtualshardShardkey->setStrategy($shardConfig->associationVirtualshardShardkeyStrategy);
        $associationVirtualshardConnectionconfigkeyClass =
                'Tinebase_Shard_Association_Virtualshard_Connectionconfigkey_'
                . $shardConfig->associationVirtualshardConnectionconfigkeyClass['class'];
        $associationVirtualshardConnectionconfigkeyObj =
                $associationVirtualshardConnectionconfigkeyClass::getInstance($shardConfig->associationVirtualshardConnectionconfigkeyClass['options'], $_database);

        if (! isset($result)) {
            echo 'ERROR: Not implemented association between Virtualshard and Shardkey Strategy '
                    . $shardConfig->associationVirtualshardShardkeyStrategy . '\n';
            return FALSE;
        }

        if (isset($_shardKey)) {
            $virtualShard = $associationVirtualshardShardkey->getVirtualShard($_database, $_shardKey);

            if (! isset($virtualShard)) {
                echo 'ERROR: Unable to get Virtual Shard of shardKey ' . $_shardKey . '\n';
                return FALSE;
            }

            $connectionConfigKey = $associationVirtualshardConnectionconfigkeyObj->getAssociation($virtualShard);

            if (! isset($connectionConfigKey)) {
                echo 'ERROR: Unable to associate Virtual Shard ' .  $virtualShard . ' of Shardkey ' . $_shardKey
                        . ' with connection Config.\n';
            }
            self::getLogger()->info("ShardKey | VirtualShard | ConnectionConfigKey");
            self::getLogger()->info($_shardKey . "; " .  $virtualShard . "; " . $connectionConfigKey);
        } else {
            $orderedByVirtualShard = array();

            if (! isset($shardConfig->shardKeyStrategy)) {
                throw new Tinebase_Exception_NotFound ('shardKeyStrategy not found in Shard Config File.');
            }

            $shardkeyStrategy = new Tinebase_Shard_Shardkey_Strategy_Context();

            $resultShardKeyStrategy = $shardkeyStrategy->setStrategy($shardConfig->shardKeyStrategy);

            if (! isset($resultShardKeyStrategy)) {
                throw new Tinebase_Exception_NotFound ('Not implemented Shardkey Strategy ' . $shardConfig->shardKeyStrategy);
            }

            $shardKeys = $shardkeyStrategy->getAllShardKeys($_database);

            if (! isset($shardKeys)) {
                throw new Tinebase_Exception_NotFound ('Unable to get shardKeys.');
            }

            self::getLogger()->info("VirtualShard | ShardKey | ConnectionConfigKey");

            foreach ($shardKeys as $shardKey) {
                $virtualShard = $associationVirtualshardShardkey->getVirtualShard($_database, $shardKey);

                if (! isset($virtualShard)) {
                     throw new Tinebase_Exception_NotFound ('Unable to get Virtual Shard of shardKey' . $shardKey . '  ');
                }

                $connectionConfigKey = $associationVirtualshardConnectionconfigkeyObj->getAssociation($virtualShard);

                if (! isset($connectionConfigKey)) {
                    echo 'ERROR: Unable to associate Virtual Shard ' .  $virtualShard . ' of Shardkey ' . $shardKey
                            . ' with connection Config.\n';
                }

                $orderedByVirtualShard[$virtualShard][] = array('shardkey' => $shardKey, 'connectionConfigKey'
                                                                => $connectionConfigKey);
            }

            ksort($orderedByVirtualShard);

            foreach ($orderedByVirtualShard as $vshard => $values) {
                foreach ($values as $value) {
                    self::getLogger()->info($vshard . "; " . $value['shardkey'] . "; " . $value['connectionConfigKey']);;
                }
            }
        }
        return TRUE;
    }

    /**
     * Set association between Virtual Shard and connection config
     *
     * @param  string $_database
     * @param  string $_virtualShard
     * @param  string $_connectionConfigKey
     * @return boolean success
     */
    private function _setVirtualShardAssociation($_database, $_virtualShard, $_connectionConfigKey)
    {
        $shardConfig = self::getShardConfig($_database);
        $associationClassName = 'Tinebase_Shard_Association_Virtualshard_Connectionconfigkey_'
                              . $shardConfig->associationVirtualshardConnectionconfigkeyClass['class'];
        $association = $associationClassName::getInstance($shardConfig->associationVirtualshardConnectionconfigkeyClass['options'], $_database);
        return $association->setAssociation($_virtualShard, $_connectionConfigKey);
    }

    /**
     * get resharding flag
     *
     * @param  string $_database
     * @return string shardKey|disabled
     */
    private function _resharding($_database)
    {
        $shardConfig = self::getShardConfig($_database);
        $flagClassName = 'Tinebase_Shard_Resharding_Flag_' . $shardConfig->reshardingFlagClass['class'];
        $flag = $flagClassName::getInstance($shardConfig->reshardingFlagClass['options'], $_database);
        return $flag->getResharding();
    }

    /**
     * set resharding flag
     *
     * @param  string $_database
     * @return string shardKey|NULL
     */
    private function _setResharding($_database, $_shardKey = NULL)
    {
        $shardConfig = self::getShardConfig($_database);
        $flagClassName = 'Tinebase_Shard_Resharding_Flag_' . $shardConfig->reshardingFlagClass['class'];
        $flag = $flagClassName::getInstance($shardConfig->reshardingFlagClass['options'], $_database);
        return $flag->setResharding($_shardKey);
    }

    /**
     * Get resharding association
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @return string connectionConfigKey
     */
    private function _getReshardingAssociation($_database, $_shardKey)
    {
        $shardConfig = self::getShardConfig($_database);
        $associationsClassName = 'Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_'
                               . $shardConfig->reshardingAssociationShardkeyConnectionconfigkeyClass['class'];
        $associations = $associationsClassName::getInstance($shardConfig->reshardingAssociationShardkeyConnectionconfigkeyClass['options'], $_database);
        return $associations->getAssociation($_shardKey);
    }

    /**
     * Add resharding association
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @param  string connectionConfigKey
     */
    private function _addReshardingAssociation($_database, $_shardKey, $_connectionConfigKey)
    {
        $shardConfig = self::getShardConfig($_database);
        $associationsClassName = 'Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_'
                               . $shardConfig->reshardingAssociationShardkeyConnectionconfigkeyClass['class'];
        $associations = $associationsClassName::getInstance($shardConfig->reshardingAssociationShardkeyConnectionconfigkeyClass['options'], $_database);
        $associations->addAssociation($_shardKey, $_connectionConfigKey);
    }

    /**
     * Clear all resharding associations
     *
     * @param  string $_database
     * @return string connectionConfigKey
     */
    private function _clearAllReshargingAssociations($_database)
    {
        $shardConfig = self::getShardConfig($_database);
        $associationsClassName = 'Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_'
                               . $shardConfig->reshardingAssociationShardkeyConnectionconfigkeyClass['class'];
        $associations = $associationsClassName::getInstance($shardConfig->reshardingAssociationShardkeyConnectionconfigkeyClass['options'], $_database);
        $associations->clearAllAssociations();
    }

    /**
     * Perform resharding of a shardKey, moving shardKey to connectionConfigKey
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @param  string connectionConfigKey
     * @return boolean success
     */
    public function reshardShardKey($_database, $_shardKey, $_connectionConfigKey)
    {
        $shardConfig = self::getShardConfig($_database);
        $connectionConfigClass = 'Tinebase_Shard_Connectionconfig_' . $shardConfig->connectionConfigClass['class'];
        $connectionConfigObj = $connectionConfigClass::getInstance($shardConfig->connectionConfigClass['options'], $_database);
        $connectionConfigDestination = new Tinebase_Config_Struct($connectionConfigObj->getConnectionConfig($_connectionConfigKey));
        $connectionConfig = $this->getConnectionConfig($_database, $_shardKey);

        $resharding = $this->_setResharding($_database, $_shardKey);

        $redistributer = Tinebase_Shard_Redistributer_Factory::factory($_database, 'pgsql');

        self::getLogger()->info("=> FETCHING DATA FROM DESTINATION " . $_connectionConfigKey);
        $rowsDestination = $redistributer->fetchAllShardKeyData($_shardKey, $connectionConfigDestination);
        self::getLogger()->info("=> DELETING DATA FROM DESTINATION " . $_connectionConfigKey);
        $deleteStatus = $redistributer->deleteAllShardKeyData($rowsDestination, $connectionConfigDestination);
        unset($rowsDestination);

        self::getLogger()->info("=> FETCHING DATA FROM ORIGIN ");
        $rowsOrigin = $redistributer->fetchAllShardKeyData($_shardKey, $connectionConfig);
        self::getLogger()->info("=> INSERTING DATA IN DESTINATION " . $_connectionConfigKey);
        $insertStatus = $redistributer->insertAllShardKeyData($rowsOrigin, $connectionConfigDestination);
        self::getLogger()->info("=> COMPARING COPIED DATA ");
        $statusCompare = $redistributer->compareAllShardKeyData($rowsOrigin, $_shardKey, $connectionConfigDestination);

        if ($statusCompare) {
            $this->_addReshardingAssociation($_database, $_shardKey, $_connectionConfigKey);

            self::getLogger()->info("=> DELETING DATA FROM ORIGIN ");
            unset($rowsOrigin);
            $rowsOrigin = $redistributer->fetchAllShardKeyData($_shardKey, $connectionConfig);
            $deleteStatus = $redistributer->deleteAllShardKeyData($rowsOrigin, $connectionConfig);
        }

        unset($rowsOrigin);
        unset($rowsDestination);

        return $statusCompare;
    }

    /**
     * Perform resharding of all shardKeys associated with a Virtual Shard
     *
     * @param  string $_database
     * @param  string $_virtualShard
     * @param  string $_connectionConfigKey
     * @param  string $_outputLogFile
     * @return boolean success
     */
    public function reshardVirtualShard($_database, $_virtualShard, $_connectionConfigKey, $_outputLogFile) {

        self::$loggerConfig =  array (
            'active' => TRUE,
            'priority' => 8,
            'filename' => $_outputLogFile,
            'traceQueryOrigins' => FALSE,
        );

        if ($this->_clearAllReshargingAssociations($_database) === FALSE) {
            echo "ERROR: It was not possible to clear Resharding temporary associations";
            return FALSE;
        }

        $shardConfig = self::getShardConfig($_database);

        $result = $this->_setResharding($_database);

        self::getLogger()->info("Obtaining the ShardKey list associated with the Virtual Shard " . $_virtualShard);

        if (! isset($shardConfig->shardKeyStrategy)) {
            throw new Tinebase_Exception_NotFound ('shardKeyStrategy not found in Shard Config File.');
        }

        $shardkeyStrategy = new Tinebase_Shard_Shardkey_Strategy_Context();

        $resultShardKeyStrategy = $shardkeyStrategy->setStrategy($shardConfig->shardKeyStrategy);

        if (! isset($resultShardKeyStrategy)) {
            throw new Tinebase_Exception_NotFound ('Not implemented Shardkey Strategy ' . $shardConfig->shardKeyStrategy);
        }

        $shardKeys = $shardkeyStrategy->getShardKeysByVirtualShard($_database, $_virtualShard);

        if (! isset($shardKeys)) {
            throw new Tinebase_Exception_NotFound ('Unable to get shardKeys.');
        }

        foreach ($shardKeys as $shardKey) {

            if ($this->_getReshardingAssociation($_database, $shardKey) !== FALSE) {
                self::getLogger()->info("Resharding  of " . $shardKey . " had been done.");
                continue;
            }

            self::getLogger()->info("Starting Resharding of " . $shardKey . "...");

            try {
                $success = $this->reshardShardKey($_database, $shardKey, $_connectionConfigKey);
            } catch (Exception $ex) {
                echo $ex->getMessage();
                echo $ex->getTraceAsString();
                echo "\n";
                return FALSE;
            }

            if ($success) {
                self::getLogger()->info($shardKey . " Successfully completed.");
            } else {
                self::getLogger()->info("Resharding of " . $shardKey . " FAILED!!!");
                return FALSE;
            }
        }

        if ($this->_setVirtualShardAssociation($_database, $_virtualShard, $_connectionConfigKey) === FALSE) {
            self::getLogger()->info("ERROR: It was not possible to associate the virtual shard with destination");
            return FALSE;
        }

        if ($this->_setResharding($_database) === FALSE) {
            self::getLogger()->info("ERROR: It was not possible to update resharding flag.");
            return FALSE;
        }

        if ($this->_clearAllReshargingAssociations($_database) === FALSE) {
            self::getLogger()->info("ERROR: It was not possible to clear resharding associations.");
            return FALSE;
        }

        self::getLogger()->info("Resharging of Virtual Shard " . $_virtualShard . " successfully finished!!!");

        return TRUE;
    }

    /**
     * Associates a range of Virtual Shards with a connectionConfigKey
     *
     * @param  string  $_database
     * @param  string  $_connectionConfigKey
     * @param  integer $_fromVirtualShard
     * @param  integer $_toVirtualShard
     * @param  string  $_outputLogFile
     * @return boolean success
     */
    public function associationVirtualshardConnectionconfigkey($_database, $_connectionConfigKey, $_fromVirtualShard, $_toVirtualShard, $_outputLogFile)
    {
        self::$loggerConfig =  array (
            'active' => TRUE,
            'priority' => 8,
            'filename' => $_outputLogFile,
            'traceQueryOrigins' => FALSE,
        );

        if ((integer)$_fromVirtualShard > (integer)$_toVirtualShard) {
            echo "ERROR: parameter toVirtualShard must be equal or greater than fromVirtualShard.\n\n";
            return FALSE;
        }

        $shardConfig = self::getShardConfig($_database);
        if (($shardConfig->numberOfVirtualShards - 1) < (integer)$_toVirtualShard) {
            echo "ERROR: parameter toVirtualShard out of range. \n\n";
            return FALSE;
        }

        for ($virtualShard = $_fromVirtualShard; $virtualShard <= $_toVirtualShard; $virtualShard++)
        {
            $result = $this->_setVirtualShardAssociation($_database, $virtualShard, $_connectionConfigKey);
            if ($result !== FALSE) {
                self::getLogger()->info("VirtualShard " . $virtualShard . " was associated with connectionConfigKey ". $_connectionConfigKey);
            } else {
                self::getLogger()->info("ERROR: Aborted!!! Could not associate VirtualShard " . $virtualShard . " with connectionConfigKey ". $_connectionConfigKey);
                return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * Setup Shard Logger to registry
     *
     * @param  Tinebase_Log   $_loggerConfig
     */
    public static function setupLogger($_loggerConfig)
    {
        $logger = new Tinebase_Log();

        try {
            $logger->addWriterByConfig($_loggerConfig);

        } catch (Exception $e) {
            error_log("Tine 2.0 can't setup the configured logger! The Server responded: $e");
            $writer =  new Zend_Log_Writer_Null();
            $logger->addWriter($writer);
        }

        Tinebase_Core::set(self::SHARD_LOGGER, $logger);
    }


    /**
     * get config from the registry
     *
     * @return Tinebase_Log the logger
     */
    public static function getLogger()
    {
        if (! Tinebase_Core::get(self::SHARD_LOGGER) instanceof Tinebase_Shard_Log) {
            Tinebase_Shard_Manager::setupLogger(self::$loggerConfig);
        }

        return Tinebase_Core::get(self::SHARD_LOGGER);
    }
}