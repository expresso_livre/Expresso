<?php
/**
* Filesystem backend class for Association Shard key => ConnectionConfig key
*
* @package Shard
* @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
* @author Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
* @copyright Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
*
*/

/**
* Filesystem backend class for Association Shard key => ConnectionConfig key
* @package Shard
*/
class Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Filesystem implements Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Interface
{
   /**
    * Full path of file where content of this backend is persisted
    *
    * @var string
    */
    private $_fileName;

   /**
    * holds the instance of the singleton
    *
    * @var Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Filesystem
    */
    private static $_instance = NULL;

    /**
    * the constructor
    *
    * don't use the constructor. use the singleton
    */
    private function __construct() {}

    /**
    * don't clone. Use the singleton.
    */
    private function __clone() {}

    /**
    * the singleton pattern
    *
    * @param array  $_options
    * @param string $_database
    * @return Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Filesystem
    */
    public static function getInstance(array $_options, $_database)
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_Shard_Resharding_Association_Shardkey_Connectionconfigkey_Filesystem;
            self::$_instance->_fileName = $_options['filePath'] . DIRECTORY_SEPARATOR
                                        . 'associationShardkeyConnectionconfigkey.' . $_database . '.'
                                        . Tinebase_Config::getDomain();
            if (!file_exists(self::$_instance->_fileName)) {
                $associations = serialize(array('associationShardkeyConnectionconfigkey' => array()));
                $result = file_put_contents(self::$_instance->_fileName, $associations, LOCK_EX);
                if ($result === FALSE) {
                    throw new Exception('Can not create Association file: ' . $this->_fileName);
                }
            }
        }
        return self::$_instance;
    }

    /**
    * get Connectionconfigkey associated with given Shard key
    *
    * @param integer $_key
    * @return string | FALSE
    */
    public function getAssociation($_key)
    {
        $result = unserialize(file_get_contents($this->_fileName));
        if ($result === FALSE) {
            throw new Exception('Can not read from associations file: ' . $this->_fileName);
        }

        if (is_array($result['associationShardkeyConnectionconfigkey'])
            && array_key_exists($_key, $result['associationShardkeyConnectionconfigkey'])) {
            return $result['associationShardkeyConnectionconfigkey'][$_key];
        }

        return FALSE;
    }

    /**
    * add Shard key X Connectionconfigkey association
    *
    * @param integer $_key
    * @param string $_connectionConfigKey
    * @return boolean
    */
    public function addAssociation($_key, $_connectionConfigKey)
    {
        $result = unserialize(file_get_contents($this->_fileName));

        if ($result === FALSE) {
            throw new Exception('Can not read from Associations file: ' . $this->_fileName);
        }

        $result['associationShardkeyConnectionconfigkey'][$_key] = $_connectionConfigKey;
        $resultPut = file_put_contents($this->_fileName, serialize($result), LOCK_EX);

        if ($resultPut === FALSE) {
            throw new Exception('Can not write to Association file: ' . $this->_fileName);
        }

        return $resultPut;
    }

    /**
    * remove Shard key X Connectionconfigkey association
    *
    * @param integer $_key
    * @return boolean
    */
    public function removeAssociation($_key)
    {
        $result = unserialize(file_get_contents($this->_fileName));
        if ($result === FALSE) {
            throw new Exception('Can not read from Associations file: ' . $this->_fileName);
        }

        if (isset($result['associationShardkeyConnectionconfigkey'][$_key])) {
            unset($result['associationShardkeyConnectionconfigkey'][$_key]);
            return TRUE;
        }

        return FALSE;
    }

    /**
    * clear all associations
    *
    */
    public function clearAllAssociations()
    {
        $associations = serialize(array('associationShardkeyConnectionconfigkey' => array()));
        $result = file_put_contents(self::$_instance->_fileName, $associations, LOCK_EX);
        if ($result === FALSE) {
            throw new Exception('Can not write to Associations file: ' . $this->_fileName);
        }
    }
}