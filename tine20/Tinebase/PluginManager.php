<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  PluginManager
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 *
 */
/**
 * class to store plugin configuration
 *
 * @package     Tinebase
 * @subpackage  PluginManager
 */

final class Tinebase_PluginManager
{
    /**
     *
     * @var array
     */
    private static $_pluginConfigItems = array();

    /**
     *
     * @var array
     */
    private static $_globalPluginConfigItems = array();

    /**
     *
     * @var array
     */
    private static $_additionalRegistryData = array();

    /**
     * @param string $name
     * @param string $xtype
     * @param string $fieldLabel
     * @param mixed $value
     */
    public static function addPluginConfigItem($name, $xtype, $fieldLabel, $value = '')
    {
        $plugin = array();
        $plugin['name'] = $name;
        $plugin['xtype'] = $xtype;
        $plugin['fieldLabel'] = $fieldLabel;
        $plugin['value'] = $value;
        self::$_pluginConfigItems[] = $plugin;
    }

    /**
     * @param string $name
     * @param string $xtype
     * @param string $fieldLabel
     * @param mixed $value
     */
    public static function addGlobalPluginConfigItem($name, $xtype, $fieldLabel, $value = '')
    {
        $plugin = array();
        $plugin['name'] = $name;
        $plugin['xtype'] = $xtype;
        $plugin['fieldLabel'] = $fieldLabel;
        $plugin['value'] = $value;
        self::$_globalPluginConfigItems[] = $plugin;
    }

    /**
     *
     * @return array
     */
    public static function getPluginConfigItems()
    {
        return array('plugins' => self::$_pluginConfigItems);
    }

    /**
     *
     * @return array
     */
    public static function getGlobalPluginConfigItems()
    {
        return array('global' => array('plugins' => self::$_globalPluginConfigItems));
    }

    /**
     * $plugins is an array so:
     *
     * array(
     *     'TinebaseComponent' => array (
     *         'staticMethodName' => array(
     *         // arguments according method signature
     *         )
     *     )
     * )
     *
     * @param array $plugins
     * @return void
     * TODO use this method into init_plugins.php
     */
    public static function setPlugins(array $plugins)
    {
        foreach ($plugins as $component => $action){
            foreach ($action as $methodName => $arguments){
                $arguments = (array) $arguments; // ensures that one only argument is inside an array
                call_user_func_array(array('Tinebase_' . $component, $methodName), $arguments);
            }
        }
    }

    /**
     * Inject a dependency into Tinebase
     *
     * @param string $component
     * @param string $method
     * @param array | string $args
     * TODO use this method into init_plugins.php
     */
    public static function setPlugin($component, $method, $args)
    {
        $args = (array) $args;
        call_user_func_array(array('Tinebase_' . $component, $method), $args);
    }

    /**
     * $plugins is an array with classnames
     * Classes must implement init() method
     * and can have any namespace
     *
     * @param array $plugins
     */
    public static function initPlugins(array $plugins)
    {
        foreach($plugins as $component){
            call_user_func(array($component, 'init'));
        }
    }

    /**
     * $plugins is an array so:
     *
     * array(
     *     'ThirdPartyComponent' => array (
     *         'staticMethodName' => array(
     *         // arguments according method signature
     *         )
     *     )
     * )
     *
     * @param array $plugins
     * @return void
     * TODO use this method into init_plugins.php
     */
    public static function setThirdPartyPlugins(array $plugins)
    {
        foreach ($plugins as $component => $action){
            foreach ($action as $methodName => $arguments){
                $arguments = (array) $arguments; // ensures that one only argument is inside an array
                call_user_func_array(array($component, $methodName), $arguments);
            }
        }
    }

    /**
     * Inject a dependency into Tinebase
     *
     * @param string $component
     * @param string $method
     * @param array | string $args
     * TODO use this method into init_plugins.php
     */
    public static function setThirdPartyPlugin($component, $method, $args)
    {
        $args = (array) $args;
        call_user_func_array(array($component, $method), $args);
    }

    /**
     *
     * @param array $methods
     * @param string $namespace
     * @see Tinebase_Frontend_Abstract::attachPlugins()
     */
    public static function attachFrontendPlugins(array $methods, $namespace)
    {
        Tinebase_Frontend_Abstract::attachPlugins($methods, $namespace);
    }

    /**
     *
     * @param array $methods
     * @param string $namespace
     * @see Tinebase_Controller_Abstract::attachPlugins()
     */
    public static function attachControllerPlugins(array $methods, $namespace)
    {
        Tinebase_Controller_Abstract::attachPlugins($methods, $namespace);
    }

    /**
     *
     * @param array $methods
     * @param string $namespace
     * @see Tinebase_Backend_Abstract::attachPlugins()
     */
    public static function attachBackendPlugins(array $methods, $namespace)
    {
        Tinebase_Backend_Abstract::attachPlugins($methods, $namespace);
    }

    /**
     * Supports $value as Closure
     * @param string $key
     * @param mixed $value
     */
    public static function addRegistryDataElement($key, $value)
    {
        self::$_additionalRegistryData[$key] = $value;
    }

    /**
     * Processes values ensuring that Closures will be called
     * @return array
     */
    public static function getAdditionalRegistryData()
    {
        foreach (self::$_additionalRegistryData as $key => $value){
            if (is_callable($value)){
                self::$_additionalRegistryData[$key] = $value();
            }
        }
        return self::$_additionalRegistryData;
    }
 }
