<?php
/**
 * Calendar Event Notifications
 *
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Calendar Event Notifications
 *
 * @package     Calendar
 */
 class Calendar_Controller_EventNotifications
 {
     const NOTIFICATION_LEVEL_NONE                      =  0;
     const NOTIFICATION_LEVEL_INVITE_CANCEL             = 10;
     const NOTIFICATION_LEVEL_EVENT_RESCHEDULE          = 20;
     const NOTIFICATION_LEVEL_EVENT_UPDATE              = 30;
     const NOTIFICATION_LEVEL_ATTENDEE_STATUS_UPDATE    = 40;
     const NOTIFICATION_LEVEL_ATTENDEES_NONE            =  50;
     const NOTIFICATION_LEVEL_ATTENDEES_ASK             =  60;
     const NOTIFICATION_LEVEL_ATTENDEES_AUTOMATIC       =  70;
     const INVITATION_ATTACHMENT_MAX_FILESIZE           = 2097152; // 2 MB

    /**
     * @var Calendar_Controller_EventNotifications
     */
    private static $_instance = NULL;
    
    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone()
    {
    }
    
    /**
     * the singleton pattern
     *
     * @return Calendar_Controller_EventNotifications
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Calendar_Controller_EventNotifications();
        }
        
        return self::$_instance;
    }
    
    /**
     * constructor
     *
     */
    private function __construct()
    {
        
    }
    
    /**
     * get updates of human interest
     *
     * @param  Calendar_Model_Event $_event
     * @param  Calendar_Model_Event $_oldEvent
     * @return array
     */
    protected function _getUpdates($_event, $_oldEvent)
    {
        // check event details
        $diff = $_event->diff($_oldEvent)->diff;
        
        $orderedUpdateFieldOfInterest = array(
            'dtstart', 'dtend', 'summary', 'location', 'description',
            'transp', 'priority', 'status', 'class',
            'url', 'rrule', 'is_all_day_event', 'originator_tz', /*'tags', 'notes',*/
        );
        
        $updates = array();
        foreach ($orderedUpdateFieldOfInterest as $field) {
            if (isset($diff[$field]) || array_key_exists($field, $diff)) {
                $updates[$field] = $diff[$field];
            }
        }

        // rrule legacy
        if ((isset($updates['rrule']) || array_key_exists('rrule', $updates))) {
            $updates['rrule'] = $_oldEvent->rrule;
        }
        
        // cehck for organizer update
        if (Tinebase_Record_Abstract::convertId($_event['organizer'], 'Addressbook_Model_Contact') !=
            Tinebase_Record_Abstract::convertId($_oldEvent['organizer'], 'Addressbook_Model_Contact')) {
            
            $updates['organizer'] = $_event->resolveOrganizer();
        }
        
        // check attendee updates
        $attendeeMigration = Calendar_Model_Attender::getMigration($_oldEvent->attendee, $_event->attendee);
        foreach ($attendeeMigration['toUpdate'] as $attendee) {
            $oldAttendee = Calendar_Model_Attender::getAttendee($_oldEvent->attendee, $attendee);
            if ($attendee->status == $oldAttendee->status) {
                $attendeeMigration['toUpdate']->removeRecord($attendee);
            }
        }
        
        foreach($attendeeMigration as $action => $migration) {
            Calendar_Model_Attender::resolveAttendee($migration, FALSE);
            if (! count($migration)) {
                unset($attendeeMigration[$action]);
            }
        }
        
        if (! empty($attendeeMigration)) {
            $updates['attendee'] = $attendeeMigration;
        }
        
        return $updates;
    }

    /**
     * send notifications
     *
     * @param Calendar_Model_Event       $_event
     * @param Tinebase_Model_FullAccount $_updater
     * @param Sting                      $_action
     * @param Calendar_Model_Event       $_oldEvent
     * @param Tinebase_Model_Alarm       $_alarm
     * @param array                      $attachs
     * @return void
     */
    public function doSendNotifications($_event, $_updater, $_action, $_oldEvent=NULL, $_alarm=NULL, $attachs=NULL)
    {
            // we only send notifications to attendee
        if (! $_event->attendee instanceof Tinebase_Record_RecordSet) {
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                . " Event has no attendee");
            return;
        }

        if ($_event->dtend === NULL) {
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                . " ". print_r($_event->toArray(), TRUE));
            throw new Tinebase_Exception_UnexpectedValue('no dtend set in event');
        }
        
        if (Tinebase_DateTime::now()->subHour(1)->isLater($_event->dtend)) {
            if ($_action == 'alarm' || ! ($_event->isRecurException() || $_event->rrule)) {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                    . " Skip notifications to past events");
                return;
            }
        }

        $notificationPeriodConfig = Calendar_Config::getInstance()->get(Calendar_Config::MAX_NOTIFICATION_PERIOD_FROM);
        if (Tinebase_DateTime::now()->subWeek($notificationPeriodConfig)->isLater($_event->dtend)) {
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                . " Skip notifications to past events (MAX_NOTIFICATION_PERIOD_FROM: " . $notificationPeriodConfig . " week(s))");
            return;
        }

        // lets resolve attendee once as batch to fill cache
        $attendee = clone $_event->attendee;
        Calendar_Model_Attender::resolveAttendee($attendee);
        
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__
            . " " . print_r($_event->toArray(), true));
        if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
            . " Notification action: " . $_action);

        switch ($_action) {
            case 'alarm':
                foreach($_event->attendee as $attender) {
                    if (Calendar_Model_Attender::isAlarmForAttendee($attender, $_alarm)) {
                        $this->sendNotificationToAttender($attender, $_event, $_updater, $_action, self::NOTIFICATION_LEVEL_NONE);
                    }
                }
                break;
            case 'created':
                foreach($_event->attendee as $attender) {
                    $this->sendNotificationToAttender($attender, $_event, $_updater, $_action, self::NOTIFICATION_LEVEL_INVITE_CANCEL, FALSE, $attachs);
                }
                break;
            case 'deleted':
                foreach($_event->attendee as $attender) {
                    $this->sendNotificationToAttender($attender, $_event, $_updater, $_action, self::NOTIFICATION_LEVEL_INVITE_CANCEL);
                }
                break;
            case 'changed':
                $attendeeMigration = Calendar_Model_Attender::getMigration($_oldEvent->attendee, $_event->attendee);
                foreach ($attendeeMigration['toCreate'] as $attender) {
                    $this->sendNotificationToAttender($attender, $_event, $_updater, 'created', self::NOTIFICATION_LEVEL_INVITE_CANCEL, FALSE, $attachs);
                }

                foreach ($attendeeMigration['toDelete'] as $attender) {
                    // insert attender to new event;
                    $newEvent = clone ($_oldEvent);
                    $attendeeTokeep = Calendar_Model_Attender::getAttendee($_oldEvent->attendee, $attender);
                    $newEvent->attendee = array();
                    $newEvent->attendee->addRecord($attendeeTokeep);
                    $this->sendNotificationToAttender($attender, $newEvent, $_updater, 'deleted', self::NOTIFICATION_LEVEL_INVITE_CANCEL);
                }

                // NOTE: toUpdate are all attendee to be notified
                if (count($attendeeMigration['toUpdate']) > 0) {
                    $updates = $this->_getUpdates($_event, $_oldEvent);
                    
                    if (empty($updates)) {
                        if(!$attachs) {
                            Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " empty update, nothing to notify about");
                            return;
                        }
                    }
                    
                    // compute change type
                    if (count(array_intersect(array('dtstart', 'dtend'), array_keys($updates))) > 0 ||
                        count(array_intersect(array('location'), array_keys($updates))) > 0) {
                        $notificationLevel = self::NOTIFICATION_LEVEL_EVENT_RESCHEDULE;
                    } else if (count(array_diff(array_keys($updates), array('attendee'))) > 0) {
                        $notificationLevel = self::NOTIFICATION_LEVEL_EVENT_UPDATE;
                    } else {
                        $notificationLevel = self::NOTIFICATION_LEVEL_ATTENDEE_STATUS_UPDATE;
                    }
                    
                    // send notifications
                    foreach ($attendeeMigration['toUpdate'] as $attender) {
                        $this->sendNotificationToAttender($attender, $_event, $_updater, 'changed', $notificationLevel, $updates, $attachs);
                    }
                }
                
                break;
                
            default:
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " unknown action '$_action'");
                break;
                
        }

        // SEND REPLY/COUNTER to external organizer
        if ($_event->organizer && ! $_event->resolveOrganizer()->account_id && count($_event->attendee) == 1) {
            $updates = array('attendee' => array('toUpdate' => $_event->attendee));
            $organizer = new Calendar_Model_Attender(array(
                'user_type'  => Calendar_Model_Attender::USERTYPE_USER,
                'user_id'    => $_event->resolveOrganizer()
            ));
            $this->sendNotificationToAttender($organizer, $_event, $_updater, 'changed', self::NOTIFICATION_LEVEL_ATTENDEE_STATUS_UPDATE, $updates);
        }
    }

    /**
     * send notification to a single attender
     *
     * @param Calendar_Model_Attender    $_attender
     * @param Calendar_Model_Event       $_event
     * @param Tinebase_Model_FullAccount $_updater
     * @param Sting                      $_action
     * @param String                     $_notificationLevel
     * @param array                      $_updates
     * @param array                      $attachs TODO remove it
     * @return void
     */
    public function sendNotificationToAttender($_attender, $_event, $_updater, $_action, $_notificationLevel, $_updates=NULL, $attachs = FALSE)
    {
        try {
            $organizer = $_event->resolveOrganizer();
            $organizerAccountId = $organizer->account_id;
            $attendee = $_attender->getResolvedUser();
            $attendeeAccountId = $_attender->getUserAccountId();

            $prefUserId = $attendeeAccountId ? $attendeeAccountId :
                            ($organizerAccountId ? $organizerAccountId :
                            ($_event->created_by));

            try {
                $prefUser = Tinebase_User::getInstance()->getFullUserById($prefUserId);
            } catch (Exception $e) {
                $prefUser = Tinebase_Core::getUser();
                $prefUserId = $prefUser->getId();
            }

            // get prefered language, timezone and notification level
            $locale = Tinebase_Translation::getLocale(Tinebase_Core::getPreference()->getValueForUser(Tinebase_Preference::LOCALE, $prefUserId));
            $timezone = Tinebase_Core::getPreference()->getValueForUser(Tinebase_Preference::TIMEZONE, $prefUserId);
            $translate = Tinebase_Translation::getTranslation('Calendar', $locale);
            $sendLevel = Tinebase_Core::getPreference('Calendar')->getValueForUser(Calendar_Preference::NOTIFICATION_LEVEL, $prefUserId);
            $sendOnOwnActions = Tinebase_Core::getPreference('Calendar')->getValueForUser(Calendar_Preference::SEND_NOTIFICATION_OF_OWN_ACTIONS, $prefUserId);

            // external (non account) notification
            if (! $attendeeAccountId) {
                // external organizer needs status updates
                $sendLevel = is_object($organizer) && $_attender->getEmail() == $organizer->getPreferedEmailAddress() ? 40 : 30;
                $sendOnOwnActions = false;
            }

            // check if user wants this notification NOTE: organizer gets mails unless she set notificationlevel to NONE
            // NOTE prefUser is organzier for external notifications
            if (($attendeeAccountId == $_updater->getId() && ! $sendOnOwnActions)
                || ($sendLevel < $_notificationLevel && (
                    ((is_object($organizer) && method_exists($attendee, 'getPreferedEmailAddress') && $attendee->getPreferedEmailAddress() != $organizer->getPreferedEmailAddress())
                    || (is_object($organizer) && !method_exists($attendee, 'getPreferedEmailAddress') && $attendee->email != $organizer->getPreferedEmailAddress()))
                    || $sendLevel == self::NOTIFICATION_LEVEL_NONE)
                    )
                ) {
                if (!$attachs) {
                    if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                        . " Preferred notification level not reached -> skipping notification for {$_attender->getEmail()}");
                    return;
                }
            }

            $method = NULL; // NOTE $method gets set in _getSubject as referenced param
            $messageSubject = $this->_getSubject($_event, $_notificationLevel, $_action, $_updates, $timezone, $locale, $translate, $method);

            // we don't send iMIP parts to external attendee if config is active
            if (Calendar_Config::getInstance()->get(Calendar_Config::DISABLE_EXTERNAL_IMIP) && ! $attendeeAccountId) {
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                    . " External iMIP is disabled.");
                $method = NULL;
            }

            $view = new Zend_View();
            $view->setScriptPath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'views');

            $view->translate    = $translate;
            $view->timezone     = $timezone;
            $view->event        = $_event;
            $view->updater      = $_updater;
            $view->updates      = is_array($_updates) ? $_updates : array();

            $messageBody = $view->render('eventNotification.php');

            $calendarPart = null;
            $attachments = $this->_getAttachments($method, $_event, $_action, $_updater, $calendarPart, $attachs);

            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " receiver: '{$_attender->getEmail()}'");
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " subject: '$messageSubject'");
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " body: $messageBody");

            $sender = $_action == 'alarm' ? $prefUser : $_updater;
            Tinebase_Notification::getInstance()->send($sender, array($attendee), $messageSubject, $messageBody, $calendarPart, $attachments);
        } catch (Exception $e) {
            Tinebase_Exception::log($e);
            return;
        }
    }

    /**
     * get notification subject and method
     *
     * @param Calendar_Model_Event $_event
     * @param string $_notificationLevel
     * @param string $_action
     * @param array $_updates
     * @param string $timezone
     * @param Zend_Locale $locale
     * @param Zend_Translate $translate
     * @param atring $method
     * @return string
     */
    protected function _getSubject($_event, $_notificationLevel, $_action, $_updates, $timezone, $locale, $translate, &$method)
    {
        $startDateString = Tinebase_Translation::dateToStringInTzAndLocaleFormat($_event->dtstart, $timezone, $locale);
        $endDateString = Tinebase_Translation::dateToStringInTzAndLocaleFormat($_event->dtend, $timezone, $locale);
        $locationString = $_event->location;

        switch ($_action) {
        case 'alarm':
            $messageSubject = sprintf($translate->_('Alarm for event "%1$s" at %2$s'), $_event->summary, $startDateString);
            break;
        case 'created':
            $messageSubject = sprintf($translate->_('Event invitation "%1$s" at %2$s'), $_event->summary, $startDateString);
            $method = Calendar_Model_iMIP::METHOD_REQUEST;
            break;
        case 'deleted':
            $messageSubject = sprintf($translate->_('Event "%1$s" at %2$s has been canceled' ), $_event->summary, $startDateString);
            $method = Calendar_Model_iMIP::METHOD_CANCEL;
            break;
        case 'changed':
            switch ($_notificationLevel) {
            case self::NOTIFICATION_LEVEL_EVENT_RESCHEDULE:
                if ((isset($_updates['dtstart']) || array_key_exists('dtstart', $_updates))) {
                    $oldStartDateString = Tinebase_Translation::dateToStringInTzAndLocaleFormat($_updates['dtstart'], $timezone, $locale);
                    $messageSubject = sprintf($translate->_('Event "%1$s" has been rescheduled from %2$s to %3$s' ), $_event->summary, $oldStartDateString, $startDateString);
                    $method = Calendar_Model_iMIP::METHOD_REQUEST;
                    break;
                }
                else if ((isset($_updates['location']) || array_key_exists('location', $_updates))) {
                    $oldLocationString = $_updates['location'];
                    $messageSubject = sprintf($translate->_('Event "%1$s" has been moved from %2$s to %3$s' ), $_event->summary, $oldLocationString, $locationString);
                    $method = Calendar_Model_iMIP::METHOD_REQUEST;
                    break;
                }
                // fallthrough if dtstart or location didn't change

                case self::NOTIFICATION_LEVEL_EVENT_UPDATE:
                    $messageSubject = sprintf($translate->_('Event "%1$s" at %2$s has been updated' ), $_event->summary, $startDateString);
                    $method = Calendar_Model_iMIP::METHOD_REQUEST;
                    break;

                case self::NOTIFICATION_LEVEL_ATTENDEE_STATUS_UPDATE:
                    if(! empty($_updates['attendee']) && ! empty($_updates['attendee']['toUpdate']) && count($_updates['attendee']['toUpdate']) == 1) {
                        // single attendee status update
                        $attender = $_updates['attendee']['toUpdate']->getFirstRecord();

                        switch ($attender->status) {
                            case Calendar_Model_Attender::STATUS_ACCEPTED:
                                $messageSubject = sprintf($translate->_('%1$s accepted event "%2$s" at %3$s' ), $attender->getName(), $_event->summary, $startDateString);
                                break;

                            case Calendar_Model_Attender::STATUS_DECLINED:
                                $messageSubject = sprintf($translate->_('%1$s declined event "%2$s" at %3$s' ), $attender->getName(), $_event->summary, $startDateString);
                                break;

                            case Calendar_Model_Attender::STATUS_TENTATIVE:
                                $messageSubject = sprintf($translate->_('Tentative response from %1$s for event "%2$s" at %3$s' ), $attender->getName(), $_event->summary, $startDateString);
                                break;

                            case Calendar_Model_Attender::STATUS_NEEDSACTION:
                                $messageSubject = sprintf($translate->_('No response from %1$s for event "%2$s" at %3$s' ), $attender->getName(), $_event->summary, $startDateString);
                                break;
                        }
                        $method = Calendar_Model_iMIP::METHOD_REPLY;
                        break;
                    } else {
                        $messageSubject = sprintf($translate->_('Attendee changes for event "%1$s" at %2$s' ), $_event->summary, $startDateString);
                        $method = Calendar_Model_iMIP::METHOD_REQUEST;
                    }

                    // we don't send iMIP parts to organizers with an account cause event is already up to date
                    if ($_event->organizer && !$_event->resolveOrganizer()->account_id) {
                        $method = Calendar_Model_iMIP::METHOD_REPLY;
                    }
                    break;
                }
                break;
        default:
            $messageSubject = 'unknown action';
            if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . " unknown action '$_action'");
            break;
        }

        return $messageSubject;
    }

    /**
     * get notification attachments
     *
     * @param string $method
     * @param Calendar_Model_Event $event
     * @param string $_action
     * @param Tinebase_Model_FullAccount $updater
     * @param Zend_Mime_Part $calendarPart
     * @return array
     */
    protected function _getAttachments($method, $event, $_action, $updater, &$calendarPart, $attachs)
    {
        if ($method === NULL) {
            if(!$attachs) {
                return array();
            } else {
                $method = Calendar_Model_iMIP::METHOD_REQUEST;
            }
        }

        $vcalendar = $this->_createVCalendar($event, $method, $updater);

        $calendarPart           = new Zend_Mime_Part($vcalendar->serialize());
        $calendarPart->charset  = 'UTF-8';
        $calendarPart->type     = 'text/calendar; method=' . $method;
        $calendarPart->encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE;


        $attachment = new Zend_Mime_Part($vcalendar->serialize());
        $attachment->type     = 'application/ics';
        $attachment->encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE;
        $attachment->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $attachment->filename = 'event.ics';

        $attachments = array($attachment);

        if($attachs) {
            foreach($attachs as $file) {
                $stream = fopen($file['tempFile']['path'], 'r');
                $part = new Zend_Mime_Part($stream);
                $part->type = $file['tempFile']['type'];
                $part->encoding = Zend_Mime::ENCODING_BASE64;
                $part->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $part->filename = $file['tempFile']['name'];
                $attachments[] = $part;
            }
        }

        // add other attachments (only on invitation)
        //if ($_action == 'created') {
        //    $eventAttachments = $this->_getEventAttachments($event);
        //    $attachments = array_merge($attachments, $eventAttachments);
        //}

        return $attachments;
    }

    /**
     * create iMIP VCALENDAR
     *
     * @param Calendar_Model_Event $event
     * @param string $method
     * @param Tinebase_Model_FullAccount $updater
     * @return Sabre\VObject\Component
     */
    protected function _createVCalendar($event, $method, $updater)
    {
        $converter = Calendar_Convert_Event_VCalendar_Factory::factory(Calendar_Convert_Event_VCalendar_Factory::CLIENT_GENERIC);
        $converter->setMethod($method);
        $vcalendar = $converter->fromTine20Model($event);

        foreach ($vcalendar->children() as $component) {
            if ($component->name == 'VEVENT') {
                if ($method != Calendar_Model_iMIP::METHOD_REPLY && $event->organizer !== $updater->contact_id) {
                    if (isset($component->{'ORGANIZER'})) {
                        // in Tine 2.0 non organizers might be given the grant to update events
                        // @see rfc6047 section 2.2.1 & rfc5545 section 3.2.18
                        $component->{'ORGANIZER'}->add('SENT-BY', 'mailto:' . $updater->accountEmailAddress);
                    }
                } else if ($method == Calendar_Model_iMIP::METHOD_REPLY) {
                    // TODO in Tine 2.0 status updater might not be updater
                    $component->{'REQUEST-STATUS'} = '2.0;Success';
                }
            }
        }

        return $vcalendar;
    }

    /**
     * get event attachments
     *
     * @param Calendar_Model_Event $_event
     * @return array of Zend_Mime_Part
     */
    protected function _getEventAttachments($_event)
    {
        $attachments = array();
        foreach ($_event->attachments as $attachment) {
            if ($attachment->size < self::INVITATION_ATTACHMENT_MAX_FILESIZE) {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                        . " Adding attachment " . $attachment->name . ' to invitation mail');

                $path = Tinebase_Model_Tree_Node_Path::STREAMWRAPPERPREFIX
                . Tinebase_FileSystem_RecordAttachments::getInstance()->getRecordAttachmentPath($_event)
                . '/' . $attachment->name;

                $handle = fopen($path, 'r');
                $stream = fopen("php://temp", 'r+');
                stream_copy_to_stream($handle, $stream);
                rewind($stream);

                $part              = new Zend_Mime_Part($stream);
                $part->encoding    = Zend_Mime::ENCODING_BASE64; // ?
                $part->filename    = $attachment->name;
                $part->setTypeAndDispositionForAttachment($attachment->contenttype, $attachment->name);

                fclose($handle);

                $attachments[] = $part;

            } else {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__
                        . " Not adding attachment " . $attachment->name . ' to invitation mail (size: ' . Tinebase_Helper::convertToMegabytes($attachment-size) . ')');
            }
        }

        return $attachments;
    }
 }
