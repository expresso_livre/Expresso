/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
/**
 * @class Tine.Calendar.DatePickerHolidays
 * @extends Tine.Calendar.DatePicker
 * <p>Show holidays in mini calendar</p>
 */
Tine.Calendar.DatePickerHolidays = Ext.extend(Tine.widgets.DatePicker, {
        /**
     * @cfg {String} todayTip
     * A string used to format the message for displaying in a tooltip over the button that
     * selects the current date. Defaults to <code>'{0} (Spacebar)'</code> where
     * the <code>{0}</code> token is replaced by today's date.
     */
    todayTip : '{0}',

    // private
    initComponent : function(){
        Tine.Calendar.DatePickerHolidays.superclass.initComponent.call(this);

        this.value = this.value ?
                 this.value.clearTime(true) : new Date().clearTime();

        this.addEvents(
            /**
             * @event select
             * Fires when a date is selected
             * @param {DatePickerHolidays} this DatePickerHolidays
             * @param {Date} date The selected date
             */
            'select'
        );

        if(this.handler){
            this.on('select', this.handler,  this.scope || this);
        }

        this.initDisabledDays();
    },
    
    // private
    update : function(date, forceRefresh){
        if(this.rendered){
	        var vd = this.activeDate, vis = this.isVisible();
	        this.activeDate = date;
	        if(!forceRefresh && vd && this.el){
	            var t = date.getTime();
	            if(vd.getMonth() == date.getMonth() && vd.getFullYear() == date.getFullYear()){
	                this.cells.removeClass('x-date-selected');
	                this.cells.each(function(c){
	                   if(c.dom.firstChild.dateValue == t){
	                       c.addClass('x-date-selected');
	                       if(vis && !this.cancelFocus){
	                           Ext.fly(c.dom.firstChild).focus(50);
	                       }
	                       return false;
	                   }
	                }, this);
	                return;
	            }
	        }
	        var days = date.getDaysInMonth(),
	            firstOfMonth = date.getFirstDateOfMonth(),
	            startingPos = firstOfMonth.getDay()-this.startDay;
	
	        if(startingPos < 0){
	            startingPos += 7;
	        }
	        days += startingPos;
	
	        var pm = date.add('mo', -1),
	            prevStart = pm.getDaysInMonth()-startingPos,
	            cells = this.cells.elements,
	            textEls = this.textNodes,
	            // convert everything to numbers so it's fast
		    /*
	            day = 86400000,
	            d = (new Date(pm.getFullYear(), pm.getMonth(), prevStart)).clearTime(),
	            today = new Date().clearTime().getTime(),
	            sel = date.clearTime(true).getTime(),
		    */
		   	
		    d = (new Date(pm.getFullYear(), pm.getMonth(), prevStart, this.initHour)),
		    today = new Date().clearTime().getTime(),
		    sel = date.clearTime(true).getTime(),
	            min = this.minDate ? this.minDate.clearTime(true) : Number.NEGATIVE_INFINITY,
	            max = this.maxDate ? this.maxDate.clearTime(true) : Number.POSITIVE_INFINITY,
	            ddMatch = this.disabledDatesRE,
	            ddText = this.disabledDatesText,
	            ddays = this.disabledDays ? this.disabledDays.join('') : false,
	            ddaysText = this.disabledDaysText,
	            format = this.format;
 
	        if(this.showToday){
	            var td = new Date().clearTime(),
	                disable = (td < min || td > max ||
	                (ddMatch && format && ddMatch.test(td.dateFormat(format))) ||
	                (ddays && ddays.indexOf(td.getDay()) != -1));
	
	            if(!this.disabled){
	                this.todayBtn.setDisabled(disable);
	                this.todayKeyListener[disable ? 'disable' : 'enable']();
	            }
	        }
	
	        var setCellClass = function(cal, cell){
	            cell.title = '';
	            //var t = d.getTime();
		    var t = d.clearTime(true).getTime();
	            cell.firstChild.dateValue = t;
	            if(t == today){
	                cell.className += ' x-date-today';
	                cell.title = cal.todayText;
	            }
	            if(t == sel){
	                cell.className += ' x-date-selected';
	                if(vis){
	                    Ext.fly(cell.firstChild).focus(50);
	                }
	            }
	            // disabling
	            if(t < min) {
	                cell.className = ' x-date-disabled';
	                cell.title = cal.minText;
	                return;
	            }
	            if(t > max) {
	                cell.className = ' x-date-disabled';
	                cell.title = cal.maxText;
	                return;
	            }
	            if(ddays){
	                if(ddays.indexOf(d.getDay()) != -1){
	                    cell.title = ddaysText;
	                    cell.className = ' x-date-disabled';
	                }
	            }
	            if(ddMatch && format){
	                var fvalue = d.dateFormat(format);
	                if(ddMatch.test(fvalue)){
	                    cell.title = ddText.replace('%0', fvalue);
	                    cell.className = ' x-date-disabled';
	                }
	            }
	        };
	
	        var i = 0;
	        for(; i < startingPos; i++) {
	            textEls[i].innerHTML = (++prevStart);
	            d.setDate(d.getDate()+1);
	            cells[i].className = 'x-date-prevday';
	            setCellClass(this, cells[i]);
	        }
	        for(; i < days; i++){
	            var intDay = i - startingPos + 1;
                    var auxDate = new Date((date.getMonth()+1).toString() + "/" + (intDay) + "/" + date.getFullYear().toString() + ' 23:59:59');
                    var auxDay = intDay > 9 ? intDay.toString() : '0' + intDay.toString();
                    var auxMonth = (date.getMonth()+1) > 9 ? (date.getMonth()+1).toString() : '0' + (date.getMonth()+1).toString();
                    textEls[i].innerHTML = (intDay)
                    textEls[i].title = '';
                    if(Tine.Calendar.registry.get('defaultContainer').holidays[date.getFullYear() + auxMonth + auxDay]) {
                        textEls[i].innerHTML = '<FONT COLOR="#FF3300"><b>' + (intDay) + '</b></FONT>';
                        textEls[i].title = Tine.Calendar.registry.get('defaultContainer').holidays[date.getFullYear() + auxMonth + auxDay];
                    } else {
                        if(auxDate.getDay() == '0') {
                            textEls[i].innerHTML = '<FONT COLOR="#000066"><b>' + (intDay) + '<b></FONT>';
                        }
                    }
	            d.setDate(d.getDate()+1);
	            cells[i].className = 'x-date-activeh';
	            setCellClass(this, cells[i]);
	        }
	        var extraDays = 0;
	        for(; i < 42; i++) {
	             textEls[i].innerHTML = (++extraDays);
	             d.setDate(d.getDate()+1);
	             cells[i].className = 'x-date-nextday';
	             setCellClass(this, cells[i]);
	        }
	
	        this.mbtn.setText(this.monthNames[date.getMonth()] + ' ' + date.getFullYear());
	
	        if(!this.internalRender){
	            var main = this.el.dom.firstChild,
	                w = main.offsetWidth;
	            this.el.setWidth(w + this.el.getBorderWidth('lr'));
	            Ext.fly(main).setWidth(w);
	            this.internalRender = true;
	            // opera does not respect the auto grow header center column
	            // then, after it gets a width opera refuses to recalculate
	            // without a second pass
	            if(Ext.isOpera && !this.secondPass){
	                main.rows[0].cells[1].style.width = (w - (main.rows[0].cells[0].offsetWidth+main.rows[0].cells[2].offsetWidth)) + 'px';
	                this.secondPass = true;
	                this.update.defer(10, this, [date]);
	            }
	        }
        }
    }
});
Ext.reg('Tine.Calendar.DatePickerHolidays', Tine.Calendar.DatePickerHolidays);
