
Ext.ns('Tine.Calendar');

Tine.Calendar.OptionsDialog = function(config) {
    
    Tine.Calendar.OptionsDialog.superclass.constructor.call(this, config);
    
    this.options = config.options || {};
    this.scope = config.scope || window;
};

/**
 * @namespace   Tine.Calendar
 * @class       Tine.Calendar.OptionsDialog
 * @extends     Ext.FormPanel
 */
Ext.extend(Tine.Calendar.OptionsDialog, Ext.FormPanel, {
    /**
     * @cfg {Array} options
     * @see {Ext.fom.CheckBoxGroup}
     */   
    options: null,
    /**
     * @cfg {Object} scope
     */
    scope: null,
    /**
     * @cfg {String} invalidText defaults to _('You need to select an option!')
     */
    invalidText: null,
    isInvalidnDays: false,
    isInvalidinitDate: false,
    /**
     * @cfg {Function} handler
     */
    handler: Ext.emptyFn,
    /**
     * @cfg {Boolean} allowCancel
     */
    allowCancel: true,
    
    windowNamePrefix: 'OptionsDialog',
    bodyStyle:'padding:5px',
    layout: 'fit',
    border: false,
    cls: 'tw-editdialog',
    anchor:'100% 100%',
    deferredRender: false,
    buttonAlign: null,
    bufferResize: 500,
    
    initComponent: function() {
        this.app = Tine.Tinebase.appMgr.get('Calendar');
        
        // init buttons and tbar
        this.initButtons();
        
        this.itemsName = this.id + '-radioItems';
        
        // get items for this dialog
        this.items = [{
            layout: 'hbox',
            border: false,
            layoutConfig: {
                align:'stretch'
            },
            items: [{
                border: false,
                html: '<div class="x-window-dlg"><div class="ext-mb-icon ext-mb-question"></div></div>',
                flex: 0,
                width: 45
            }, {
                border: false,
                layout: 'fit',
                flex: 1,
                autoScroll: true,
                items: [
                    {
                        xtype: 'label',
                        border: false,
                        cls: 'ext-mb-text'
                    }, 
                    {
                        xtype:'fieldset',
                        title: 'Dias:',
                        items: [{
                                xtype: 'datefield',
                                fieldLabel: this.app.i18n._('Start Date'),
                                name: 'dtstart',
                                requiredGrant: 'editGrant'
                                
                        },{
                                xtype: 'numberfield',
                                fieldLabel: this.app.i18n._('Number of days:'),
                                labelSeparator: '',
                                name: 'numOfDays',
                                inputValue: 'numOfDays',
                                width: 25
                        }]
                    }
                ]
            }]
        }];
        
        this.supr().initComponent.call(this);
    },
    
    /**
     * init buttons
     */
    initButtons: function() {
        this.fbar = ['->', {
            xtype: 'button',
            text: this.app.i18n._('Ok'),
            minWidth: 70,
            scope: this,
            handler: this.onOk,
            iconCls: 'action_saveAndClose'
        }, {
            xtype: 'button',
            text: this.app.i18n._('Cancel'),
            minWidth: 70,
            scope: this,
//            hidden: !this.allowCancel,
            handler: this.onCancel,
            iconCls: 'action_cancel'
        }];
    },
    
    onOk: function() {
        
        var stDate = this.getForm().findField('dtstart');
        var initDate = stDate.getValue();
        
        var nDays = this.getForm().findField('numOfDays');
        var numOfDays = nDays.getValue();
        
        if (numOfDays == '') {
            nDays.markInvalid(this.app.i18n._('Inform the number of days!'));
            this.isInvalidnDays = true;
        } else if (numOfDays > 45) {
            nDays.markInvalid(this.app.i18n._('Maximum value is 45!'));
            this.isInvalidnDays = true;
        }
        
        if (initDate == "") {
            stDate.markInvalid(this.app.i18n._('Inform inicial date!'));
            this.isInvalidinitDate = true;
        }
        
        if (this.isInvalidnDays || this.isInvalidinitDate) {
            
            this.isInvalidnDays = false;
            this.isInvalidinitDate = false;
            
            return;
        }
        
        var fields = Array(initDate,numOfDays);
        
        this.handler.call(this.scope, fields);
        this.window.close();
    },
    
    onCancel: function() {
        this.handler.call(this.scope, 'cancel');
        this.window.close();
    }
});

/**
 * grants dialog popup / window
 */
Tine.Calendar.OptionsDialog.openWindow = function (config) {
    var window = Tine.WindowFactory.getWindow({
        width: config.width || 400,
        height: config.height || 150,
        resizable: false,
        closable: false,
        name: Tine.Calendar.OptionsDialog.windowNamePrefix + Ext.id(),
        contentPanelConstructor: 'Tine.Calendar.OptionsDialog',
        contentPanelConstructorConfig: config,
        modal: true
    });
    return window;
};
