<?php
/**
 * Expresso Br
 * Test case that verifies the behavior of the login screen.
 *
 * @package ExpressoBrTest\Functional\Login
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Rafael Raymundo da Silva <rafael.silva@serpro.gov.br>
 * @author Marcelo Costa Toyama <marcelo.toyama@serpro.gov.br>
 * @copyright Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 */

namespace ExpressoBrTest\Functional\N1_Autenticacao;

use ExpressoBrTest\Functional\Generic\ExpressoBrTest;

use ExpressoBrTest\Functional\Page\LoginPage;

class ExpressoBrTest_Functional_N1_Autenticacao extends ExpressoBrTest
{
    /**
     * Logar no sistema com login incorreto
     *
     * CTV3-41
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-41
     *
     * Input data:
     *
     * - invalid.user: a invalid user login
     * - valid.password: valid password for the user
     */
    public function test_CTV3_41_LogarSistemaLoginIncorreto()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $INVALID_USER = $this->getTestValue('invalid.user');
        $VALID_PASSWORD = $this->getTestValue('valid.password');

        $loginPage->typeUser($INVALID_USER);
        $loginPage->typePassword($VALID_PASSWORD);

        $loginPage->clickLogin();
        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with an INVALID user');
    }

    /**
     * Logar no sistema com senha incorreta
     *
     * CTV3-43
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-43
     *
     * Input data:
     *
     * - valid.user: a valid user login
     * - invalid.password: invalid password for the user
     */
    public function test_CTV3_43_LogarSistemaSenhaIncorreta()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $VALID_USER = $this->getTestValue('valid.user');
        $INVALID_PASSWORD = $this->getTestValue('invalid.password');

        $loginPage->typeUser($VALID_USER);
        $loginPage->typePassword($INVALID_PASSWORD);

        $loginPage->clickLogin();
        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with an INVALID password');
    }

    /**
     * Logar no sistema com login vazio
     *
     * CTV3-44
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-44
     *
     * Input data:
     *
     * - invalid.password: invalid password for the user
     */
    public function test_CTV3_44_LogarSistemaLoginVazio()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $VALID_PASSWORD = $this->getTestValue('valid.password');

        $loginPage->typePassword($VALID_PASSWORD);

        $loginPage->clickLogin();
        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with an EMPTY user');
    }

    /**
     * Logar no sistema com senha vazia
     *
     * CTV3-45
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-45
     *
     * Input data:
     *
     * - valid.user: a valid user login
     */
    public function test_CTV3_45_LogarSistemaSenhaVazia()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $VALID_USER = $this->getTestValue('valid.user');

        $loginPage->typeUser($VALID_USER);

        $loginPage->clickLogin();
        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with an EMPTY password');
    }

    /**
     * Logar no sistema com login vazio
     *
     * CTV3-46
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-46
     *
     * Input data:
     *
     * - invalid.password: invalid password for the user
     */
    public function test_CTV3_46_LogarSistemaLoginUsandoCaracteresBranco()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $EMPTY_LOGIN = $this->getTestValue('empty.user');
        $VALID_PASSWORD = $this->getTestValue('valid.password');

        $loginPage->typeUser($EMPTY_LOGIN);
        $loginPage->typePassword($VALID_PASSWORD);

        $loginPage->clickLogin();
        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with a BLANK character on user');
    }

    /**
     * Logar no sistema com senha usando caracteres em branco
     *
     * CTV3-47
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-47
     *
     * Input data:
     *
     * - valid.user: a valid user login
     */
    public function test_CTV3_47_LogarSistemaSenhaUsandoCaracteresBranco()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $VALID_LOGIN = $this->getTestValue('valid.user');
        $EMPTY_PASSWORD = $this->getTestValue('empty.password');

        $loginPage->typeUser($VALID_LOGIN);
        $loginPage->typePassword($EMPTY_PASSWORD);

        $loginPage->clickLogin();
        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with a BLANK character on password');
    }

    /**
     * Logar no sistema com login usando caracteres especiais
     *
     * CTV3-48
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-48
     *
     * Input data:
     *
     * - specialCharacter.user: a valid user login
     * - valid.password: valid password for the user
     */
    public function test_CTV3_48_LogarSistemaLoginUsandoCaracteresEspeciais()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $SPECIALCHARACTER_USER = $this->getTestValue('specialCharacter.user');
        $VALID_PASSWORD = $this->getTestValue('valid.password');

        $loginPage->typeUser($SPECIALCHARACTER_USER);
        $loginPage->typePassword($VALID_PASSWORD);

        $loginPage->clickLogin();

        $this->assertTrue($loginPage->isMbContentPresent(), 'System is no longer at the login screen after a login attempt with a SPECIAL character on user');
    }

    /**
     * Logar no sistema com sucesso
     *
     * CTV3-1
     * http://comunidadeexpresso.serpro.gov.br/testlink/linkto.php?tprojectPrefix=CTV3&item=testcase&id=CTV3-1
     *
     * Input data:
     *
     * @param VALID_USER: a valid user login
     * @param VALID_PASSWORD: valid password for the user
     */
    public function test_CTV3_1_LogarSistemaSucesso()
    {
        $loginPage = new LoginPage($this);

        //load test data
        $VALID_USER = $this->getTestValue('valid.user');
        $VALID_PASSWORD = $this->getTestValue('valid.password');

        $this->assertTrue($loginPage->doLogin($VALID_USER,$VALID_PASSWORD), 'E-mail listing was not available after successful login');
    }
}