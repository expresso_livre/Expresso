<?php
/**
 * Expresso Br
 * Test case that verifies the behavior of the login screen.
 *
 * @package ExpressoBrTest\Functional\Login
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Rafael Raymundo da Silva <rafael.silva@serpro.gov.br>
 * @author Marcelo Costa Toyama <marcelo.toyama@serpro.gov.br>
 * @copyright Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 */

define('TEST_ROOT_PATH', dirname(__FILE__) . '/');

require_once (TEST_ROOT_PATH.'SplClassLoader.php');

if( !file_exists(TEST_ROOT_PATH.'test_conf.php') ){ // File with a valid ULR ,
    echo "test_conf.php does not exist\n";
    echo "File must exists with a valid ULR\n";
    echo "See test_conf.php.dist\n";
    exit;
}
require_once (TEST_ROOT_PATH.'test_conf.php');

$classLoader = new SplClassLoader('ExpressoBrTest', TEST_ROOT_PATH);
$classLoader->register();

\PHPUnit_Extensions_SeleniumTestCase::shareSession(true);
\PHPUnit_Extensions_Selenium2TestCase::shareSession(true);