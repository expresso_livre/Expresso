<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Acl
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Custom Test class for Tinebase_Acl_Roles
 */
class Custom_Tinebase_Acl_RolesTest extends Tinebase_Acl_RolesTest
{
    /**
     * @var array test objects
     */
    protected $objects = array();

    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     * Customized to sync a user from backend instead create a new one
     *
     * @access protected
     */
    protected function setUp()
    {
        TestCase::setUp(); //grandparent

        $this->objects['user'] = $this->_personas['sclever'];
        $this->objects['application'] = Tinebase_Application::getInstance()->getApplicationByName('Addressbook');
        $this->objects['role'] = new Tinebase_Model_Role(array(
            'id'                    => 10,
            'name'                  => 'phpunitrole',
            'description'           => 'test role for phpunit',
        ));
        $this->objects['role_2'] = new Tinebase_Model_Role(array(
            'id'                    => 11,
            'name'                  => 'phpunitrole 2',
            'description'           => 'test role 2 for phpunit',
        ));

        Tinebase_Acl_Roles::getInstance()->resetClassCache();
    }

    /**
     * Tears down the fixture
     * This method is called after a test is executed.
     * Customized because no user was created by setUp method
     * @access protected
     */
    protected function tearDown()
    {
        Tinebase_Acl_Roles::getInstance()->resetClassCache();

        // cleanup for testHasRight/test disabled app
        if (Tinebase_Application::getInstance()->getApplicationById($this->objects['application']->getId())->status == Tinebase_Application::DISABLED) {
            Tinebase_Application::getInstance()->setApplicationState($this->objects['application'], Tinebase_Application::ENABLED);
        }
        TestCase::tearDown(); //grandparent
    }
}
