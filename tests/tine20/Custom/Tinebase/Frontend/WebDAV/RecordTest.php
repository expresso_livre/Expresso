<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 */


/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Test class for Tinebase_Frontend_WebDAV_Record
 */
class Custom_Tinebase_Frontend_WebDAV_RecordTest extends Tinebase_Frontend_WebDAV_RecordTest
{

    /**
     * It is necessary to avoid skipping of all tests
     */
    public function testDoNothing()
    {
        $this->assertNull(NULL);
    }

    /*
     * Expressoes does not store attachments
     */
    public function testDownloadAttachment()
    {
        $this->markTestSkipped('Expresso does not store attachments');
    }

    /**
     * Expresso does not store attachments
     */
    public function testListAttachments()
    {
        $this->markTestSkipped('Expresso does not store attachments');
    }
}
