<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 */

/**
 * Test class for Tinebase_Frontend_Cli
 */
class Custom_Tinebase_Frontend_CliTest extends Tinebase_Frontend_CliTest
{
    /**
     * We must set username option to use an existing user to avoid creating a new cron user
     */
    public function testTriggerAsyncEvents()
    {
        $opts = new Zend_Console_Getopt('abp:');

        $opts->addRules(
            array(
              'username|u' => 'Username'
            )
        );
        $opts->setArguments(array());

        if (Tinebase_Config_Manager::isMultidomain()) {
            $opts->username = Tinebase_Core::getUser()->accountEmailAddress;
        } else {
            $opts->username = Tinebase_Core::getUser()->accountLoginName;
        }

        ob_start();
        $this->_cli->triggerAsyncEvents($opts);
        $out = ob_get_clean();

        $this->assertContains('Tine 2.0 scheduler run', $out, $out);
    }

    /**
     * Crm module is not present in Expresso
     * Test testPurgeDeletedRecordsAddressbook() is enought
     */
    public function testPurgeDeletedRecordsAllTables()
    {
        $this->markTestSkipped('Expresso does not have Crm module. Test testPurgeDeletedRecordsAddressbook() is enough');
    }

}