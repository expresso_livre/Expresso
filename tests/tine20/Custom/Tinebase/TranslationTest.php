<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2016 SERPRO (http://www.serpro.gov.br)
 *
 */

/**
 * Test class for Tinebase_Group
 */
class Custom_Tinebase_TranslationTest extends Tinebase_TranslationTest
{
    /**
     * Void test because custom translation is not necessary for Expresso
     * (non-PHPdoc)
     * @see Tinebase_TranslationTest::testCustomTranslations()
     */
    public function testCustomTranslations()
    {
        $this->markTestSkipped('Unnecessary test for Expresso');
    }

    /**
    * test french translations singular
    *
    * @see 0007014: Dates not formatted
    */
    public function testFrench()
    {
        $this->markTestSkipped('Expresso does not have French tranlation');
    }
}
