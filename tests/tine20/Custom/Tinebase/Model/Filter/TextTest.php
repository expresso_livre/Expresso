<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * Test class for Tinebase_Model_Filter_Text
 *
 * @package     Tinebase
 */
class Custom_Tinebase_Model_Filter_TextTest extends Tinebase_Model_Filter_TextTest
{
    /**
     * This customization is necessary because there is some particularities in
     * Ldap of Serpro. Any default user among the employees has a value in org_unit
     * attribute, but in Tine20, default users does not have it.
     */
    public function testEmptyStringValues()
    {
        $contact = Addressbook_Controller_Contact::getInstance()->get(Tinebase_Core::getUser()->contact_id);

        $filter = new Addressbook_Model_ContactFilter(array(
            array('field' => 'id',          'operator' => 'equals', 'value' => Tinebase_Core::getUser()->contact_id),
            array('field' => 'org_unit',    'operator' => 'equals', 'value' => ''),
        ));

        if($contact->org_unit) {
            $this->assertEquals(0, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'org_unit is NULL and should be included with empty string');
        } else {
            $this->assertEquals(1, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'org_unit is NOT NULL and should not be included with empty string');
        }

        $filter = new Addressbook_Model_ContactFilter(array(
            array('field' => 'id',          'operator' => 'equals', 'value' => Tinebase_Core::getUser()->contact_id),
            array('field' => 'n_fileas',    'operator' => 'equals', 'value' => ''),
        ));

        if($contact->n_fileas) {
            $this->assertEquals(0, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'n_fileas is NULL and should be included with empty string');
        } else {
            $this->assertEquals(1, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'n_fileas is set and should not be included with empty string');
        }

        $filter = new Addressbook_Model_ContactFilter(array(
            array('field' => 'id',          'operator' => 'equals', 'value' => Tinebase_Core::getUser()->contact_id),
            array('field' => 'org_unit',    'operator' => 'not',    'value' => ''),
        ));

        if($contact->org_unit) {
            $this->assertEquals(1, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'org_unit is NULL and should not be included with empty string with not operator');
        } else {
            $this->assertEquals(0, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'org_unit is NOT NULL and should be included with empty string with not operator');
        }

        $filter = new Addressbook_Model_ContactFilter(array(
            array('field' => 'id',          'operator' => 'equals', 'value' => Tinebase_Core::getUser()->contact_id),
            array('field' => 'n_fileas',    'operator' => 'not',    'value' => ''),
        ));

        if($contact->n_fileas) {
            $this->assertEquals(1, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'n_fileas is NULL and should not be included with empty string and not operator');
        } else {
            $this->assertEquals(0, count(Addressbook_Controller_Contact::getInstance()->search($filter)), 'n_fileas is set and should be included with empty string and not operator');
        }
    }
}
