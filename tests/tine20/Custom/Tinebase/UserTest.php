<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  User
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2009-2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Jonas Fischer <j.fischer@metaways.de>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Test class for Tinebase_User_Abstract
 */
class Custom_Tinebase_UserTest extends Tinebase_UserTest
{
    /**
     * Readonly server and no rollback for ldap
     */
    public function testPasswordPolicy()
    {
        $this->markTestSkipped('Readonly server and no rollback for ldap');
    }
    /**
     * Readonly server and no rollback for ldap
     */
    public function testPasswordPolicyUsername()
    {
        $this->markTestSkipped('Readonly server and no rollback for ldap');
    }

}