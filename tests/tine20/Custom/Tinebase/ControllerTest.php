<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 */

/**
 * Test class for Tinebase_ControllerTest
 */
class Custom_Tinebase_ControllerTest extends Tinebase_ControllerTest
{

    /**
     * testCleanupCache
     */
    public function testCleanupCache()
    {
        $this->_instance->cleanupCache(Zend_Cache::CLEANING_MODE_ALL);

        $cache = Tinebase_Core::getCache();
        $oldLifetime = $cache->getOption('lifetime');
        $cache->setLifetime(1);
        $cacheId = Tinebase_Helper::arrayToCacheId('testCleanupCache');
        $cache->save('value', $cacheId);
        sleep(3);

        // cleanup with CLEANING_MODE_OLD
        $this->_instance->cleanupCache();
        $cache->setLifetime($oldLifetime);

        $this->assertFalse($cache->load($cacheId));

        // check for cache files
        $config = Tinebase_Core::getConfig();

        if ($config->caching && $config->caching->backend == 'File' && $config->caching->path) {
            $cacheFile = $this->_lookForCacheFile($config->caching->path);
            $this->assertEquals(NULL, $cacheFile, 'found cache file: ' . $cacheFile);
        }
    }
}