<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

class Custom_Tinebase_TransactionManagerTest extends Tinebase_TransactionManagerTest
{

    /**
     * tests transaction ids
     * Override because $db initialization was changed by multidomain implementation
     */
    public function testUniqueTransactionIds()
    {
        $db = Tinebase_Core::getDb();

        $transactionId01 = $this->_instance->startTransaction($db);
        $this->assertEquals('string', gettype($transactionId01), 'TransactionId is not a string');
        $this->assertGreaterThan(10, strlen($transactionId01) , 'TransactionId is weak');

        $transactionId02 = $this->_instance->startTransaction($db);
        $this->assertNotEquals($transactionId01, $transactionId02, 'TransactionId is not unique');

        $this->_instance->rollBack();
    }

    /**
     * test one single transaction
     * Override because $db initialization was changed by multidomain implementation
     */
    public function testOneDbSingleTransaction()
    {
        $db = Tinebase_Core::getDb();

        $this->_createDbTestTable($db);
        $transactionId = $this->_instance->startTransaction($db);
        $db->insert($this->_testTableName, array(
            'Column1' => $transactionId
        ));
        $this->_instance->commitTransaction($transactionId);

        $columns = $db->fetchAll("SELECT * FROM " . $this->_testTableName . " WHERE " . $db->quoteInto($db->quoteIdentifier('Column1') . ' = ?', $transactionId) . ";");
        $this->assertEquals(1, count($columns), 'Transaction failed');
        $this->assertEquals($transactionId, $columns[0]['Column1'], 'Transaction was not executed properly');
    }

    /**
     * Override because $db initialization was changed by multidomain implementation
     */
    public function testOneDbRollback()
    {
        $db = Tinebase_Core::getDb();

        $this->_createDbTestTable($db);
        $transactionId = $this->_instance->startTransaction($db);
        $db->insert($this->_testTableName, array(
            'Column1' => $transactionId
        ));
        $this->_instance->rollBack();

        $columns = $db->fetchAll("SELECT * FROM " . $this->_testTableName . " WHERE " . $db->quoteInto($db->quoteIdentifier('Column1') . ' = ?', $transactionId) . ";");
        foreach ($columns as $column) {
            $this->assertNotEquals($transactionId, $column['Column1'], 'RollBack failed, data was inserted anyway');
        }
    }

    /**
     * Override Tine20 method because Expresso does not have Sales module
     * tests if nested transactions will be rolled back, if the outer fails
     */
    public function testNestedTransactions()
    {
        $containerId = NULL;

        $containerObject = Tinebase_Container::getInstance();

        $containerContentBackend  = new Tinebase_Backend_Sql(array(
            'modelName' => 'Tinebase_Model_ContainerContent',
            'tableName' => 'container_content',
        ));

        $tm = Tinebase_TransactionManager::getInstance();

        $tm->startTransaction(Tinebase_Core::getDb());

        try {
            // create container
            $container = $containerObject->addContainer(new Tinebase_Model_Container(array(
                'name'              => 'PHPUnit testNestedTransactions',
                'type'              => Tinebase_Model_Container::TYPE_PERSONAL,
                'owner_id'          => Tinebase_Core::getUser()->getId(),
                'backend'           => 'Sql',
                'model'             => 'Addressbook_Model_Contact',
                'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Addressbook')->getId()
            )));
            $containerId = $container->getId();

            $containerContentRecord = new Tinebase_Model_ContainerContent(array(
                'container_id' => $containerId,
                'action'       => Tinebase_Model_ContainerContent::ACTION_CREATE,
                'record_id'    => NULL, //record_id can not be NULL
                'time'         => Tinebase_DateTime::now(),
                'content_seq'  => 1,
            ));

            //Record Id can not be NULL. It will raise an exception.
            $containerContentBackend->create($containerContentRecord);
        } catch (Exception $e) {
            $tm->rollBack();
        }

        $this->setExpectedException('Tinebase_Exception_NotFound');

        // Try to get added container
        $addedContainer = $containerObject->get($containerId, FALSE);
    }
}
