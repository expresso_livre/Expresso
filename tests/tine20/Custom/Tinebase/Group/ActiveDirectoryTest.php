<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Group
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 SERPRO (http://www.serpro.gov.br)
 * @author      Jeferson Jose de Miranda <jeferson.miranda@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Custom test class for Tinebase_Group
 *
 * Expresso does not use Sql backend for groups
 */
class Custom_Tinebase_Group_ActiveDirectoryTest extends Tinebase_Group_ActiveDirectoryTest
{
    /**
     * Instantiating Tinebase_Group_ActiveDirectory results in error.
     */
    protected function setUp()
    {
        // do nothing
    }

    /**
     * It is necessary to avoid skipping of all tests
     */
    public function testDoNothing()
    {
        $this->assertNull(NULL);
    }

    public function testResolveUUIdToGIdNumber()
    {
        $this->markTestSkipped('Expresso dos not use Active Directory.');
    }

    public function testAddGroupInSyncBackend()
    {
        $this->markTestSkipped('Expresso dos not use Active Directory.');
    }

}
