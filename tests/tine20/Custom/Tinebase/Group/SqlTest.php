<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Group
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Custom test class for Tinebase_Group
 *
 * Expresso does not use Sql backend for groups
 */
class Custom_Tinebase_Group_SqlTest extends Tinebase_Group_SqlTest
{

    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
        TestCase::setUp(); //grandparent
    }

    /**
     * Must have at least one test not skipped
     * Do nothing
     */
    public function testDoNothing()
    {
        // Do nothing
    }

    /**
     * try to add a group
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testAddGroup()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * try to get all groups containing phpunit in their name
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testGetGroups()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * try to get the group with the name tine20phpunit
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testGetGroupByName()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * try to get a group by
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testGetGroupById()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * try to update a group
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testUpdateGroup()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * try to delete a group
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testDeleteGroups()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * testSetGroupMembershipsWithRecordset
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testSetGroupMembershipsWithRecordset()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }

    /**
     * testSetGroupMembershipsWithArray
     * Nothing to do
     * Expresso does not use sql backend
     */
    public function testSetGroupMembershipsWithArray()
    {
        $this->markTestSkipped('Expresso does not use sql backend for groups');
    }
}
