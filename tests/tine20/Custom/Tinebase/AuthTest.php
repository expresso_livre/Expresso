<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Auth
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Test class for Tinebase_Auth_Abstract
 */
class Custom_Tinebase_AuthTest extends Tinebase_AuthTest
{

    /**
     * test ldap authentication
     */
    public function testLdapAuth()
    {
        $this->assertEquals(Tinebase_Auth::LDAP, Tinebase_Auth::getConfiguredBackend());

        $testConfig = Zend_Registry::get('testConfig');

        // valid authentication
        $authResult = Tinebase_Auth::getInstance()->authenticate($testConfig->username, $testConfig->password, FALSE);
        $this->assertTrue($authResult->isValid());

        // invalid authentication
        $authResult = Tinebase_Auth::getInstance()->authenticate($testConfig->username, 'some pw', FALSE);
        $this->assertFalse($authResult->isValid());
        $this->assertEquals(Tinebase_Auth::FAILURE_CREDENTIAL_INVALID, $authResult->getCode());
        if ($testConfig->email) {
            $this->assertEquals(array('Invalid credentials for user ' . $testConfig->email, ''), $authResult->getMessages());
        }
    }

    /**
     * test imap authentication
     */
    public function testImapAuth()
    {
        $this->markTestSkipped('Expresso authenticates via Ldap');
    }
}
