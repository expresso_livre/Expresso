<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 SERPRO (http://www.serpro.gov.br)
 * @author      Jeferson Miranda <jeferson.miranda@serpro.gov.br>
 */

/**
 * Test class for Tinebase_CoreTest
 */
class Custom_Tinebase_CoreTest extends Tinebase_CoreTest
{
    public function testGetDispatchServerSnom()
    {
        $this->markTestSkipped("Expresso does not use Voipmanager Application");
    }

    public function testGetDispatchServerAsterisk()
    {
        $this->markTestSkipped("Expresso does not use Voipmanager Application");
    }
}