<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Account
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Jeferson Jose de Miranda <jeferson.miranda@serpro.gov.br>
 */

/**
 * Test class for Tinebase_User_Plugin_SambaTest
 */
class Custom_Tinebase_User_Plugin_SambaTest extends Tinebase_User_Plugin_SambaTest
{
    protected function setUp()
    {
        // do not mark skipped at setup
    }

    /**
     * Tears down the fixture
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
        if (isset($this->objects['users'])) {
            foreach ($this->objects['users'] as $user) {
                $this->_backend->deleteUser($user);
            }
        }
    }

    /**
     * It is necessary to avoid skipping of all tests
     */
    public function testDoNothing()
    {
        $this->assertNull(NULL);
    }

    /**
     * try to add an user
     *
     * @return Tinebase_Model_FullUser
     */
    public function testAddUser()
    {
        $this->markTestSkipped('Expresso does not use Samba Plugin.');
    }

    /**
     * try to update an user
     *
     */
    public function testUpdateUser()
    {
        $this->markTestSkipped('Expresso does not use Samba Plugin.');
    }

    /**
     * try to enable an account
     *
     */
    public function testSetStatus()
    {
        $this->markTestSkipped('Expresso does not use Samba Plugin.');
    }

    /**
     * try to set password
     *
     */
    public function testSetPassword()
    {
        $this->markTestSkipped('Expresso does not use Samba Plugin.');
    }

    /**
     * try to set the expirydate
     *
     */
    public function testSetExpiryDate()
    {
        $this->markTestSkipped('Expresso does not use Samba Plugin.');
    }
}