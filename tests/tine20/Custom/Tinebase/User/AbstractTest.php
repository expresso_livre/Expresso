<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  User
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2016 SERPRO (serpro.gov.br)
 * @author      Jeferson Jose de Miranda <jeferson.miranda@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Custom Test Class for AbstractTest
 */
class Custom_Tinebase_User_AbstractTest extends Tinebase_User_AbstractTest
{

    /**
     * Account DN
     * @var string
     */
    protected $_accountDN = null;

    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     *
     * custom: load and store config
     *
     * @access protected
     */
    protected function setUp()
    {
        $this->_uit = Tinebase_User::getInstance();

        $config = Zend_Registry::get('testConfig');
        if($this->_uit instanceof Tinebase_User_Ldap) {
            if ($config && isset($config->customtests) && isset($config->customtests->customAttributes)
                    && isset($config->customtests->customAttributes->Tinebase_Model_FullUser)
                    && isset($config->customtests->customAttributes->Tinebase_Model_FullUser->accountDN)) {
                $this->_accountDN = $config->customtests->customAttributes->Tinebase_Model_FullUser->accountDN;
            }
        }
    }

    /**
     * Creates a new test full user object
     *
     * @return Tinebase_Model_FullUser
     */
    protected function _getTestFullUser()
    {
        $smtpConfig = Tinebase_Config::getInstance()->get(Tinebase_Model_Config::SMTP, new Tinebase_Config_Struct())->toArray();

        $config = Zend_Registry::get('testConfig');
        if ($config && isset($config->maildomain)) {
            $emailDomain = $config->maildomain;
        } else if (isset($smtpConfig['primarydomain']) && ! empty($smtpConfig['primarydomain'])) {
            $emailDomain = $smtpConfig['primarydomain'];
        } else if (!empty(Tinebase_Core::getUser()->accountEmailAddress)) {
            list($user, $emailDomain) = explode('@', Tinebase_Core::getUser()->accountEmailAddress, 2);
        }

        $userData = array(
            'accountFirstName' => 'Leonie',
            'accountLastName'  => 'Weiss',
            'accountPrimaryGroup' => Tinebase_Core::getUser()->accountPrimaryGroup,
            'accountEmailAddress' => 'leonie.weiss@' . isset($emailDomain) ? $emailDomain : 'phpunit.com'
        );

        if(!empty($this->_accountDN)) {
            $userData['accountDN'] = $this->_accountDN;
        }

        $user = new Tinebase_Model_FullUser($userData, true);
        return $user;
    }

    /**
     * Verify if ldap backend is readonly
     * @return boolean
     */
    protected function _isReadOnlyBackend()
    {
        if($this->_uit instanceof Tinebase_User_Interface_SyncAble) {
            $userBackendConfigs = Tinebase_Config::getInstance()->get(Tinebase_Config::USERBACKEND);
            if (($userBackendConfigs->get('readonly') == 1) ||
                    ($userBackendConfigs->get('masterLdapHost') == '')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Test is null if backend is marked as read only
     */
    public function testIsNull()
    {
        if($this->_isReadOnlyBackend()) {
            $this->assertNull(null);
        }
    }

    /**
     * test generation of loginnames
     *
     * custom: add accountEmailAddress param
     */
    public function testGenerateUserName()
    {
        if ($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Backend is SyncAble and is marked as read only");
        } else {
            $user = $this->_getTestFullUser();
            $accountLoginNames = array(
                "weissle",
                "weissle00",
                "weissle01",
                "weissle02",
                "weissle03",
                "weissle04",
                "weissle05",
                "weissle06",
                "weissle07",
                "weissle08",
            );

            $createdUserIds = array();
            for ($i = 0; $i < 10; $i++) {
                $user->accountLoginName = $this->_uit->generateUserName($user);
                $createdUserIds[] = $this->_uit->addUser($user)->getId();
                $user->setId(NULL);
                $this->assertEquals($accountLoginNames[$i], $user->accountLoginName);
            }
            $this->_uit->deleteUsers($createdUserIds);
        }
    }

    /**
     * test User with the same Name
     * Suffix for accountLoginName and accountFullName
     *
     * custom: accountDN parameter read from config
     */
    public function testDoubleUserNames()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Backend is SyncAble and is marked as read only");
        } else {
            $user = $this->_getTestFullUser();
            $user->accountLoginName = $this->_uit->generateUserName($user);
            $user->accountFullName = $this->_uit->generateAccountFullName($user);
            $user1 = $this->_uit->addUser($user);

            $user = $this->_getTestFullUser();
            $user->accountLoginName = $this->_uit->generateUserName($user);
            $user->accountFullName = $this->_uit->generateAccountFullName($user);
            $user2 = $this->_uit->addUser($user);

            $this->assertEquals('weissle', $user1->accountLoginName);
            $this->assertEquals('weissle00', $user2->accountLoginName);

            $this->assertEquals('Leonie Weiss', $user1->accountFullName);
            $this->assertEquals('Leonie Weiss00', $user2->accountFullName);

            $this->_uit->deleteUser($user1->getId());
            $this->_uit->deleteUser($user2->getId());
        }
    }

    /*
     * custom: call to custom method _createAndStoreDummyUsers
     */
    public function testResolveUsersWithOneField()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Backend is SyncAble and is marked as read only");
        } else {
            $this->_createAndStoreDummyUsers(1);

            // test resolveUsers with one record and one field
            $dummyUser = $this->_objects[0];

            $dummyRecord = new Tinebase_Record_DummyRecord(array('test_1' => $dummyUser->getId()), true);

            $this->assertFalse($dummyRecord->test_1 instanceof Tinebase_Model_User);
            $this->_uit->resolveUsers($dummyRecord, 'test_1');
            $this->assertTrue($dummyRecord->test_1 instanceof Tinebase_Model_User);
        }
    }

    /*
     * custom: call to custom method _createAndStoreDummyUsers
     */
    public function testResolveUsersWithMultipleFields()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Backend is SyncAble and is marked as read only");
        } else {
            $this->_createAndStoreDummyUsers(2);
            $dummyRecord = new Tinebase_Record_DummyRecord(array('test_1' => $this->_objects[0]->getId(), 'test_2' => $this->_objects[1]->getId()), true);

            $this->_uit->resolveUsers($dummyRecord, array('test_1', 'test_2'));
            $this->assertTrue($dummyRecord->test_1 instanceof Tinebase_Model_User);
            $this->assertTrue($dummyRecord->test_2 instanceof Tinebase_Model_User);
            $this->assertNotEquals($dummyRecord->test_1->getId(), $dummyRecord->test_2->getId());
        }
    }

    /*
     * custom: call to custom method _createAndStoreDummyUsers
     */
    public function testResolveMultipleUsersWithMultipleFields()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Backend is SyncAble and is marked as read only");
        } else {
            $this->_createAndStoreDummyUsers(3);

            $dummyRecordSet = new Tinebase_Record_RecordSet('Tinebase_Record_DummyRecord');
            $dummyRecordSet->addRecord(new Tinebase_Record_DummyRecord(array('test_1' => $this->_objects[0]->getId(), 'test_2' => $this->_objects[1]->getId()), true));
            $dummyRecordSet->addRecord(new Tinebase_Record_DummyRecord(array('test_1' => $this->_objects[0]->getId()), true));
            $this->_uit->resolveMultipleUsers($dummyRecordSet, array('test_1', 'test_2'));
            $this->assertTrue($dummyRecordSet[0]->test_1 instanceof Tinebase_Model_User);
            $this->assertEquals($dummyRecordSet[0]->test_1->getId(), $this->_objects[0]->getId());
            $this->assertTrue($dummyRecordSet[0]->test_2 instanceof Tinebase_Model_User);
            $this->assertTrue($dummyRecordSet[1]->test_1 instanceof Tinebase_Model_User);
            $this->assertNull($dummyRecordSet[1]->test_2);
        }
    }

    /*
     * custom: get group from current user
     * custom: accountDN parameter read from config
     */
    protected function _createAndStoreDummyUsers($count)
    {
        for ($i=0; $i<$count; $i++) {
            $dummyUser = new Tinebase_Model_FullUser(array(
                'accountLoginName'      => 'dummy_'.$i,
                'accountStatus'         => 'enabled',
                'accountExpires'        => NULL,
                'accountPrimaryGroup'   => Tinebase_Core::getUser()->accountPrimaryGroup,
                'accountLastName'       => 'Dummy',
                'accountFirstName'      => 'No.'.$i,
                'accountEmailAddress'   => 'phpunit@metaways.de'
            ));

            if (!empty($this->_accountDN)) {
                $dummyUser['accountDN'] = $this->_accountDN;
            }

            $dummyUser = $this->_uit->addUser($dummyUser);
            $this->_objects[] = $dummyUser;
        }
    }
}
