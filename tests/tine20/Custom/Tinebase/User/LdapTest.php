<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  User
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 Serpro (http://www.serpro.gov.br)
 * @author      Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Test class for Tinebase_User_Ldap
 */
class Custom_Tinebase_User_LdapTest extends Tinebase_User_LdapTest
{
    /**
     * Verify if ldap backend is readonly
     * @return boolean
     */
    protected function _isReadOnlyBackend()
    {
        $userBackendConfigs = Tinebase_Config::getInstance()->get(Tinebase_Config::USERBACKEND);
        if (($userBackendConfigs->get('readonly') == 1) ||
                ($userBackendConfigs->get('masterLdapHost') == '')) {
            return true;
        }
        return false;
    }

    /**
     * Improve domain detection from Tinebase_Core::getUser()->accountEmailAddress
     * Append unique id to user loginName and Email for account creation
     *
     * @return Tinebase_Model_FullUser
     */
    public static function getTestRecord()
    {
        $config = Zend_Registry::get('testConfig');
        $smtpConfig = Tinebase_Config::getInstance()->get(Tinebase_Model_Config::SMTP, new Tinebase_Config_Struct())->toArray();

        if ($config && isset($config->maildomain)) {
            $emailDomain = $config->maildomain;
        } else if (isset($smtpConfig['primarydomain']) && ! empty($smtpConfig['primarydomain'])) {
            $emailDomain = $smtpConfig['primarydomain'];
        } else if (!empty(Tinebase_Core::getUser()->accountEmailAddress)) {
            list($user, $emailDomain) = explode('@', Tinebase_Core::getUser()->accountEmailAddress, 2);
        } else {
            $this->markTestSkipped('Test cannot be run. No mail domain found');
        }

        $userData = array(
            'accountLoginName'      => 'phpunit' . TestCase::getUniqueId(),
            'accountStatus'         => 'enabled',
            'accountExpires'        => NULL,
            'accountPrimaryGroup'   => Tinebase_Group::getInstance()->getDefaultGroup()->id,
            'accountLastName'       => 'Tine 2.0',
            'accountFirstName'      => 'PHPUnit User',
            'accountEmailAddress'   => 'phpunit' . TestCase::getUniqueId() . '@' . $emailDomain,
        );
        if ($config && isset($config->customtests) && isset($config->customtests->customAttributes)
            && isset($config->customtests->customAttributes->Tinebase_Model_FullUser)
            && isset($config->customtests->customAttributes->Tinebase_Model_FullUser->accountDN) )
        {
            $userData['accountDN'] = $config->customtests->customAttributes->Tinebase_Model_FullUser->accountDN;
        }

        $user  = new Tinebase_Model_FullUser($userData);

        return $user;
    }

    /**
     * Tears down the fixture
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Readonly backend");
        } else {
            parent::tearDown();
        }
    }

    /**
     * It is necessary to avoid skipping of all tests
     */
    public function testDoNothing()
    {
        $this->assertNull(NULL);
    }

    /**
     * try to add an user
     *
     * @return Tinebase_Model_FullUser
     */
    public function testAddUser()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Readonly backend");
        } else {
            parent::testAddUser();
        }
    }

    protected function _getPersonaMappingOfUsername($username)
    {
        $config = Zend_Registry::get('testConfig');
        if (isset($config) &&
                isset($config->personasmapping) &&
                isset($config->personasmapping->$username)) {
            $username = $config->personasmapping->$username;
        }
        return $username;
    }

    /**
     * try to get all users containing phpunit in there name
     */
    public function testGetUsers()
    {
        if($this->_isReadOnlyBackend()) {
            $username = $this->_getPersonaMappingOfUsername('sclever');
            $users = $this->_backend->getUsers($username);
            $this->assertGreaterThanOrEqual(1, count($users));
        } else {
            $this->testAddUser();
            $users = $this->_backend->getUsers('phpunit');
            $this->assertGreaterThanOrEqual(1, count($users));
        }
    }

    /**
     * try to get an user by loginname
     *
     */
    public function testGetUserByLoginName()
    {
        if($this->_isReadOnlyBackend()) {
            $username = $this->_getPersonaMappingOfUsername('sclever');
            $testUser = $this->_backend->getFullUserByLoginName($username);

            $this->assertEquals($username, $testUser->accountLoginName);
            $this->assertEquals('Tinebase_Model_FullUser', get_class($testUser), 'wrong type');
            return $testUser;
        } else {
            parent::testGetUserByLoginName();
        }
    }

    /**
     * try to get an user by userId
     *
     */
    public function testGetUserById()
    {
        if($this->_isReadOnlyBackend()) {
            $user = $this->testGetUserByLoginName();
            $testUser = $this->_backend->getFullUserById($user->getId());

            $this->assertEquals($user->accountLoginName, $testUser->accountLoginName);
            $this->assertEquals('Tinebase_Model_FullUser', get_class($testUser), 'wrong type');
        } else {
            parent::testGetUserById();
        }
    }

    /**
     * try to update an user
     */
    public function testUpdateUser()
    {
        if ($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Read only backend");
        } else {
            parent::testUpdateUser();
        }
    }

    /**
     * try to enable an account
     *
     */
    public function testSetStatus()
    {
        if($this->_isReadOnlyBackend()) {
            $user = $this->testGetUserByLoginName();
            $this->_backend->setStatus($user, Tinebase_User::STATUS_DISABLED);
            $testUser = $this->_backend->getUserById($user, 'Tinebase_Model_FullUser');
            $this->assertEquals(Tinebase_User::STATUS_DISABLED, $testUser->accountStatus);

            $this->_backend->setStatus($user, Tinebase_User::STATUS_ENABLED);
            $testUser = $this->_backend->getUserById($user, 'Tinebase_Model_FullUser');
            $this->assertEquals(Tinebase_User::STATUS_ENABLED, $testUser->accountStatus);
        } else {
            parent::testSetStatus();
        }
    }

    /**
     * try to update the logintimestamp
     *
     */
    public function testSetLoginTime()
    {
        if ($this->_isReadOnlyBackend()) {
            $user = $this->testGetUserByLoginName();
            $this->_backend->setLoginTime($user, '127.0.0.1');
            $testUser = $this->_backend->getUserById($user, 'Tinebase_Model_FullUser');
            $this->assertNotEquals($user->accountLastLogin, $testUser->accountLastLogin);
        } else {
            parent::testSetLoginTime();
        }
    }

    /**
     * try to set password
     */
    public function testSetPassword()
    {
        if ($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Read only backend");
        } else {
            parent::testSetPassword();
        }
    }

    /**
     * try to set the expirydate
     *
     */
    public function testSetExpiryDate()
    {
        if($this->_isReadOnlyBackend()) {
            $user = $this->testGetUserByLoginName();
            $this->_backend->setExpiryDate($user, Tinebase_DateTime::now()->subDay(1));
            $testUser = $this->_backend->getUserById($user, 'Tinebase_Model_FullUser');

            $this->assertEquals('Tinebase_DateTime', get_class($testUser->accountExpires), 'wrong type');
            $this->assertEquals(Tinebase_User::STATUS_EXPIRED, $testUser->accountStatus);

            $this->_backend->setExpiryDate($user, NULL);
            $testUser = $this->_backend->getUserById($user, 'Tinebase_Model_FullUser');

            $this->assertEquals(NULL, $testUser->accountExpires);
            $this->assertEquals(Tinebase_User::STATUS_ENABLED, $testUser->accountStatus);
        } else {
            parent::testSetExpiryDate();
        }
    }

    /**
     * try to delete an user
     *
     */
    public function testDeleteUser()
    {
        if($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Readonly backend");
        } else {
            parent::testDeleteUser();
        }
    }

    /**
     * execute Tinebase_User::syncUser
     */
    public function testSyncUser()
    {
        if ($this->_isReadOnlyBackend()) {
            $this->markTestSkipped("Read only backend");
        } else {
            parent::testSyncUser();
        }
    }
}