<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 *
 */

/**
 * Test class for Tinebase_PersistentFilter
 */
class Custom_Tinebase_PersistentFilterTest extends Tinebase_PersistentFilterTest
{

    /**
     * test add grant
     */
    public function testAddGrants()
    {
        $defaultAdminGroup = Tinebase_Group::getInstance()->getDefaultAdminGroup();
        $filter = $this->testSaveSharedFavorite();
        $filter->grants->addRecord(new Tinebase_Model_PersistentFilterGrant(array(
            'account_id'       => $defaultAdminGroup->getId(),
            'account_type'     => Tinebase_Acl_Rights::ACCOUNT_TYPE_GROUP,
            'record_id'        => $filter->getId(),
            Tinebase_Model_Grants::GRANT_READ   => true,
        )));
        $updatedFilter = $this->_instance->update($filter);

        $this->assertEquals(3, count($updatedFilter->grants));
        $grant = $updatedFilter->grants->filter('account_id', $defaultAdminGroup->getId())->getFirstRecord();
        $this->assertTrue($grant !== null);
        $this->assertTrue($grant->userHasGrant(Tinebase_Model_PersistentFilterGrant::GRANT_READ));
    }
}
