<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Tinebase
 * @subpackage  Timemachine
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2016 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Custom Test Class for ModificationLog
 */
class Custom_Tinebase_Timemachine_ModificationLogTest extends Tinebase_Timemachine_ModificationLogTest
{
    /**
     * Custom test modlog undo
     * While Tine20 starts modlog->seq in 1, Expresso starts it in 0
     */
    public function testUndo()
    {
        // create a record
        $contact = Addressbook_Controller_Contact::getInstance()->create(new Addressbook_Model_Contact(array(
            'n_family' => 'tester',
            'tel_cell' => '+491234',
        )));
        // change something using the record controller
        $contact->tel_cell = NULL;
        $contact = Addressbook_Controller_Contact::getInstance()->update($contact);

        // fetch modlog and test seq
        $modlog = $this->_modLogClass->getModifications('Addressbook', $contact->getId(), NULL, 'Sql',
            Tinebase_DateTime::now()->subSecond(5), Tinebase_DateTime::now())->getFirstRecord();
        $this->assertTrue($modlog !== NULL);
        $this->assertEquals(1, $modlog->seq);
        $this->assertEquals('+491234', $modlog->old_value);

        $filter = new Tinebase_Model_ModificationLogFilter(array(
            array('field' => 'record_type',         'operator' => 'equals', 'value' => 'Addressbook_Model_Contact'),
            array('field' => 'record_id',           'operator' => 'equals', 'value' => $contact->getId()),
            array('field' => 'modification_time',   'operator' => 'within', 'value' => 'weekThis'),
        ));
        $result = $this->_modLogClass->undo($filter);
        $this->assertEquals(1, $result['totalcount'], 'did not get 1 undone modlog: ' . print_r($result, TRUE));
        $this->assertEquals('+491234', $result['undoneModlogs']->getFirstRecord()->old_value);

        // check record after undo
        $contact = Addressbook_Controller_Contact::getInstance()->get($contact);
        $this->assertEquals('+491234', $contact->tel_cell);
    }

    /**
     * Custom testDateTimeModlog
     *
     * While Tine20 starts modlog->seq in 1, Expresso starts it in 0
     */
    public function testDateTimeModlog()
    {
        $task = Tasks_Controller_Task::getInstance()->create(new Tasks_Model_Task(array(
            'summary' => 'test task',
        )));

        $task->due = Tinebase_DateTime::now();
        $updatedTask = Tasks_Controller_Task::getInstance()->update($task);

        $task->seq = 0;
        $modlog = $this->_modLogClass->getModificationsBySeq($task, 1);

        $this->assertEquals(1, count($modlog));
        $this->assertEquals((string) $task->due, (string) $modlog->getFirstRecord()->new_value, 'new value mismatch: ' . print_r($modlog->toArray(), TRUE));
    }
}
