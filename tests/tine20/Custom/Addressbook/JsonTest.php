<?php

/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Addressbook
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2008-2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2016, Serpro - Serviço Federal de Processamento de Dados
 * @author      Mário César Kolling <mario.kolling@serpro.gov.br>
 *
 */

/**
 * Custom test class for Addressbook_Frontend_Json
 */
class Custom_Addressbook_JsonTest extends Addressbook_JsonTest
{
    /**
     * Customized test tags modlog for Expresso
     * This test was customized because Expresso implementation changed the way
     * notes logs are formed by translating the record type of the data changed and
     * the record data changed was added to the message, instead of the generic sentence "n Action"
     *
     * @return array contact with tag
     *
     */
    public function testTagsModlog()
    {
        $contact = $this->_addContact();
        $tagName = Tinebase_Record_Abstract::generateUID();
        $tag = array(
            'type'          => Tinebase_Model_Tag::TYPE_PERSONAL,
            'name'          => $tagName,
            'description'    => 'testModlog',
            'color'         => '#009B31',
        );
        $contact['tags'] = array($tag);

        $result = $this->_instance->saveContact($contact);

        $this->assertEquals($tagName, $result['tags'][0]['name']);

        // testing if a tag was added
        $this->_checkChangedNote($result['id'], array(
            Tinebase_Translation::getTranslation('Tinebase')->_('Changed fields:') . ' '
                . Tinebase_Translation::getTranslation('Tinebase')->_('tags'),
            '"added":[{',
        ));

        return $result;
    }
}
