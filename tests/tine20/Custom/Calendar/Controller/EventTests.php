<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */

/**
 * Test class for Calendar_Controller_Event
 *
 * @package     Calendar
 */
class Custom_Calendar_Controller_EventTests extends Calendar_Controller_EventTests
{

    public function testAttendeeGroupMembersAddUser()
    {
        $this->markTestSkipped('For now, Expresso does not add users');
    }

    public function testCreateEventWithConfictFromGroupMember()
    {
        $this->markTestSkipped('Expresso does not add group as attender yet');
    }

    public function testAttendeeGroupMembers()
    {
        $this->markTestSkipped('Expresso does not add group as attender yet');
    }

    public function testAttendeeGroupMembersChange()
    {
        $this->markTestSkipped('Expresso does not add group as attender yet');
    }

    public function testAttendeeNotInFilter()
    {
        $this->markTestSkipped('Operator notin in AttenderFilter is not used in Expresso');
    }

    public function testCustomFields()
    {
        $this->markTestSkipped('Custom fields are not enabled for Expresso Calendar');
    }

}
