<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2010-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Goekmen Ciyiltepe <g.ciyiltepe@metaways.de>
 */

/**
 * Test class for Calendar_Controller_Event
 *
 * @package     Calendar
 */
class Custom_Calendar_Controller_RecurTest extends Calendar_Controller_RecurTest
{
    /**
     * Expresso increment rrule_until if it is lesser than dtend
     * So we don't expect Tinebase_Exception_Record_Validation
     * Instead, we check if rrule_until is greater than dtstart
     */
    public function testInvalidRruleUntil()
    {
        $event = new Calendar_Model_Event(array(
            'uid'           => Tinebase_Record_Abstract::generateUID(),
            'summary'       => 'Abendessen',
            'dtstart'       => '2012-06-01 18:00:00',
            'dtend'         => '2012-06-01 18:30:00',
            'originator_tz' => 'Europe/Berlin',
            'rrule'         => 'FREQ=DAILY;INTERVAL=1;UNTIL=2011-05-31 17:30:00',
            'container_id'  => $this->_getTestCalendar()->getId(),
        ));

        $persistentEvent = $this->_controller->create($event);
        $this->assertGreaterThanOrEqual($persistentEvent->dtstart->getTimeStamp(), $persistentEvent->rrule_until->getTimeStamp());
    }

}