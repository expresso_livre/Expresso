<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Calendar
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2010-2014 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 */

/**
 * Custom tests for Calendar_Controller_MSEventFacade
 *
 * @package     Calendar
 */
class Custom_Calendar_Controller_MSEventFacadeTest extends Calendar_Controller_MSEventFacadeTest
{
    /**
     * Expresso does not store event attachments
     *
     * @return Calendar_Model_Event
     */
    public function testCreate()
    {
        $event = $this->getTestEvent();
        $persistentEvent = $this->_uit->create($event);
        $this->_assertTestEvent($persistentEvent);

        return $persistentEvent;
    }

    /**
     * asserts tested event without checking for attachments
     *
     * @param Calendar_Model_Event $persistentEvent
     */
    protected function _assertTestEvent($persistentEvent)
    {
        $this->assertEquals(2, $persistentEvent->exdate->count());

        $this->assertEquals(Calendar_Model_Event::TRANSP_OPAQUE, $persistentEvent->transp, 'base transp from perspective');
        $this->assertEquals(3, count($persistentEvent->alarms), 'base alarms not from perspective');
        $this->assertEquals(0, count($persistentEvent->alarms->filter('minutes_before', 15)), '15 min. before is not skipped');
        $this->assertEquals(0, count($persistentEvent->alarms->filter('minutes_before', 60)), '60 min. before is not for test CU');

        $persistException = $persistentEvent->exdate->filter('is_deleted', 0)->getFirstRecord();
        $this->assertEquals('2009-03-26 08:00:00', $persistException->dtstart->format(Tinebase_Record_Abstract::ISO8601LONG));
        $this->assertEquals('2009-03-26 06:00:00', $persistException->getOriginalDtStart()->format(Tinebase_Record_Abstract::ISO8601LONG));
        $this->assertEquals('exception', $persistException->summary);
        $this->assertEquals(Calendar_Model_Event::TRANSP_OPAQUE, $persistException->transp, 'recur transp from perspective');
        $this->assertEquals(3, count($persistException->alarms), 'exception alarms not from perspective');
        $this->assertEquals(0, count($persistException->alarms->filter('minutes_before', 15)), '15 min. before is not skipped');
        $this->assertEquals(0, count($persistException->alarms->filter('minutes_before', 60)), '60 min. before is not for test CU');

        $deletedInstance = $persistentEvent->exdate->filter('is_deleted', 1)->getFirstRecord();
        $this->assertEquals('2009-03-27 06:00:00', $deletedInstance->dtstart->format(Tinebase_Record_Abstract::ISO8601LONG));
    }

}