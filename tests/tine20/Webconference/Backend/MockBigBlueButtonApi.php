<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Webconference
 * @subpackage  Backend
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 SERPRO (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */
class Webconference_Backend_MockBigBlueButtonApi extends Webconference_Backend_BigBlueButtonApi
{
}
