<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Webconference
 * @subpackage  AllTests
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 SERPRO (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'TestHelper.php';

class Webconference_AllTests
{
    /**
     * run Felamimail tests
     *
     */
    public static function main()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }

    /**
     * get all Felamimail test suites
     *
     * @return PHPUnit_Framework_TestSuite
     */
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('Tine 2.0 Webconference All Tests');
        $suite->addTestSuite('Webconference_BackendTest');
        return $suite;
    }
}
