<?php
/**
 * Tine 2.0 - http://www.tine20.org
 *
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html
 * @copyright   Copyright (c) 2016 SERPRO (http://www.serpro.gov.br)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 */

/**
 * Test helper
 */
require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'TestHelper.php';

/**
 * Test class for Webconference_Backend
 */
class Webconference_BackendTest extends PHPUnit_Framework_TestCase
{
    /**
     * test if backend factory returns right instance
     */
    public function testBackendFactory()
    {
        // happy way - use of default class
        $backend = Webconference_Backend::factory(Webconference_Backend::BIGBLUEBUTTONAPI);
        $this->assertEquals('Webconference_Backend_BigBlueButtonApi', get_class($backend));
    }
}
