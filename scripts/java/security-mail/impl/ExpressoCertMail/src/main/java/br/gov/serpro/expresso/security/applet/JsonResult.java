/**
 * JsonResult Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import br.gov.serpro.expresso.security.applet.openmessage.ExpressoMessage;
import com.sun.mail.iap.ByteArray;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;

@JsonPropertyOrder({"type", "success", "exception", "mimeMsg", "exprMsg", "signerCertificate"})
public class JsonResult {

    private String type;
    @JsonProperty("type")
    public void setType(String value) {
        type = value;
    }
    public String getType() {
        return type;
    }

    private Boolean success;
    @JsonProperty("success")
    public void setSuccess(Boolean value) {
        success = value;
    }
    public Boolean isSuccess() {
        return success;
    }

    private JsonException exception;
    @JsonProperty("exception")
    public void setException(JsonException value) {
        exception = value;
    }
    public JsonException getException() {
        return exception;
    }

    private String mimeMsg;
    @JsonProperty("mimeMsg")
    public void setMimeMsg(String value) {
        mimeMsg = value;
    }
    public String getMimeMsg() {
        return mimeMsg;
    }

    private ExpressoMessage exprMsg;
    @JsonProperty("exprMsg")
    public void setExprMsg(ExpressoMessage value) {
        exprMsg = value;
    }
    public ExpressoMessage getExprMsg() {
        return exprMsg;
    }

    private String signerCertificate;
    @JsonProperty("signerCertificate")
    public void setSignerCertificate(Certificate certificate) throws IOException, CertificateEncodingException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out);
        PemWriter pemWriter = new PemWriter(writer);
        pemWriter.writeObject(new PemObject("CERTIFICATE", certificate.getEncoded()));
        pemWriter.flush();
        signerCertificate = out.toString();
    }
    public String getSignerCertificate() {
        return signerCertificate;
    }

}
