/**
 * PKCS11ConfigBuilder Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.File;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.logging.Logger;
import br.gov.serpro.expresso.security.applet.openmessage.PKCS11Config.Mechanism;

public class PKCS11ConfigBuilder {

    private static final Logger logger = Logger.getLogger(PKCS11ConfigBuilder.class.getName());

    private PKCS11ConfigBuilder() {}

    public static PKCS11ConfigBuilder create() {
        return new PKCS11ConfigBuilder();
    }

    final private Deque<File> scanPaths = new ArrayDeque<File>();
    {
        scanPaths.add(new File("C:/Windows/System32"));
        scanPaths.add(new File("/usr/lib"));
    }
    private String name;
    final private List<String> libraries = new ArrayList<String>();
    private String description;
    private String slot;
    private Integer slotListIndex;
    final private List<Mechanism> enabledMechanisms = new ArrayList<Mechanism>();
    final private List<Mechanism> disabledMechanisms = new ArrayList<Mechanism>();
    private Boolean showInfo;
    //TODO: attributes

    public PKCS11ConfigBuilder scanPath(String path) {
        this.scanPaths.addFirst(new File(path));
        return this;
    }

    public PKCS11ConfigBuilder name(String name) {
        this.name = name;
        return this;
    }

    public PKCS11ConfigBuilder library(String library) {
        this.libraries.add(library);
        return this;
    }

    public PKCS11ConfigBuilder description(String description) {
        this.description = description;
        return this;
    }

    public PKCS11ConfigBuilder slot(String slot) {
        this.slot = slot;
        return this;
    }

    public PKCS11ConfigBuilder slotListIndex(int slotListIndex) {
        this.slotListIndex = slotListIndex;
        return this;
    }

    public PKCS11ConfigBuilder enableMechanism(Mechanism... mechanism) {
        for(Mechanism m : mechanism) {
            if(enabledMechanisms.contains(m)) continue;
            enabledMechanisms.add(m);
        }
        return this;
    }

    public PKCS11ConfigBuilder disableMechanism(Mechanism... mechanism) {
        for(Mechanism m : mechanism) {
            if(disabledMechanisms.contains(m)) continue;
            disabledMechanisms.add(m);
        }
        return this;
    }

    public PKCS11ConfigBuilder showInfo(boolean showInfo) {
        this.showInfo = showInfo;
        return this;
    }

    public PKCS11Config build() {

        String library = null;

        scan:for(String lib : libraries) {
            File libPath = new File(lib);
            if(libPath.isAbsolute()) {
                if(libPath.canRead()) {
                    library = libPath.toString();
                    break scan;
                }
            }
            else {
                for(File scanPath : scanPaths) {
                    libPath = new File(scanPath, lib);
                    if(libPath.canRead()) {
                        library = libPath.toString();
                        break scan;
                    }
                }
            }
        }

        if(library == null) {
            return null;
        }

        return new PKCS11Config(
            name,
            description,
            library,
            slot,
            slotListIndex,
            enabledMechanisms,
            disabledMechanisms,
            showInfo);
    }
}
