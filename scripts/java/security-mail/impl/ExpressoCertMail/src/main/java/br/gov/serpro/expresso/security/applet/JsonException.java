/**
 * JsonException Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import com.google.common.base.Throwables;

@JsonPropertyOrder({"type", "message", "localizedMessage", "cause", "stackTrace"})
public class JsonException {

    private String type;
    @JsonProperty("type")
    public void setType(String value) {
        type = value;
    }
    public String getType() {
        return type;
    }


    private String message;
    @JsonProperty("message")
    public void setMessage(String value) {
        message = value;
    }
    public String getMessage() {
        return message;
    }


    private String localizedMessage;
    @JsonProperty("localizedMessage")
    public void setLocalizedMessage(String value) {
        localizedMessage = value;
    }
    public String getLocalizedMessage() {
        return localizedMessage;
    }


    private JsonException cause;
    @JsonProperty("cause")
    public void setCause(JsonException value) {
        cause = value;
    }
    public JsonException getCause() {
        return cause;
    }


    private String stackTrace;
    @JsonProperty("stackTrace")
    public void setStackTrace(String value) {
        stackTrace = value;
    }
    public String getStackTrace() {
        return stackTrace;
    }


    JsonException() {}

    JsonException(Throwable e) {
        type = e.getClass().getName();
        message = e.getMessage();
        localizedMessage = e.getLocalizedMessage();
        stackTrace = Throwables.getStackTraceAsString(e);
        if(e.getCause() != null) {
            cause = new JsonException(e.getCause());
        }
    }
}
