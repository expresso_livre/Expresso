/**
 * MediaType Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import org.apache.james.mime4j.dom.field.ContentTypeField;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;

public enum MediaType {

    TEXT_CSS (Type.TEXT, "css", "css"),
    TEXT_HTML  (Type.TEXT, "html", "htm", "html"),
    TEXT_XML  (Type.TEXT, "xml", "xml"),
    TEXT_JAVA  (Type.TEXT, "java", "java"),
    TEXT_X_JAVA_SOURCE (Type.TEXT, "x-java-source", "java"),
    TEXT_PLAIN (Type.TEXT, "plain", "txt", "asc", "md"),
    IMAGE_GIF (Type.IMAGE, "gif", "gif"),
    IMAGE_JPEG (Type.IMAGE, "jpeg", "jpg", "jpeg"),
    IMAGE_PNG (Type.IMAGE, "png", "png"),
    IMAGE_BMP (Type.IMAGE, "bmp", "bmp"),
    IMAGE_TIFF (Type.IMAGE, "tiff", "tif", "tiff"),
    IMAGE_VND_MICROSOFT_ICON (Type.IMAGE, "vnd.microsoft.icon", "ico"),
    IMAGE_SVG (Type.IMAGE, "svg+xml", "svg"),
    AUDIO_MPEG (Type.AUDIO, "mpeg", "mp3"),
    AUDIO_MPEG_URL (Type.AUDIO, "mpeg-url", "m3u"),
    VIDEO_MP4 (Type.VIDEO, "mp4", "mp4"),
    VIDEO_OGG (Type.VIDEO, "ogg", "ogv"),
    VIDEO_X_FLV (Type.VIDEO, "x-flv", "flv"),
    VIDEO_QUICKTIME (Type.VIDEO, "quicktime", "mov"),
    APPLICATION_X_SHOCKWAVE_FLASH (Type.APPLICATION, "x-shockwave-flash", "swf"),
    APPLICATION_JAVASCRIPT (Type.APPLICATION, "javascript", "js"),
    APPLICATION_PDF (Type.APPLICATION, "pdf", "pdf"),
    APPLICATION_MSWORD (Type.APPLICATION, "msword", "doc", "docx"),
    APPLICATION_X_OGG (Type.APPLICATION, "x-ogg", "ogg"),
    APPLICATION_OCTET_STREAM (Type.APPLICATION, "octet-stream", "zip", "exe", "class");

    private static final class $S {
        //(type, subtype) -> mediatype
        private static final Table<String, String, MediaType> values = TreeBasedTable.create();
        //(extension, weight) -> mediatype
        private static final Table<String, Integer, MediaType> extValues = TreeBasedTable.create();
    }

    private final Type type;
    private final String subtype;
    private final String[] extensions;

    public Type type() {
        return type;
    }

    public String subtype() {
        return subtype;
    }

    public String[] extensions() {
        return extensions;
    }

    @Override
    public String toString() {
        return type + "/" + subtype;
    }

    MediaType(Type type, String subtype, String... extensions) {
        this.type = type;
        this.subtype = subtype;
        this.extensions = extensions;
        $S.values.put(type.type, subtype, this);
        int i = 0;
        for(String ext : extensions) {
            ++i;
            $S.extValues.put(ext, i, this);
        }
    };

    public enum Type {
        APPLICATION("application"),
        AUDIO("audio"),
        IMAGE("image"),
        MESSAGE("message"),
        MODEL("model"),
        MULTIPART("multipart"),
        TEXT("text"),
        VIDEO("video");

        private final String type;

        public String type() {
            return type;
        }

        Type(String type) {
            this.type = type;
        }
    }

    public static MediaType lookup(ContentTypeField contentType) {
        return $S.values.get(contentType.getMediaType(), contentType.getSubType());
    }

    // Get MIME type from file name extension, if possible
    public static MediaType lookup(String uri) {
        int dot = uri.lastIndexOf('.');
        if (dot >= 0) {
            String ext = uri.substring(dot + 1).toLowerCase();
            for(MediaType mediaType : $S.extValues.row(ext).values()) {
                return mediaType;
            }
        }
        return APPLICATION_OCTET_STREAM;
    }
}
