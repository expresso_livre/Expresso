/**
 * PKCS11Config Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.util.List;
import java.util.logging.Logger;
import javax.security.auth.login.LoginException;
import sun.security.pkcs11.SunPKCS11;
import static java.util.logging.Level.*;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.*;

public class PKCS11Config {

    private static final Logger logger = Logger.getLogger(PKCS11Config.class.getName());

    private final String name;
    private final String description;
    private final String library;
    private final String slot;
    private final Integer slotListIndex;
    private final List<Mechanism> enabledMechanisms;
    private final List<Mechanism> disabledMechanisms;
    private final Boolean showInfo;
    //TODO: attributes

    private final Provider provider;

    private KeyStore keyStore;

    PKCS11Config(
            String name,
            String description,
            String library,
            String slot,
            Integer slotListIndex,
            List<Mechanism> enabledMechanisms,
            List<Mechanism> disabledMechanisms,
            Boolean showInfo
    ) {
        this.name = name;
        this.description = description;
        this.library = library;
        this.slot = slot;
        this.slotListIndex = slotListIndex;
        this.enabledMechanisms = enabledMechanisms;
        this.disabledMechanisms = disabledMechanisms;
        this.showInfo = showInfo;

        provider = new SunPKCS11(new ByteArrayInputStream(toString().getBytes()));
        Security.addProvider(provider);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLibrary() {
        return library;
    }

    public String getSlot(){
        return slot;
    }

    public Integer getSlotListIndex() {
        return slotListIndex;
    }

    public List<Mechanism> getEnabledMechanisms() {
        return enabledMechanisms;
    }

    public List<Mechanism> getDisabledMechanisms() {
        return disabledMechanisms;
    }

    public Boolean showInfo() {
        return showInfo;
    }

    public Provider getProvider() {
        return provider;
    }

    public KeyStore getKeyStore() {
        return keyStore;
    }

    private String strValue;

    @Override
    public final String toString() {
        if(strValue != null) {
            return strValue;
        }
        StringBuilder sb = new StringBuilder();
        if(name != null) {
            sb.append("name = ").append(name).append("\n");
        }
        if(library != null) {
            sb.append("library = ").append(library).append("\n");
        }
        if(description != null) {
            sb.append("description = ").append(description).append("\n");
        }
        if(slot != null) {
            sb.append("slot = ").append(slot).append("\n");
        }
        if(slotListIndex != null) {
            sb.append("slotListIndex = ").append(slotListIndex).append("\n");
        }
        if( ! enabledMechanisms.isEmpty()) {
            sb.append("enabledMechanisms = {\n");
            for(Mechanism mechanism : enabledMechanisms) {
                sb.append("  ").append(mechanism.name()).append("\n");
            }
            sb.append("}\n");
        }
        if( ! disabledMechanisms.isEmpty()) {
            sb.append("disabledMechanisms = {\n");
            for(Mechanism mechanism : disabledMechanisms) {
                sb.append("  ").append(mechanism.name()).append("\n");
            }
            sb.append("}\n");
        }
        if(showInfo != null) {
            sb.append("showInfo = ").append(showInfo).append("\n");
        }

        return (strValue = sb.toString());
    }


    private String htmlStrValue;

    public final String toHtmlString() {
        if(htmlStrValue != null) {
            return htmlStrValue;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        if(name != null) {
            sb.append("<b>name</b> = ").append(name).append("<br>");
        }
        if(library != null) {
            sb.append("<b>library</b> = ").append(library).append("<br>");
        }
        if(description != null) {
            sb.append("<b>description</b> = ").append(description).append("<br>");
        }
        if(slot != null) {
            sb.append("<b>slot</b> = ").append(slot).append("<br>");
        }
        if(slotListIndex != null) {
            sb.append("<b>slotListIndex</b> = ").append(slotListIndex).append("<br>");
        }
        if( ! enabledMechanisms.isEmpty()) {
            sb.append("<b>enabledMechanisms</b> = {<br>");
            for(Mechanism mechanism : enabledMechanisms) {
                sb.append("  ").append(mechanism.name()).append("<br>");
            }
            sb.append("}<br>");
        }
        if( ! disabledMechanisms.isEmpty()) {
            sb.append("<b>disabledMechanisms</b> = {<br>");
            for(Mechanism mechanism : disabledMechanisms) {
                sb.append("  ").append(mechanism.name()).append("<br>");
            }
            sb.append("}<br>");
        }
        if(showInfo != null) {
            sb.append("<b>showInfo</b> = ").append(showInfo).append("<br>");
        }
        sb.append("</html>");
        return (htmlStrValue = sb.toString());
    }

    private boolean loaded;

    public boolean isLoaded() {
        return loaded;
    }

    public boolean load(String password) {
        try {
            getKeyStore().load(new FileInputStream(getLibrary()), password.toCharArray());
        }
        catch (Exception e) {
            if(e.getCause() instanceof LoginException) {
                return loaded = false;
            }
            logger.log(SEVERE, e.getMessage(), e);
            raise(e);
        }
        return loaded = true;
    }

    public boolean isConnected() {
        return (keyStore != null);
    }

    public void update() {

        int n = 0;
        update: for(;;) {
            try {
                ++n;
                if(keyStore == null) {
                    keyStore = KeyStore.getInstance("pkcs11", provider);
                }
                keyStore.load(new FileInputStream(getLibrary()), "".toCharArray());
                loaded = true;
            }
            catch(Exception e) {
                if(e.getMessage().equalsIgnoreCase("pkcs11 not found")) {
                    logger.log(FINE, "Token não conectado: {0}, {1}", new Object[]{getName(), n});
                    if(n < 3) {
                        continue update;
                    }
                    loaded = false;
                }
                else if(e.getMessage().equalsIgnoreCase("token has been removed")) {
                    logger.log(FINE, "Token removido: {0}", getName());
                    keyStore = null;
                    loaded = false;
                }
                else if(e.getCause() instanceof LoginException) {
                    logger.log(FINE, "Token conectado mas ainda não logado: {0}", getName());
                    loaded = false;
                }
                else {
                    logger.log(SEVERE, e.getMessage(), e);
                    raise(e);
                }
            }
            break update;
        }

        if(logger.isLoggable(FINE)) {
            StringBuilder sb = new StringBuilder();
            sb.append("[").append(name).append(" config]");
            if(isConnected()) {
                if(isLoaded()) {
                    sb.append(" (connected, loaded)");
                }
                else {
                    sb.append(" (connected)");
                }
            }
            sb.append("\n").append(toString());
            logger.fine(sb.toString());
        }
    }

    public enum Mechanism {

        CKM_MD2,
        CKM_MD5,
        CKM_RC4,
        CKM_AES_CTR,
        CKM_RSA_X_509,
        CKM_DSA,
        CKM_DSA_SHA1,
        CKM_ECDSA,
        CKM_ECDSA_SHA1,
        CKM_SHA_1,
        CKM_SHA256,
        CKM_SHA384,
        CKM_SHA512,

        CKM_DES_CBC,
        CKM_DES3_CBC,
        CKM_AES_CBC,
        CKM_BLOWFISH_CBC,

        CKM_MD2_RSA_PKCS,
        CKM_RSA_PKCS,
        CKM_MD5_RSA_PKCS,
        CKM_SHA1_RSA_PKCS,
        CKM_SHA256_RSA_PKCS,
        CKM_SHA384_RSA_PKCS,
        CKM_SHA512_RSA_PKCS,

        CKM_RC4_KEY_GEN,
        CKM_AES_KEY_GEN,
        CKM_DES_KEY_GEN,
        CKM_DES3_KEY_GEN,
        CKM_BLOWFISH_KEY_GEN,

        CKM_RSA_PKCS_KEY_PAIR_GEN,
        CKM_DSA_KEY_PAIR_GEN,
        CKM_DH_PKCS_KEY_PAIR_GEN,
        CKM_EC_KEY_PAIR_GEN,

        CKM_ECDH1_DERIVE,
        CKM_DH_PKCS_DERIVE,

        CKM_MD5_HMAC,
        CKM_SHA_1_HMAC,
        CKM_SHA256_HMAC,
        CKM_SHA384_HMAC,
        CKM_SHA512_HMAC;
    }
}
