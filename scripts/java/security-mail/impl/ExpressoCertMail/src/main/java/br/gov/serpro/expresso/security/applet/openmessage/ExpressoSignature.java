/**
 * ExpressoSignature Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "addresses", "verified", "certificate"})
public class ExpressoSignature {

    private static final Logger logger = Logger.getLogger(ExpressoSignature.class.getName());

    @JsonProperty("addresses")
    private final Set<String> addresses = new TreeSet<String>();
    @JsonIgnore
    public void addAddress(String value) {
        addresses.add(value);
    }
    @JsonIgnore
    public Iterator<String> getAddressesIterator() {
        return addresses.iterator();
    }

    private Boolean verified;
    @JsonProperty("verified")
    public void setVerified(Boolean value) {
        verified = value;
    }
    public Boolean isVerified() {
        return verified;
    }


    private String certificate;
    @JsonProperty("certificate")
    public void setCertificate(String value) {
        certificate = value;
    }
    public String getCertificate() {
        return certificate;
    }
}
