/**
 * WebServer Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet.openmessage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.text.Normalizer;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.base.CharMatcher;
import com.google.common.base.Throwables;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.io.ByteStreams;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import br.gov.serpro.expresso.security.applet.Utils;
import br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response;
import br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.ResponseException;
import static java.util.logging.Level.FINE;
import static java.util.logging.Level.SEVERE;
import static br.gov.serpro.expresso.security.applet.openmessage.CryptoException.raise;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.MIME_HTML;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.MIME_PLAINTEXT;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Method.GET;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Method.OPTIONS;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Method.POST;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response.Status.BAD_REQUEST;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response.Status.FORBIDDEN;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response.Status.INTERNAL_ERROR;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response.Status.METHOD_NOT_ALLOWED;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response.Status.NOT_FOUND;
import static br.gov.serpro.expresso.security.applet.openmessage.NanoHTTPD.Response.Status.OK;

public class WebServer extends NanoHTTPD {

    private static final Logger logger = Logger.getLogger(WebServer.class.getName());

    private static final String getRegex = "^/(download/)?([0-9a-f]{32})(/([^/]*))?$";
    private static final Pattern getPattern = Pattern.compile(getRegex, Pattern.CASE_INSENSITIVE);

    private static final String zipRegex = "^/downloadZip/([0-9a-f]{32})$";
    private static final Pattern zipPattern = Pattern.compile(zipRegex, Pattern.CASE_INSENSITIVE);

    private static final String deleteRegex = "^/delete/([0-9a-f]{32})$";
    private static final Pattern deletePattern = Pattern.compile(deleteRegex, Pattern.CASE_INSENSITIVE);

    private static final String uploadRegex = "^/upload/?$";
    private static final Pattern uploadPattern = Pattern.compile(uploadRegex, Pattern.CASE_INSENSITIVE);

    private final Set<InetAddress> machineAddresses;

    //private final Cache<String, ExpressoAttachment> attachments;
    private final Table<String, String, ExpressoAttachment> attachments;
    private URL codebase;

    public WebServer() {
        super(8998);

        //attachments = CacheBuilder.newBuilder().weakValues().build();
        attachments = HashBasedTable.create();

        machineAddresses = new LinkedHashSet<InetAddress>();
        try {
            machineAddresses.add(InetAddress.getLocalHost());
        }
        catch(UnknownHostException ex) {
            logger.log(SEVERE, Throwables.getStackTraceAsString(ex), ex);
        }

        try {
            Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();
            while(eni.hasMoreElements()) {
                NetworkInterface ni = eni.nextElement();
                Enumeration<InetAddress> eia = ni.getInetAddresses();
                while(eia.hasMoreElements()) {
                    machineAddresses.add(eia.nextElement());
                }
            }
        }
        catch(SocketException ex) {
            logger.log(SEVERE, Throwables.getStackTraceAsString(ex), ex);
        }
    }

    public void setCodebase(URL codebase) {
        this.codebase = codebase;
    }

    public String getAllowOrigin() {
        StringBuilder allowOrigin = new StringBuilder();
        if (codebase != null) {
            allowOrigin
                .append(codebase.getProtocol())
                .append("://")
                .append(codebase.getHost());
            if (codebase.getPort() != -1) {
                allowOrigin.append(":")
                    .append(codebase.getPort());
            }
        } else {
            // TODO: get default allowOrigin from config
            // Fall back to default
            allowOrigin.append("https://expressov3.serpro.gov.br");
        }
        return allowOrigin.toString();
    }

    public void publish(String eid, ExpressoAttachment attachment) {
        logger.log(FINE, "publish message eid: {0} attachment eid: {1}", new Object[]{eid, attachment.getEid()});
        attachments.put(eid, attachment.getEid(), attachment);
    }

    @Override
    public NanoHTTPD.Response serve(NanoHTTPD.IHTTPSession session) {

        String httpClientIp = session.getHeaders().get("http-client-ip");

        client_IP_validation:
        {
            for(InetAddress ia : machineAddresses) {
                if(ia.getHostAddress().equals(httpClientIp)) {
                    break client_IP_validation;
                }
            }
            logger.log(Level.WARNING, "Client IP not allowed: {0}", httpClientIp);
            return new Response(FORBIDDEN, MIME_PLAINTEXT, "CLIENT IP NOT ALLOWED: " + httpClientIp);
        }

        switch(session.getMethod()) {
            case GET:
                return get(session.getUri(), session.getHeaders(), session.getParms(), session.getCookies());

            case POST:
                Map<String, String> files = new HashMap<String, String>();
                try {
                    session.parseBody(files);
                }
                catch (IOException ioe) {
                    logger.log(SEVERE, null, ioe);
                    return new Response(INTERNAL_ERROR, MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
                }
                catch (ResponseException re) {
                    logger.log(SEVERE, null, re);
                    return new Response(re.getStatus(), MIME_PLAINTEXT, re.getMessage());
                }

                return post(session.getUri(), session.getHeaders(), session.getParms(), files, session.getCookies());

            case OPTIONS :
                // For preflighted XmlHttpRequest, just return headers
                return options(session.getUri(), session.getHeaders(), session.getCookies());

            default:
                return new Response(METHOD_NOT_ALLOWED, MIME_PLAINTEXT, "METHOD NOT ALLOWED: " + session.getMethod());
        }
    }

    public Response options(
            String uri,
            Map<String, String> headers,
            CookieHandler cookies) {

        if(logger.isLoggable(FINE)) {
            StringBuilder sb = new StringBuilder();
            sb.append("===OPTIONS===\n");
            sb.append("URI: [").append(uri).append("]\n");
            append("Cookies", cookies, sb);
            append("Headers", headers, sb);
            logger.fine(sb.toString());
        }

        Response response = new Response(OK, MIME_HTML, (InputStream) null);
        response.addHeader("Access-Control-Allow-Origin", getAllowOrigin());
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "content-type,x-file-name,x-file-size,x-file-type,x-requested-with,x-tine20-request-type");

        return  response;
    }

    public Response get(
            String uri,
            Map<String, String> headers,
            Map<String, String> parms,
            CookieHandler cookies) {

        if(logger.isLoggable(FINE)) {
            StringBuilder sb = new StringBuilder();
            sb.append("===GET===\n");
            sb.append("URI: [").append(uri).append("]\n");
            append("Cookies", cookies, sb);
            append("Headers", headers, sb);
            append("Parameters", parms, sb);
            logger.fine(sb.toString());
        }

        Matcher matcher = getPattern.matcher(uri);
        if(matcher.matches()) {

            boolean download = (matcher.group(1) != null);
            String eid = matcher.group(2);
            logger.log(FINE, "download attachment eid: {0}", eid);

            Iterator<ExpressoAttachment> it = attachments.column(eid).values().iterator();

            if(it.hasNext()) {

                ExpressoAttachment att = it.next();

                String mime = MediaType.lookup(att.getFileName()).toString();

                Response res = new Response(OK, mime, new ByteArrayInputStream(att.getBody()));

                if(download) {

                    String asciiOnlyFileName = CharMatcher.ASCII.retainFrom(
                            Normalizer.normalize(att.getFileName(), Normalizer.Form.NFD));

                    if(CharMatcher.WHITESPACE.matchesAnyOf(asciiOnlyFileName)) {
                        asciiOnlyFileName = "\"" + asciiOnlyFileName + "\"";
                    }

                    String utf8URLEncodedFileName = null;
                    try {
                        utf8URLEncodedFileName = URLEncoder.encode(att.getFileName(), "UTF-8").replace("+", "%20");
                    }
                    catch (UnsupportedEncodingException ex) {
                        raise(ex);
                    }

                    res.addHeader("Content-Disposition", "attachment" +
                            "; filename=" + asciiOnlyFileName +
                            "; filename*=UTF-8''" + utf8URLEncodedFileName);
                } else {
                    res.addHeader("Content-Disposition", "inline");
                }

                res.addHeader("Content-Type", att.getContentType());
                res.addHeader("Accept-Ranges", "bytes");
                res.addHeader("Content-Length", "" + att.getSize());

                return res;
            }
            else {
                return new Response(NOT_FOUND, MIME_PLAINTEXT, "NOT FOUND");
            }
        }
        else {
            matcher = zipPattern.matcher(uri);
            if(matcher.matches()) {
                String eid = matcher.group(1);
                logger.log(FINE, "download ZIP message eid: {0}", eid);

                Map<String, ExpressoAttachment> msgAttachments;

                if(attachments.containsRow(eid)) {
                    msgAttachments = attachments.row(eid);
                }
                else {
                    return new Response(OK, MIME_PLAINTEXT, "non-existent resource");
                }

                class Buffer extends ByteArrayOutputStream {
                    public byte[] getBuf() {
                        return buf;
                    }
                    public Buffer() {
                        super(4096);
                    }
                }
                Buffer buf = new Buffer();

                ZipOutputStream outputStream = null;
                try {
                    outputStream = new ZipOutputStream(buf);

                    ZipParameters parameters = new ZipParameters();
                    parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                    //parameters.setCompressionMethod(Zip4jConstants.COMP_STORE);
                    parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
                    //parameters.setCompressionLevel(Zip4jConstants.ENC_NO_ENCRYPTION);
                    parameters.setSourceExternalStream(true);
                    parameters.setEncryptFiles(false);
                    //parameters.setIncludeRootFolder(false);
                    //parameters.setRootFolderInZip("foo");

                    for(ExpressoAttachment att : msgAttachments.values()) {
                        parameters.setFileNameInZip(att.getFileName());
                        try {
                            outputStream.putNextEntry(null, parameters);
                            try {
                                outputStream.write(att.getBody());
                                outputStream.flush();
                            }
                            finally {
                                outputStream.closeEntry();
                            }
                        }
                        catch (IOException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        }
                        catch (ZipException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        }
                    }
                }
                finally {
                    if(outputStream != null) {
                        try {
                            outputStream.flush();
                        }
                        catch (IOException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        }

                        try {
                            outputStream.finish();
                        }
                        catch (IOException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        }
                        catch (ZipException ex) {
                            logger.log(Level.SEVERE, null, ex);
                        }

                        try {
                            outputStream.close();
                        }
                        catch(IOException ex) {
                        }
                    }
                }

                Response res = new Response(OK, "application/zip", new ByteArrayInputStream(buf.getBuf(), 0, buf.size()));
                res.addHeader("Content-Disposition", "attachment; filename=mensagens.zip; filename*=UTF-8''mensagens.zip");
                res.addHeader("Content-Type", "application/x-zip-compressed");
                res.addHeader("Accept-Ranges", "bytes");
                res.addHeader("Content-Length", "" + buf.size());

                return res;
            }
            else {
                matcher = uploadPattern.matcher(uri);
                if(matcher.matches()) {
                    logger.log(FINE, "upload");

                    Response res = new Response(OK, MIME_HTML,
                            "<html>\n" +
                            "    <body>\n" +
                            "        <form name='up' method='post' enctype='multipart/form-data'>\n" +
                            "            <input type='file' name='file'/><br/>\n" +
                            "            <label>message eid: <input type='text' id='msg-eid' name='eid'/></label><br/>\n" +
                            "            <input type='submit' name='submit' value='Upload'/>\n" +
                            "        </form>\n" +
                            "    </body>\n" +
                            "</html>\n");

                    return res;
                }
                else {
                    matcher = deletePattern.matcher(uri);
                    if(matcher.matches()) {
                        String eid = matcher.group(1);
                        logger.log(FINE, "delete attachment eid: {0}", eid);

                        Map<String, ExpressoAttachment> m = attachments.column(eid);

                        if(m.isEmpty()) {
                            return new Response(OK, MIME_PLAINTEXT, "non-existent resource");
                        }
                        else {
                            StringBuilder sb = new StringBuilder();
                            for(Entry<String, ExpressoAttachment> e : m.entrySet()) {
                                attachments.remove(e.getKey(), eid);
                                if(sb.length() > 0) {
                                    sb.append("\n");
                                }
                                sb.append("deleted ")
                                        .append(e.getKey()).append("::").append(eid)
                                        .append(" [").append(e.getValue().getFileName()).append("]");
                            }
                            return new Response(OK, MIME_PLAINTEXT, sb.toString());
                        }
                    }
                    else {
                        return new Response(BAD_REQUEST, MIME_PLAINTEXT, "BAD REQUEST");
                    }
                }
            }
        }
    }

    public NanoHTTPD.Response post(
            String uri,
            Map<String, String> headers,
            Map<String, String> parms,
            Map<String, String> files,
            CookieHandler cookies) {

        if(logger.isLoggable(FINE)) {
            StringBuilder sb = new StringBuilder();
            sb.append("===POST===\n");
            sb.append("URI: [").append(uri).append("]\n");
            append("Cookies", cookies, sb);
            append("Headers", headers, sb);
            append("Parameters", parms, sb);
            append("Files", files, sb);
            logger.fine(sb.toString());
        }

        ExpressoAttachment att = new ExpressoAttachment();

        /**
         *  Parms format:
         *
         *  embbeded images - {
         *      "base64", (yes | no)
         *      "transactionid", (id)
         *      "method", (?)
         *      "upload" (string filename)
         *  }
         *
         *  tempFile {
         *       "",
         *       ""
         *  }
         *
         */

        att.setFileName(parms.containsKey("file") ? parms.get("file") : parms.get("upload"));
        try {
            byte[] a = ByteStreams.toByteArray(new FileInputStream(files.containsKey("file") ? files.get("file") : files.get("upload")));
            att.setBody(a);
        }
        catch (IOException ioe) {
            logger.log(SEVERE, null, ioe);
            return new Response(INTERNAL_ERROR, MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
        }

        att.setSize(att.getBody().length);

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {}

        md.update(att.getBody());

        char[] hexDigest = new char[32];
        int i = 0;
        for(byte b : md.digest()) {
            hexDigest[i++] = Character.forDigit((char)((b & 0xf0) >>> 4), 16);
            hexDigest[i++] = Character.forDigit((char)(b & 0x0f), 16);
        }
        att.setEid(new String(hexDigest));

        att.setContentType(parms.get("type") != null ? parms.get("type") : MediaType.lookup(att.getFileName()).toString());
        att.setError(0);

        publish(parms.get("eid"), att);

        ExpressoJsonAttachmentResponse jsonResponse = new ExpressoJsonAttachmentResponse();
        jsonResponse.setStatus(ExpressoJsonAttachmentResponse.Status.success);
        jsonResponse.setTempFile(att);

        boolean isJsonResponse = headers.containsKey("x-requested-with") && headers.get("x-requested-with").equalsIgnoreCase("XMLHttpRequest");

        try {
            StringBuilder sb = new StringBuilder();
            if (isJsonResponse)
            {
                sb.append(Utils.asJsonString(jsonResponse));
            } else {
                // When multipart/form-data upload, response use postMessage api for cors
                sb.append("<HTML><HEAD</HEAD><BODY><SCRIPT>")
                .append("try{")
                .append(MessageFormat.format("parent.postMessage(JSON.stringify({0}), ''{1}'');",
                    new Object[]{Utils.asJsonString(jsonResponse), getAllowOrigin()}))
                .append("}catch(e){")
                .append("console.log(e.message);")
                .append("}")
                .append("</SCRIPT></BODY></HTML>");
            }

            Response response = new Response(OK, isJsonResponse ? ExpressoJsonAttachmentResponse.CONTENT_TYPE : "text/html", sb.toString());
            //Response response = new Response(OK, ExpressoJsonAttachmentResponse.CONTENT_TYPE, Utils.asJsonString(jsonResponse));
            response.addHeader("Access-Control-Allow-Origin", getAllowOrigin());
            return response;
        }
        catch (IOException ioe) {
            return new Response(INTERNAL_ERROR, MIME_PLAINTEXT, "SERVER INTERNAL ERROR: IOException: " + ioe.getMessage());
        }
    }


    private void append(String name, Map<String, String> map, StringBuilder sb) {
        sb.append(name).append(":\n");
        for(Entry<String,String> field : map.entrySet()) {
            sb.append("    ").append(field.getKey()).append(": ").append(field.getValue()).append("\n");
        }
    }

    private void append(String name, Iterable<String> iter, StringBuilder sb) {
        sb.append(name).append(":\n");
        for(String field : iter) {
            sb.append("    ").append(field).append("\n");
        }
    }

}
