/**
 * Utils Class
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mario Cesar Kolling <mario.kolling@serpro.gov.br>
 * @author      Marcio Andre Scholl Levien <marcio.levien@serpro.gov.br>
 * @copyright   Copyright (c) 2011-2016 Serpro (http://www.serpro.gov.br)
 */

package br.gov.serpro.expresso.security.applet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

public final class Utils {

    private Utils() {}

    public static String asString(MimeMessage m) throws IOException, MessagingException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.writeTo(os); //ASCII output ...
        return os.toString("ISO-8859-1");
    }

    public static String asJsonString(Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"));
        ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
        StringWriter sw = new StringWriter();
        writer.writeValue(sw, obj);

        //TODO: remover/comentar antes da distribuição
//        System.out.println(sw.toString());

        return sw.toString();
    }

}
